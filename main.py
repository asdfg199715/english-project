# -*- coding: utf-8 -*-

import os
import platform
import random
import sqlite3
import sys
import threading
import time
from datetime import date
from Vocabularyword import (VocablaryLA, VocabluaryGEPTLA, VocabluaryGEPTLB,
                  VocabluaryGEPTLC)
import certifi
from kivymd.uix.managerswiper import MDSwiperPagination
import googlemaps
import pyttsx3
import requests
from bs4 import BeautifulSoup
from googletrans import Translator
from gtts import gTTS
from kivymd.app import MDApp
from kivy.clock import Clock
from kivy.core.window import Window, WindowBase
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import (BooleanProperty, ListProperty, NumericProperty,
                             ObjectProperty, StringProperty)
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import AsyncImage, Image
from kivy.uix.popup import Popup
from kivy.uix.widget import Widget
from kivy.utils import get_hex_from_color
from kivymd.uix.behaviors.ripplebehavior import CircularRippleBehavior
from kivymd.theming import ThemeManager
from kivymd.uix.button import (MDFillRoundFlatButton, MDFlatButton,
                               MDIconButton, MDRaisedButton, MDRoundFlatButton)
from kivymd.uix.dialog import MDDialog
from kivymd.uix.dropdownitem import MDDropDownItem
from kivymd.uix.expansionpanel import MDExpansionPanel
from kivymd.uix.label import MDLabel
from kivymd.uix.list import ILeftBody, ILeftBodyTouch, IRightBodyTouch
from kivymd.uix.menu import MDDropdownMenu
from kivymd.uix.selectioncontrol import MDCheckbox
from kivymd.uix.tab import MDTabsBase
from kivymd.uix.textfield import MDTextField
from kivymd.uix.useranimationcard import MDUserAnimationCard
from plyer import gps, stt
from screens import Screens
from kivymd.uix.accordion import MDAccordionSubItem
from kivymd.uix.accordion import MDAccordionItem
from kivymd.uix.accordion import MDAccordion
from kivy.core.window import Window
from kivy.utils import platform as core_platform

Window.keyboard_anim_args = {'d': .2, 't': 'in_out_expo'}
Window.softinput_mode = "below_target"
sys.path.append(os.path.abspath(__file__).split('demos')[0])

google_key = "AIzaSyC3Zs_Aln2ewhniP1cvuCb5hajMZyYbJxE"
gmaps = googlemaps.Client(key=google_key)
os.environ['SSL_CERT_FILE'] = certifi.where()
os.environ['REQUESTS_CA_BUNDLE'] = certifi.where()

''''
if core_platform == "android":
    from jnius import autoclass
    DisplayMetrics = autoclass('android.util.DisplayMetrics')
    metrics = DisplayMetrics()
    metrics.setToDefaults()

    print("DPI")
    print(metrics.densityDpi)
    print("DPIX")
    print (str(metrics.xdpi))
    print("DPIY")
    print (str(metrics.ydpi))
    print("DPIX")
    print (str(metrics.widthPixels))
    print("DPIY")
    print(str(metrics.heightPixels))
else:
    print("WindowSize"+str(Window.size))
'''

main_widget_kv = """
#:import get_hex_from_color kivy.utils.get_hex_from_color
#:import MDRoundFlatButton kivymd.uix.button.MDRoundFlatButton
#:import NavigationLayout kivymd.uix.navigationdrawer.NavigationLayout
#:import ThreeLineListItem kivymd.uix.list.ThreeLineListItem
#:import MDCheckbox kivymd.uix.selectioncontrol.MDCheckbox
#:import MDAccordion kivymd.uix.accordion.MDAccordion
#:import MDAccordionItem kivymd.uix.accordion.MDAccordionItem
#:import MDAccordionSubItem kivymd.uix.accordion.MDAccordionSubItem
#:import MDToolbar kivymd.uix.toolbar.MDToolbar
#:import MDTextField kivymd.uix.textfield.MDTextField
#:import MDTextFieldRound kivymd.uix.textfield.MDTextFieldRound
#:set color_shadow [0, 0, 0, .2980392156862745]
#:import stt plyer.stt
#:import threading threading
#:import MDRaisedButton kivymd.uix.button.MDRaisedButton
#:import MDSpinner kivymd.uix.spinner.MDSpinner

<NewGpswordPopup>:#GPS新增單字介面 
    separator_color:[0,0,0,0]
    title_align:'center'
    size_hint: None, None
    size: app.root.width/1.1, app.root.height/1.1
    auto_dismis: False
    BoxLayout:               
        spacing:dp(40)     
        orientation: 'vertical'
        padding:[20,0,20,0]
        pos_hint: {'center_x': .5}
        BoxLayout:
            MDLabel:
                font_style: 'H5'
                text: "編號"                
                theme_text_color: 'Custom'
                text_color: (1,1,1,1)
                size_hint:None,None
                pos_hint: {'center_y': .5}
            MDTextField:
                disabled:True
                size_hint:None,None
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                size_hint_x:0.7                            
                id:NewGpsword_Num
                theme_text_color: 'Custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                color_mode: 'custom'
                line_color_normal:(1,1,1,1)
                line_color_focus:(1,1,1,1) 
                text:root.num
                pos_hint: {'center_y': .5}
        BoxLayout:
            spacing:dp(40)     
            MDLabel:                             
                font_style: 'H5'                   
                theme_text_color: 'Custom'
                text: "單字種類"
                text_color: (1,1,1,1)
                pos_hint: {'center_y': .3}     
                size_hint:None,None           
            MDRaisedButton:              
                id:ctype
                pos_hint: {'center_y': .5}                
                size_hint_x:0.4
                theme_text_color: 'Primary'
                text: root.ctype    
                on_release:
                    MDDropdownMenu(items=root.newgpswordtype, width_mult=3).open(self)                 
            MDRaisedButton:                   
                id:etype
                pos_hint: {'center_y': .5}         
                size_hint_x:0.4
                theme_text_color: 'Primary'
                text:  root.etype 
                on_release:
                    MDDropdownMenu(items=root.newgpswordtype, width_mult=3).open(self)     
        BoxLayout:
            MDLabel:
                font_style: 'H5'
                text: "單字"                
                theme_text_color: 'Custom'
                text_color: (1,1,1,1)
                size_hint:None,None
                pos_hint: {'center_y': .5}
            MDTextField:
                size_hint:None,None
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                size_hint_x:0.7                            
                id:NewGpsword_Word
                theme_text_color: 'Custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                color_mode: 'custom'
                line_color_normal:(1,1,1,1)
                line_color_focus:(1,1,1,1) 
                text:root.word
                pos_hint: {'center_y': .5}
        BoxLayout:
            MDLabel:
                font_style: 'H5'
                text: "中文"                
                theme_text_color: 'Custom'
                text_color: (1,1,1,1)
                size_hint:None,None
                pos_hint: {'center_y': .5}
            MDTextField:
                size_hint:None,None
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                size_hint_x:0.7                            
                id:NewGpsword_Chinese
                theme_text_color: 'Custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                color_mode: 'custom'
                line_color_normal:(1,1,1,1)
                line_color_focus:(1,1,1,1) 
                text:root.chinese
                pos_hint: {'center_y': .5}
        
        BoxLayout:
            spacing:dp(40)     
            MDLabel:                             
                font_style: 'H5'                   
                theme_text_color: 'Custom'
                text: "詞性"            
                text_color: (1,1,1,1)
                pos_hint: {'center_y': .3}     
                size_hint:None,None           
                    
            MDRaisedButton:                   
                id:pos
                pos_hint: {'center_y': .5}         
                size_hint_x:0.6
                theme_text_color: 'Primary'
                text: root.wordpos
                on_release:
                    MDDropdownMenu(items=root.wordposs, width_mult=3).open(self)
        BoxLayout:
            MDLabel:
                font_style: 'H5'
                text: "範例"                
                theme_text_color: 'Custom'
                text_color: (1,1,1,1)
                size_hint:None,None
                pos_hint: {'center_y': .5}
            MDTextField:
                size_hint:None,None
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                size_hint_x:0.7                            
                id:NewGpsword_Example
                theme_text_color: 'Custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                color_mode: 'custom'
                line_color_normal:(1,1,1,1)
                line_color_focus:(1,1,1,1) 
                text:root.example
                pos_hint: {'center_y': .5}
        BoxLayout:
            MDLabel:
                font_style: 'H5'
                text: "範例中文"                
                theme_text_color: 'Custom'
                text_color: (1,1,1,1)
                size_hint:None,None
                pos_hint: {'center_y': .5}
            MDTextField:
                id:NewGpsword_Cexample
                size_hint:None,None
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                size_hint_x:0.7                            
                theme_text_color: 'Custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                color_mode: 'custom'
                line_color_normal:(1,1,1,1)
                line_color_focus:(1,1,1,1) 
                text:root.cexample
                pos_hint: {'center_y': .5}
        BoxLayout:
            size_hint: None, None
            size: self.minimum_size
            spacing: dp(10)
            pos_hint: {'center_x': .5}
            MDFillRoundFlatButton:
                id:newcgbtn
                text: "新增"
                pos_hint: {'center_x': .5}
                theme_text_color :'Custom'
                background_color: (1,1,1,1)
                text_color:(1,1,1,1)
                ripple_colors:(1,1,1,1)
                on_release:root.insertnewgpsword(NewGpsword_Num.text,NewGpsword_Word.text,NewGpsword_Chinese.text,ctype.text,etype.text,pos.text,NewGpsword_Example.text,NewGpsword_Cexample.text)
            MDFillRoundFlatButton:
                text: "刪除"
                pos_hint: {'center_x': .5}
                theme_text_color :'Custom'
                background_color: (1,1,1,1)
                text_color:(1,1,1,1)
                ripple_colors:(1,1,1,1)
                on_release:root.delnewgpsword(NewGpsword_Num.text,NewGpsword_Word.text,NewGpsword_Chinese.text,ctype.text,etype.text,pos.text,NewGpsword_Example.text,NewGpsword_Cexample.text)
            MDRoundFlatButton:
                id:signup
                text: "關閉"
                pos_hint: {'center_x': .5}
                theme_text_color :'Custom'
                background_color: (1,1,1,1)
                text_color:(1,1,1,1)
                ripple_colors:(1,1,1,1)
                on_release:
                    root.dismiss()
            Widget:
                size_hint_y: None
                height: dp(10) 
<LineEllipse1>: #連連看畫線
    canvas:
        Color:
            rgba: 1, .1, .1, .9
        Line:
            width: 2
            points:app.pos

<speaktestpopup>: #語音辨識視窗
    title: "語音辨識"
    separator_color:[0,0,0,0]
    title_align:'center'
    size_hint: None, None
    size: app.root.width/1.1, app.root.height/1.1
    auto_dismis: False

    ScrollView:
        BoxLayout:
            id:layout
            orientation: 'vertical'
            size_hint_y: None
            height: self.minimum_height
            padding: dp(20)
            spacing: 10
            MDLabel:
                text:root.word
                pos_hint: {'center_x': .5}
                halign: 'center'
                font_style: 'H4'

            MDTextField:
                multiline: True    
                id: results
                hint_text: '精準結果'
            MDTextField:
                multiline: True
                id: partial
                hint_text: '模糊結果'
            MDRaisedButton:
                id: start_button
                text: '開始聆聽'
                on_release: root.start_listening()
                pos_hint: {'center_x': .5}

<SignupPopup>: #註冊彈出式視窗
    answer: "123"
    title: "註冊"
    separator_color:[0,0,0,0]
    title_align:'center'
    size_hint: None, None
    size: app.root.width/1.1, app.root.height/1.1
    auto_dismis: False

    ScrollView:
        do_scroll_x: False
        BoxLayout:           
            spacing: dp(1)
            orientation: 'vertical'
            padding:[20,20,20,20]
            pos_hint: {'center_x': .5}
            MDLabel:
                font_style: 'H5'
                text: "帳號"                
                theme_text_color: 'Custom'
                text_color: (1,1,1,1)
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                id:Signup_account
                theme_text_color: 'Custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                line_color_normal:(1,1,1,1)
                line_color_focus:(1,1,1,1) 
                max_text_length: 10               
            MDLabel:
                font_style: 'H5'
                theme_text_color: 'Custom'
                text: "使用者名稱"
                text_color: (1,1,1,1)
                password: True
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                id:Signup_username
                color_mode: 'custom'
                line_color_normal:(1,1,1,1)
                helper_text_mode: "on_focus"
                helper_text: ""
                line_color_focus:(1,1,1,1)     
                max_text_length: 20
            MDLabel:
                font_style: 'H5'
                theme_text_color: 'Custom'
                text: "密碼"
                text_color: (1,1,1,1)       
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                id:Signup_password
                color_mode: 'custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                line_color_focus:(1,1,1,1)  
                line_color_normal:(1,1,1,1)
                password: True                
                max_text_length: 20     
            MDLabel:
                font_style: 'H5'
                theme_text_color: 'Custom'
                text: "確認密碼"
                text_color: (1,1,1,1)
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                id:Signup_repassword
                color_mode: 'custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                line_color_focus:(1,1,1,1)  
                password: True
                max_text_length: 20             
                line_color_normal:(1,1,1,1)
            MDLabel:
                font_style: 'H5'
                theme_text_color: 'Custom'
                text: "信箱"
                text_color: (1,1,1,1)
                password: True
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1                    
                id:Signup_email
                color_mode: 'custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                max_text_length: 10
                line_color_focus:(1,1,1,1)  
                line_color_normal:(1,1,1,1)
                max_text_length:50             
            MDLabel:
                font_style: 'H5'
                theme_text_color: 'Custom'
                text: "電話"
                text_color: (1,1,1,1)
                password: True
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                id:Signup_phone
                color_mode: 'custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                line_color_focus:(1,1,1,1)     
                line_color_normal:(1,1,1,1)
                hint_text_color:(1,1,1,1)
                max_text_length: 10
            BoxLayout:
                size_hint: None, None
                size: self.minimum_size
                spacing: dp(10)
                pos_hint: {'center_x': .5}
                MDFillRoundFlatButton:
                    id:signup
                    text: "註冊"
                    pos_hint: {'center_x': .5}
                    theme_text_color :'Custom'
                    background_color: (1,1,1,1)
                    text_color:(1,1,1,1)
                    ripple_colors:(1,1,1,1)
                    on_release:
                        app.Signup(root,Signup_account,Signup_username,Signup_password,Signup_repassword,Signup_email,Signup_phone)
                MDRoundFlatButton:
                    id:signup
                    text: "關閉"
                    pos_hint: {'center_x': .5}
                    theme_text_color :'Custom'
                    background_color: (1,1,1,1)
                    text_color:(1,1,1,1)
                    ripple_colors:(1,1,1,1)
                    on_release:
                        root.dismiss()
                Widget:
                    size_hint_y: None
                    height: dp(10) 

<loadingscreen>:
    answer: "123"
    title: root.title
    title_align:'center'
    size_hint: None, None
    auto_dismiss:False
    separator_color:[0,0,0,0]
    BoxLayout:
        size_hint_y: None
        size_hint_x: .8
        height: spinner.height + dp(20)
        spacing: dp(10)
        padding: dp(10)
        MDSpinner
            id: spinner
            size_hint: None, None
            size: dp(46), dp(46)
            color: 1, 1, 1, 1
        MDLabel:
            id: label_download
            shorten: True
            max_lines: 1
            halign: 'left'
            valign: 'top'
            text_size: self.width, None
            size_hint_y: None
            height: spinner.height
            size_hint_x: .8
            text:"載入中....."
        Widget:
            size_hint_x: .1               
        
<TextPopup>:
    answer: "123"
    title: root.title
    separator_color:[0,0,0,0]
    title_align:'center'
    size_hint: None, None
    size: app.root.width/2, app.root.height/2
    auto_dismis: False

    BoxLayout:
        orientation: 'vertical'
        OneLineListItem:
            text: "單字測驗"
            on_release:
                app.show_WordtestPage()
                root.dismiss()
        OneLineListItem:
            text: "拼字測驗"
            on_release:
                app.show_SpellWordtestPage()
                root.dismiss()
        OneLineListItem:
            text: "聽力測驗"
            on_release:
                app.show_LisitingtestPage()
                root.dismiss()
        OneLineListItem:
            text: "圖像測驗"
            on_release:
                app.show_PicturetestPage()
                root.dismiss()
        OneLineListItem:
            text: "One-line item"

<LearningPopup>:
    title: root.title
    separator_color:[0,0,0,0]
    title_align:'center'
    size_hint: None, None
    size: app.root.width/1.5, app.root.height/1.5
    auto_dismis: False

    BoxLayout:
        orientation: 'vertical'
        OneLineListItem:
            text: "GPS場景單字"
            on_release:
                app.show_GpstestPage()
                app.set_title_toolbar(self.text)
                root.dismiss()
        OneLineListItem:
            text: "小學英文單字"
            on_release:
                app.show_vocablaryLA(app,self.text)
                app.set_title_toolbar(self.text)
                root.dismiss()
        OneLineListItem:
            text: "全民英檢初級單字"
            on_release:
                app.show_vocabluaryGEPTLA(app,self.text)
                app.set_title_toolbar(self.text)
                root.dismiss()
        OneLineListItem:
            text: "全民英檢中級單字"
            on_release:
                app.show_vocabluaryGEPTLB(app,self.text)
                app.set_title_toolbar(self.text)
                root.dismiss()
        OneLineListItem:
            text: "全民英檢中高級單字"
            on_release:
                app.show_vocabluaryGEPTLC(app,self.text)
                app.set_title_toolbar(self.text)
                root.dismiss()
        OneLineListItem:
            text: "多益英文單字"
            on_release:
                app.show_Toeic(app,self.text)
                app.set_title_toolbar(self.text)
                root.dismiss()
        OneLineListItem:
            text: "單字搜尋"
            on_release:
                app.show_WordSearchPage()
                app.set_title_toolbar(self.text)
                root.dismiss()
        OneLineListItem:
            text: "瀏覽已標記單字"
            on_release:
                app.show_cards()
                app.set_title_toolbar(self.text)
                root.dismiss()

<CircleWidget@Widget>
    size_hint: None, None
    size: dp(20), dp(20)

    canvas:
        Color:
            rgba: app.theme_cls.primary_color
        Ellipse:
            pos: self.pos
            size: dp(20), dp(20)

<relationworditem@BoxLayout>:
    size_hint_y: None
    height: self.minimum_height
    MDRaisedButton:
        text: root.word  
        on_release:               
            app.user_animation_card.dismiss()
            app.show_user_example_animation_card(self.text,relationword=True)  

<ContentForAnimCard>: #單字卡內容
    orientation: 'vertical'
    size_hint_y: None
    height: self.minimum_height
    BoxLayout:
        padding: dp(50)
        size_hint_y: None
        height: self.minimum_height
        Widget:
        MDLabel:
            font_style: 'Caption'
            theme_text_color: 'Primary'
            text: root.id
            halign: 'center'
            font_size:root.width/30
        Widget:

    
    BoxLayout:
        size_hint_y: None
        spacing:dp(50)
        height: self.minimum_height
        Widget:
        MDRoundFlatButton:
            text: "發音"
            on_press: 
                mythread = threading.Thread(target=app.listening,args=(root.id,))
                mythread.start()   
        MDRoundFlatButton:
            text: "練習發音"
            on_press: root.on_call_testpopup(root.id) 
        Widget:
    BoxLayout:       
        padding:[0,20,0,0]
        height:dp(200)
        size_hint:1,None
        ImageButton:   
            id:imagebtn
            pos_hint: {'center_x': .8}         
    BoxLayout:
        orientation: 'vertical'
        size_hint_y: None
        height: self.minimum_height
        TwoLineIconListItem:
            text: "中文"
            on_press: root.callback(self.text)
            secondary_text:
                "[color=%s]%s[/color]" \
                % (get_hex_from_color(app.theme_cls.primary_color),root.chinese)
            IconLeftSampleWidget:
                icon: 'camera-front-variant'

        TwoLineIconListItem:
            text: "詞性"
            on_press: root.callback(self.text)
            secondary_text:
                "[color=%s]%s[/color]" \
                % (get_hex_from_color(app.theme_cls.primary_color),root.part)
            IconLeftSampleWidget:
                icon: 'phone'

        ThreeLineIconListItem:
            text: "範例"
            on_press: root.callback(self.text)
            secondary_text:
                "[color=%s]%s\\n%s[/color]" % (get_hex_from_color(app.theme_cls.primary_color),root.example,root.cexample)
            IconLeftSampleWidget:
                id: li_icon_3
                icon: 'sd'
        
        OneLineAvatarIconListItem:
            text: "標記單字"            
            IconLeftSampleWidget:
                id: li_icon_3
                icon: 'bookmark-check'
            IconRightSampleWidget:  
                id:test      
                on_release:
                    root.savetagword(self,root.id,root.range)
    BoxLayout:
        padding: dp(30)
        size_hint_y: None
        height: self.minimum_height
        Widget:
        MDLabel:
            visible: False 
            opacity: 1 if self.visible else 0
            id:relationwordlabel
            font_style: 'Caption'
            theme_text_color: 'Primary'
            text: "相關單字列表"
            halign: 'center'
            font_size:root.width/30
        Widget:
            
    GridLayout:            
        id:tet
        padding:[20,20,20,0]
        size_hint_y: None
        height: self.minimum_height
        spacing:dp(10)
            
NavigationLayout:
    id: nav_layout
    BoxLayout:
        MDAccordion:
            id:accordiontest
            orientation: 'vertical'
            size_hint_x: None
            width: dp(320)
            MDAccordionItem:
                title:'登入'
                font_style:"H5"
                icon: 'account-plus'                                                                  
                BoxLayout:
                    orientation:'vertical'
                    size: dp(330), dp(300)
                    size_hint: None, None
                    ImageButton:
                        source:'https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/eye-24-256.png'                            
                    BoxLayout:
                        spacing:dp(30)
                        size_hint_y: 0.3                        
                        Widget:
                        MDRaisedButton: 
                            text:"首頁"
                            pos_hint: {'center_y': .5}
                            size_hint:None,None
                            width: dp(200)
                            on_release:
                                app.show_MainPage()
                                app.set_title_toolbar(self.text)
                                app.root.toggle_nav_drawer()
                        MDRaisedButton:
                            text:"登出"
                            pos_hint: {'center_y': .5}
                            size_hint:None,None
                            width: dp(200)
                            on_release:
                                app.show_homePage()
                                app.root.toggle_nav_drawer()
                        Widget:
            MDAccordionItem:
                title:'單字學習'
                icon: 'earth'
                font_style:"H5"
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "GPS場景單字"
                    on_release:                    
                        app.show_GpstestPage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "GPS氣象單字"
                    on_release:
                        app.show_WeatherPage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "小學英文單字"
                    on_release:
                        app.show_vocablaryLA(app,self.text)
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "全民英檢初級單字"
                    on_release:
                        app.show_vocabluaryGEPTLA(app,self.text)
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"
                    text: "全民英檢中級單字"
                    on_release:
                        app.show_vocabluaryGEPTLB(app,self.text)
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"
                    text: "全民英檢中高級單字"
                    on_release:
                        app.show_vocabluaryGEPTLC(app,self.text)
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()     
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"
                    text: "節日單字"
                    on_release:
                        app.show_festivalwordPage() 
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "單字搜尋"
                    on_release:
                        app.show_WordSearchPage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()    
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "瀏覽已標記單字"
                    on_release:
                        app.show_cards()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
            MDAccordionItem:
                title:'單字測驗專區'
                font_style:"H5"
                icon: 'clipboard-text'
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "單字測驗"
                    on_release:
                        app.show_WordtestPage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "拼字測驗"
                    on_release:
                        app.show_SpellWordtestPage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "聽力測驗"
                    on_release:
                        app.show_LisitingtestPage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "重組測驗"
                    on_release:
                        app.show_RearrangementPage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()     
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "句子測驗"
                    on_release:
                        app.show_sentencetestPage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "圖像測驗"
                    on_release:
                        app.show_PicturetestPage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()                         
                
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "連連看測驗"
                    on_release:
                        app.show_ConnecttestPage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()   
            MDAccordionItem:
                title:'常錯分析'
                font_style:"H5"
                icon: 'account'
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "常錯單字"
                    on_release:
                        app.show_oftenwrongword()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "作答紀錄"
                    on_release:
                        app.show_wordtesthistroyPage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()               
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "瀏覽常錯關聯圖"
                    on_release:
                        app.show_CoceptMapPage()
                        app.set_title_toolbar(self.text)                        
                        app.root.toggle_nav_drawer()
            MDAccordionItem:                
                title:'儀錶板'
                font_style:"H5"
                icon: 'account'
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "個人設定"
                    on_release:
                        app.show_ProfilePage()
                        app.root.toggle_nav_drawer()                        
                MDAccordionSubItem:
                    height:dp(50)
                    font_style: "H6"                
                    text: "GPS單字管理"
                    on_release:
                        app.show_QusetionAdmPage()
                        app.root.toggle_nav_drawer()
                
    BoxLayout:
        orientation: 'vertical'
        MDToolbar:
            id: toolbar
            title: '無所不在英文挑戰系統'
            md_bg_color: app.theme_cls.primary_color
            background_palette: 'Primary'
            background_hue: '500'
            left_action_items: [['menu', lambda x:app.toast("登入後以繼續")]]

        ScreenManager:
            id: scr_mngr
            Screen:
                name:"Home"
                BoxLayout:
                    id :Login_lay
                    orientation: 'vertical'
                    canvas:
                        Rectangle:
                            size: self.size
                            pos: self.pos
                            source: './assets/loginimg.png'                        
                    BoxLayout:                                          
                        id: box_top
                        orientation: 'vertical'
                        size_hint: None, None
                        size: self.minimum_size
                        pos_hint: {'center_x': .5, 'top': 1}
                        padding: dp(5)
                        BoxLayout:
                            spacing: dp(5)
                            size_hint_y: None
                            height: dp(20)

                            CircleWidget:
                            CircleWidget:
                            Widget:
                        Widget:
                            size_hint_y: None
                            height: dp(5)

                        Label:
                            markup: True
                            text: '[size=35]             無所不在[/size]'
                            size_hint: None, None
                            size: self.texture_size
                            bold: True
                        
                        BoxLayout:
                            spacing: dp(5)
                            size_hint: None, None
                            size: self.minimum_size

                            Label:
                                markup: True
                                text: '[size=50]英文單字測驗系統[/size]'
                                size_hint: None, None
                                size: self.texture_size
                                bold: True
                            CircleWidget:
             
                    BoxLayout:
                        canvas.before:
                            Color: 
                                rgba: 1,1,1, 1
                            Line:
                                width: 1
                                rectangle: self.x + 10, self.y + 10,self.width - 20,self.height - 20
                        id:box_top
                        spacing: dp(1)
                        orientation: 'vertical'
                        padding:[20,20,20,20]
                        pos_hint: {'center_x': .5}
                        MDLabel:
                            font_style: 'H4'
                            text: "帳號"
                            theme_text_color: 'Custom'
                            text_color: (1,1,1,1)
                        MDTextField:
                            canvas.before:
                                Color:
                                    rgba: 1, 1, 1, 1
                            id:login_account
                            color_mode: 'custom'
                            helper_text_mode: "on_focus"
                            helper_text: ""
                            text:"tet"
                            line_color_focus:(1,1,1,1)
                            line_color_normal:(1,1,1,1)
                        MDLabel:
                            font_style: 'H4'
                            theme_text_color: 'Custom'
                            text: "密碼"
                            text_color: (1,1,1,1)                            
                        MDTextField:
                            canvas.before:
                                Color:
                                    rgba: 1, 1, 1, 1
                            id:login_password
                            color_mode: 'custom'
                            helper_text_mode: "on_focus"
                            helper_text: ""
                            line_color_focus:(1,1,1,1)
                            line_color_normal:(1,1,1,1)
                            password: True
                            text:"123"
                        BoxLayout:
                            size_hint: None, None
                            size: self.minimum_size
                            spacing: dp(10)
                            pos_hint: {'center_x': .5}
                            MDRoundFlatButton:                                
                                id:sigbtn
                                text: "註冊"
                                pos_hint: {'center_x': .5}
                                color_mode: 'custom'
                                theme_text_color :'Custom'
                                background_color: (1,1,1,1)
                                text_color:(1,1,1,1)
                                on_release:app.on_call_Signuppopup()
                            MDFillRoundFlatButton:
                                text: "登入"
                                pos_hint: {'center_x': .5}
                                theme_text_color :'Custom'
                                background_color: (1,1,1,1)
                                text_color:(0,0,0,1)
                                md_bg_color: (1,1,1,1)
                                on_release:
                                    print(app.theme_cls.primary_color)
                                    app.Login(login_account.text,login_password.text,toolbar,accordiontest)
                            Widget:
                                size_hint_y: None
                                height: dp(10) 
                    BoxLayout:
                        size_hint_y: None
                        height: dp(50)
"""



class ImageButton(ButtonBehavior, AsyncImage):
    pass

class IconLeftSampleWidget(ILeftBodyTouch, MDIconButton):
    pass

class IconRightSampleWidget(IRightBodyTouch, MDCheckbox):
    pass

class relationworditem(BoxLayout):
    word = StringProperty()
    def __init__(self,word, **kwargs):        
        super(relationworditem, self).__init__(**kwargs)
        self.word=word

class ContentForAnimCard(BoxLayout):
    callback = ObjectProperty(lambda x: None)
    chinese = StringProperty()
    part =  StringProperty()
    example = StringProperty()
    cexample = StringProperty()
    relationword=BooleanProperty()
    def __init__(self,range,chinese,part,example,cexample,relationword, **kwargs):     
        super(ContentForAnimCard, self).__init__(**kwargs)
        if relationword == True:
            self.ids.relationwordlabel.visible=True
            word=self.id
            url = "https://www.shutterstock.com/zh-Hant/search/"+word
            r = requests.get(
                url, headers={
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'
                })
            soup = BeautifulSoup(r.text, 'html.parser')
            b_tag = soup.find("img", class_="z_g_j z_g_a z_g_b")  
            try:
                self.ids.imagebtn.source = b_tag.get('src')
            except:
                self.ids.imagebtn.source="http://"
            r = requests.get('https://relatedwords.org/api/related?term='+word)
            data =r.json()
            relationwordlsit=[]
            for i in data:
                if " " not in i['word']:
                    relationwordlsit.append(i['word'])
                if len(relationwordlsit) >=30:
                    break
            print(word)
            totelcols=1
            totelrows=1

            height=0
            width=0
            
            if core_platform == "android":
                from jnius import autoclass
                DisplayMetrics = autoclass('android.util.DisplayMetrics')
                metrics = DisplayMetrics()
                metrics.setToDefaults()
                for i in relationwordlsit:
                    if width>metrics.xdpi:
                        totelrows+=1
                    else:
                        width+= (len(i)*20)+40
                        totelcols+=1
            else:
                for i in relationwordlsit:
                    if width>Window.size[0]:
                        totelrows+=1
                    else:
                        width+= (len(i)*20)+40
                        totelcols+=1

            self.ids.tet.cols=totelcols-1
            self.ids.tet.rows=totelrows
            
            for i in relationwordlsit:
                self.ids.tet.add_widget(relationworditem(word=str(i)))
        self.chinese = chinese
        self.part = part
        self.example=example
        self.cexample =cexample
        self.range = range
        self.BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        self.db_path = os.path.join(self.BASE_DIR, "database.db")
        self.conn = sqlite3.connect(self.db_path)
        self.c = self.conn.cursor()
        cursor = self.c.execute(
            "SELECT count(*) from 'tag_Vocablary' where tagVocablary_Word = '"+self.id+"'")
        for row in cursor:
            count=(str(row[0]))
        if count =="0":
            self.ids.test.active = False
        else:
            self.ids.test.active = True       

    def on_call_testpopup(self,words): #語音彈出式視窗
        poti = speaktestpopup(word=words,background = 'atlas://data/images/defaulttheme/vkeyboard_key_down')
        poti.open()

    def savetagword(self,item,word,range): 
        self.BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        self.db_path = os.path.join(self.BASE_DIR, "database.db")
        self.conn = sqlite3.connect(self.db_path)
        self.c = self.conn.cursor()
        cursor = self.c.execute(
            "SELECT * from 'Vocablary' where Vocablary_Word = '"+word+"'")
        data1=word
        data2=range
        for row in cursor:
            data3=(str(row[3]))
            data4=(str(row[4]))
            data5=(str(row[5]))
            data6=(str(row[6]))
        if (item.active):            
            self.c.execute("INSERT INTO tag_Vocablary VALUES ('"+data1+"', '"+data2+"', '"+data3+"', '"+data4+"','"+data5+"','"+data6+"');");
        else:
            self.c.execute("delete from  tag_Vocablary where tagVocablary_Word = '"+data1+"' and tagVocablary_range = '"+data2+"'")
        self.conn.commit()
        self.conn.close()

class KitchenSink(MDApp, Screens):    
    title = "無所不在英文單字測驗系統"
    user_animation_card = None
    def __init__(self, **kwargs):        
        super(KitchenSink, self).__init__(**kwargs)
    
        self.theme_cls.primary_palette  ="Orange"
        self.questionadm = [
                     {'viewclass': 'MDRaisedButton',
                        'text': '選擇題',
                        'on_release': lambda : self.on_call_choicequestionpopup()},
                        {'viewclass': 'MDRaisedButton',
                        'text': '克漏字',
                        'on_release': lambda :self.on_call_leakwordpopup()},               
        ]      
 
        # self.item1=None
      
        self.userdata=[]
        self.matplotliburldata={}
        self.Window = Window
        self.user_card = None
        self.cards_created = None
        self.user_animation_card = None
        
        self.ok_cancel_dialog = None
        self.BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        self.db_path = os.path.join(self.BASE_DIR, "database.db")
        self.conn = sqlite3.connect(self.db_path)
        self.size = Window.size
        self.playcount=0
        self.url=""


        self.gpswordadmin = [
                {'viewclass': 'MDRaisedButton',
                'text': 'SPA',
                'on_release': lambda : self.loadgpsword("spa")},
                {'viewclass': 'MDRaisedButton',
                'text': '五金店',
                'on_release': lambda :self.loadgpsword("hardware store")}, 
                {'viewclass': 'MDRaisedButton',
                'text': '住宿',
                'on_release': lambda :self.loadgpsword("lodging")},
                {'viewclass': 'MDRaisedButton',
                'text': '便利店',
                'on_release': lambda :self.loadgpsword("convenience store")},
                {'viewclass': 'MDRaisedButton',
                'text': '停車處',
                'on_release': lambda :self.loadgpsword("parking")},
                {'viewclass': 'MDRaisedButton',
                'text': '健康設施',
                'on_release': lambda :self.loadgpsword("health")},
                {'viewclass': 'MDRaisedButton',
                'text': '公園',
                'on_release': lambda :self.loadgpsword("park")},
                {'viewclass': 'MDRaisedButton',
                'text': '公車站',
                'on_release': lambda :self.loadgpsword("bus station")},
                {'viewclass': 'MDRaisedButton',
                'text': '加油站',
                'on_release': lambda :self.loadgpsword("gas station")},
                {'viewclass': 'MDRaisedButton',
                'text': '博物館',
                'on_release': lambda :self.loadgpsword("museum")},
                {'viewclass': 'MDRaisedButton',
                'text': '咖啡店',
                'on_release': lambda :self.loadgpsword("cafe")},
                {'viewclass': 'MDRaisedButton',
                'text': '商店',
                'on_release': lambda :self.loadgpsword("store")},
                {'viewclass': 'MDRaisedButton',
                'text': '地鐵站',
                'on_release': lambda :self.loadgpsword("subway station")},  
                {'viewclass': 'MDRaisedButton',
                'text': '夜店',
                'on_release': lambda :self.loadgpsword("night club")},  
                {'viewclass': 'MDRaisedButton',
                'text': '家居用品店',
                'on_release': lambda :self.loadgpsword("home goods store")},  
                {'viewclass': 'MDRaisedButton',
                'text': '大學',
                'on_release': lambda :self.loadgpsword("university")},  
                {'viewclass': 'MDRaisedButton',
                'text': '學校',
                'on_release': lambda :self.loadgpsword("school")},  
                {'viewclass': 'MDRaisedButton',
                'text': '寺廟',
                'on_release': lambda :self.loadgpsword("place of worship")},   
                {'viewclass': 'MDRaisedButton',
                'text': '小吃',
                'on_release': lambda :self.loadgpsword("food")},   
                {'viewclass': 'MDRaisedButton',
                'text': '巴士站',
                'on_release': lambda :self.loadgpsword("transit station")},   
                {'viewclass': 'MDRaisedButton',
                'text': '房地產仲介',
                'on_release': lambda :self.loadgpsword("real estate agency")},    
                {'viewclass': 'MDRaisedButton',
                'text': '批發',
                'on_release': lambda :self.loadgpsword("storage")},                                                                                                                                                                                                                                                                                               
                {'viewclass': 'MDRaisedButton',
                'text': '政府',
                'on_release': lambda :self.loadgpsword("city hall")},   
                {'viewclass': 'MDRaisedButton',
                'text': '政治設施',
                'on_release': lambda :self.loadgpsword("political")},  
                {'viewclass': 'MDRaisedButton',
                'text': '教會',
                'on_release': lambda :self.loadgpsword("church")},  
                {'viewclass': 'MDRaisedButton',
                'text': '旅行社',
                'on_release': lambda :self.loadgpsword("travel agency")},    
                {'viewclass': 'MDRaisedButton',
                'text': '景點',
                'on_release': lambda :self.loadgpsword("point of interest")},     
                {'viewclass': 'MDRaisedButton',
                'text': '服飾店',
                'on_release': lambda :self.loadgpsword("clothing store")},     
                {'viewclass': 'MDRaisedButton',
                'text': '機場',
                'on_release': lambda :self.loadgpsword("airport")},                                                                                                                                                   
                {'viewclass': 'MDRaisedButton',
                'text': '機構',
                'on_release': lambda :self.loadgpsword("establishment")}, 
                {'viewclass': 'MDRaisedButton',
                'text': '汽車保養廠',
                'on_release': lambda :self.loadgpsword("car repair")}, 
                {'viewclass': 'MDRaisedButton',
                'text': '消防局',
                'on_release': lambda :self.loadgpsword("fire station")}, 
                {'viewclass': 'MDRaisedButton',
                'text': '火車站',
                'on_release': lambda :self.loadgpsword("train station")},
                {'viewclass': 'MDRaisedButton',
                'text': '珠寶店',
                'on_release': lambda :self.loadgpsword("jewelry store")},      
                {'viewclass': 'MDRaisedButton',
                'text': '百貨公司',
                'on_release': lambda :self.loadgpsword("department store")},      
                {'viewclass': 'MDRaisedButton',
                'text': '美容院',
                'on_release': lambda :self.loadgpsword("beauty salon")},      
                {'viewclass': 'MDRaisedButton',
                'text': '自動提款機',
                'on_release': lambda :self.loadgpsword("atm")},   
                {'viewclass': 'MDRaisedButton',
                'text': '花店',
                'on_release': lambda :self.loadgpsword("florist")},   
                {'viewclass': 'MDRaisedButton',
                'text': '藥房',
                'on_release': lambda :self.loadgpsword("pharmacy")},   
                {'viewclass': 'MDRaisedButton',
                'text': '警察局',
                'on_release': lambda :self.loadgpsword("police")},    
                {'viewclass': 'MDRaisedButton',
                'text': '購物中心',
                'on_release': lambda :self.loadgpsword("shopping mall")},   
                {'viewclass': 'MDRaisedButton',
                'text': '道路',
                'on_release': lambda :self.loadgpsword("route")},   
                {'viewclass': 'MDRaisedButton',
                'text': '酒吧',
                'on_release': lambda :self.loadgpsword("bar")},   
                {'viewclass': 'MDRaisedButton',
                'text': '銀行',
                'on_release': lambda :self.loadgpsword("bank")},         
                {'viewclass': 'MDRaisedButton',
                'text': '雜貨店或便利商店',
                'on_release': lambda :self.loadgpsword("grocery or supermarket")},   
                {'viewclass': 'MDRaisedButton',
                'text': '電子產品商店',
                'on_release': lambda :self.loadgpsword("electronics store")},   
                {'viewclass': 'MDRaisedButton',
                'text': '電影院',
                'on_release': lambda :self.loadgpsword("movie theater")},   
                {'viewclass': 'MDRaisedButton',
                'text': '頭髮護理',
                'on_release': lambda :self.loadgpsword("hair care")},    
                {'viewclass': 'MDRaisedButton',
                'text': '餐廳',
                'on_release': lambda :self.loadgpsword("restaurant")},
                {'viewclass': 'MDRaisedButton',
                'text': '體育場',
                'on_release': lambda :self.loadgpsword("stadium")}, 
                {'viewclass': 'MDRaisedButton',
                'text': '麵包店',
                'on_release': lambda :self.loadgpsword("bakery")},                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                ]
        

    def listening(self,word):
        mythread = threading.Thread(target=self.speak,args=(word,))
        mythread.start()             
                    
    def speak(self,word):       
        from gtts import gTTS             
        if core_platform == "android":
            tts = gTTS(text = word, lang='en')
            tts.save('test.mp3')
            from jnius import autoclass
            from time import sleep
            # get the MediaPlayer java class
            MediaPlayer = autoclass('android.media.MediaPlayer')

            # create our player
            mPlayer = MediaPlayer()
            mPlayer.setDataSource('test.mp3')
            mPlayer.prepare()
        
            # play
            
            mPlayer.start()
            
            sleep(mPlayer.getDuration()/1000)
        else:
            from pygame import mixer
            tts = gTTS(text=word, lang='en')
            tts.save(f'speech{self.playcount%2}.mp3')
            mixer.init()
            mixer.music.load(f'speech{self.playcount%2}.mp3')
            mixer.music.play()
            self.playcount += 1
            
    def toast(self,text):
        try:
            from kivymd.toast import toast
        except TypeError:
            from kivymd.toast.kivytoast import toast
        toast(text)

    def loadgpsword(self,search="bar",wordsearch="null"): #載入GPS單字資料
        from kivymd.uix.card import MDCardPost
        my_data={'type': str(search)}
        if wordsearch != "null":
            my_data={'word': str(wordsearch)}
        r = requests.post('http://120.96.63.79/oit_English/gpswordadmingetword', data = my_data)
        json_data = r.json()
        gps_class_number = json_data['gps_class_number']
        gps_class_etype	 =  json_data['gps_class_etype']
        gps_class_ctype = json_data['gps_class_ctype']
        gps_class_word =  json_data['gps_class_word']
        gps_class_chinese = json_data['gps_class_chinese']
        gps_class_pos = json_data['gps_class_pos']
 
        self.main_widget.ids.scr_mngr.get_screen(
                'QustionAdmin').ids.rv.data =  [{"value": [a,b,c,d,e,f]} for a,b,c,d,e,f in zip(gps_class_number, gps_class_etype, gps_class_ctype,gps_class_word,gps_class_chinese,gps_class_pos)]
    
    def Signup(self,root,userid,username,userpassword,userrepassword,usermail,userphone): #註冊作業
        form=[userid,username,userpassword,userrepassword,usermail,userphone]
        num=0
        for i in form:
            if i.text == "":
                i.helper_text="不能為空"
                i.line_color_normal=(1,0,0,1)
                i.helper_text_mode = "persistent"
                return
            elif i.max_text_length<len(i.text):
                i.helper_text="字數過長"
                i.line_color_normal=(1,0,0,1)
                i.helper_text_mode = "persistent"
                return
            else:
                i.helper_text=""
                i.line_color_normal=(1,1,1,1)
                i.helper_text_mode = "persistent"
            num+=1
        if (userpassword.text != userrepassword.text ):
            userpassword.helper_text="兩次的密碼不一致"
            userpassword.line_color_normal=(1,0,0,1)
            userpassword.helper_text_mode = "persistent"
            return
        else:    
            userrepassword.helper_text=""
            userrepassword.line_color_normal=(1,1,1,1)
            userrepassword.helper_text_mode = "persistent"
        
        my_data = {'id': userid.text, 'name': username.text, 'password': userpassword.text,'email':usermail.text,"phone":userphone.text}
        r = requests.post('http://120.96.63.79/oit_English/creatcount', data = my_data)
        data = r.json()
        if (data['message'] == "success"):
            toast("帳號註冊成功")
            root.dismiss()
        else:
            toast("帳號已重複")

    
    def loadoftenword(self,screen,range): #載入常錯單字        
        from kivymd.uix.expansionpanel import MDExpansionPanel
        def callback(text):
            toast('{} to'.format(text))
        my_data = {'range':str(range),'userid':str(self.userdata['id'])}
        
        r = requests.post('http://120.96.63.79/oit_English/getoftenword', data = my_data)       
        data=r.json()    
        num=0
        chinese=[]
        part=[]
        example=[]
        examplec=[]
        conn = sqlite3.connect(self.db_path)
        c = conn.cursor()

        names_contacts=data['content']['word']
        screen.clear_widgets()
        for name_contact in names_contacts:
            cursor = c.execute("SELECT *  from Vocablary where Vocablary_Word='"+name_contact+"' and Vocablary_range='"+"VocabularyGEPTLA"+"'")
            for row in cursor:
                chinese.append(row[2])
                part.append(row[3])
                example.append(row[4])
                examplec.append(row[5])
        if (len(names_contacts))>0:
            for name_contact in names_contacts:
                content = OftenWrongCard(callback=callback,chinese=str(chinese[num]),part=str(part[num]),example=str(example[num]),examplec=str(examplec[num]))
                screen.add_widget(
                    MDExpansionPanel(content=content,
                                        icon='assets/vocabulary.png',
                                        title=name_contact))            
            num+=1
        else:
            screen.add_widget(MDLabel(text="無資料",halign= 'center',pos_hint= {'center_x': 0.5, 'center_y': 0.5}))
            screen.height=100

    def Login(self,ac,pw,title,accordiontest):   #登入作業    
        try:    
            my_data = {'account': str(ac), 'password': str(pw)}
            r = requests.post('http://120.96.63.79/oit_English/Login', data = my_data)
            if r.json()['Status'] =="Success":
                self.toast(str(r.json()['name'] )+"您好")
                Screens.show_MainPage(self)
                self.userdata=r.json()
                matuserdata=self.userdata['id']
                self.matplotliburldata={'user': self.userdata['id']}
                title.left_action_items=[['menu', lambda x: self.root.toggle_nav_drawer()]]
            else:
                KitchenSink.toast(self,"帳號或密碼錯誤")
        except:
            self.toast("連線失敗，請檢察您的網路連線")
    def changeurl(self):
        self.url='http://120.96.63.79/'+self.userdata['id']+'.jpg'
    def shangeprofilesetting(self,account,password,repassword):
        if (password.text != repassword.text):
            password.helper_text="兩次的密碼不一致"
            password.line_color_normal=(1,0,0,1)
            password.helper_text_mode = "persistent"
            password.helper_text="兩次的密碼不一致"
            password.line_color_normal=(1,0,0,1)
            password.helper_text_mode = "persistent"

    def set_title_toolbar(self, title): 
        self.main_widget.ids.toolbar.title = title
    
    def loadhistroy(self,screen):
        screen.clear_widgets()
        my_data = {'userid':self.userdata['id']}
        r = requests.post('http://120.96.63.79/oit_English/getwordtesthistroy', data = my_data)
        for i in r.json():
            if i[1] == "VocabularyGEPTLA":
                i[1] = "全民英檢初級"
            if i[1] == "VocabularyGEPTLB":
                i[1] = "全民英檢中級"
            if i[1] == "VocabularyGEPTLC":
                i[1] = "全民英檢中高級"
            if i[1] == "VocabularyLA":
                i[1] = "國中英文單字"
            if i[1] == "GpsWordtest":
                i[1] = "GPS場景單字"
            
            if i[2] == "Wordtest":
                i[2] = "一般單字"
            if i[2] == "SpellWordtest":
                i[2] = "拼字測驗"
            if i[2] == "Lisitingtest":
                i[2] ="聽力測驗"
            if i[2] == "Showpicture":
                i[2] = "圖像測驗"
            if i[3] == "Rearrangement":
                i[3] = "句子重組測驗"
            if i[3] =="Connecttest":
                i[3] = "連連看測驗"
            if i[3] == "Sentencetest":
                i[3] == "句子測驗"
            if i[2] == "Connecttest":
                i[2] = "連連看測驗"
                         
            screen.add_widget(wordtesthistroylist(index=i[0],trange=i[1],ttype=i[2],score=i[3],date=i[4]))

    def loadanswercontent(self,id): #載入單字錯誤紀錄        
        from kivymd.uix.card import MDCardPost       
        my_data = {'userid':self.userdata['id'],"range":id.split(",")[1],"index":id.split(",")[0]}        
        r = requests.post('http://120.96.63.79/oit_English/getwordtesthistroycontent', data = my_data)
        data=r.json() 
        type=[]
        num=[]
        errortage = data['errortage']  
        for i in range(len(errortage)):
            if errortage[i]==True:
                errortage[i]="正確"
            else:
                errortage[i]="錯誤"
        if id.split(",")[1] == "GpsWordtest":
            gpsetype=[]
            gpswordindex=[]
            for i in range(len(data['word'])):
                num.append(str(i+1))
                type.append(id.split(",")[1])
            word=data['word']   
            gpsetype=data['gpswordetype']
            gpswordindex= data['gpswordindex']      
            errortage = data['errortage']  
            self.main_widget.ids.scr_mngr.get_screen(
                    'Viewhistroycontect').ids.rv.data =  [{"value": [x,y,z,a,b]} for x,y,z,a,b in zip(num,word,errortage,gpsetype,gpswordindex)]
        else:        
            for i in range(len(data['word'])):
                num.append(str(i+1))
                type.append(id.split(",")[1])
            word=data['word']
            self.main_widget.ids.scr_mngr.get_screen(
                    'Viewhistroycontect').ids.rv.data =  [{"value": [x,y,z]} for x,y,z in zip(num,word,errortage)]
               
    def set_list_md_icons(self,text='', search=False,screen='',tee=VocablaryLA):
        def add_icon_item(name_icon):
            self.main_widget.ids.scr_mngr.get_screen(
                screen).ids.rv.data.append(
                {
                    'viewclass': str(screen+"item"),
                    'id': name_icon,
                    'icon': name_icon,
                    'text': name_icon,
                }
            )

        self.main_widget.ids.scr_mngr.get_screen(screen).ids.rv.data = []
        for name_icon in tee.keys():
            if search:
                if text in name_icon:
                    add_icon_item(name_icon)
            else:
                add_icon_item(name_icon)

    def on_call_testpopup(self):
        poti = TextPopup('File Manager',background = 'atlas://data/images/defaulttheme/button_pressed')
        poti.open()  
    def on_call_Learningpopup(self):
        poti = LearningPopup("",background = 'atlas://data/images/defaulttheme/button_pressed')
        poti.open()
    def on_call_Signuppopup(self):
        poti = SignupPopup('註冊',background = 'atlas://data/images/defaulttheme/vkeyboard_key_down')
        poti.open()
    def on_call_choicequestionpopup(self):
        poti = Choicequestionpopup('新增選擇題',background = 'atlas://data/images/defaulttheme/spinner')
        poti.open()
    def on_call_leakwordpopup(self):
        poti = Leakwordpopup('新增克漏字題目',background = 'atlas://data/images/defaulttheme/spinner')
        poti.open()
    def on_call_NewGpswordPopup(self,num="0",type="0"):
        poti = NewGpswordPopup('新增單字',num=num,type=type,background = 'atlas://data/images/defaulttheme/spinner')
        poti.open()
    def show_user_example_animation_card(self, word,gpsid="null",gpstype="null",relationword=False): #開啟單字卡片        
        if word =="輸入單字":
            return
        if relationword == True: #關聯單字
            translator = Translator()            
            chinese = translator.translate(word, dest='zh-tw').text #中文

            r = requests.get('https://www.wordsapi.com/mashape/words/'+word+'?when=2019-09-02T05:36:35.449Z&encrypted=8cfdb283e7229a9bea9307bfea58bdbfaeb3280934fb9eb8')
            data =r.json()
            try:
                part = translator.translate(data['results'][1]['partOfSpeech'], dest='zh-tw').text 
            except:
                part = "無資料"

            r = requests.get('https://www.wordsapi.com/mashape/words/'+word+'/examples?when=2019-09-02T03:53:09.529Z&encrypted=8cfdb283e7229a9bea9307bfec58bbbaaeb0240935fd9eb8')
            data =r.json()
            try:
                example = data['examples'][0] #範例
                cexample = translator.translate(data['examples'][0], dest='zh-tw').text #範例中文
            except:
                example = "無資料"
                cexample = "無資料"                      
        else:
            if (gpsid==[]):
                gpsid="null"
                gpstype = "null"        
            if (gpsid != "null") or (gpstype!="null"): #GPS單字
                my_data={'num': gpsid,"etype":gpstype}
                r = requests.post('http://120.96.63.79/oit_English/getgpswordinformation', data = my_data)
                data=r.json()
                chinese =data['gps_class_chinese']
                part = data['gps_class_pos']
                example = data['gps_class_example']
                cexample = data['gps_class_cexample']           
            else:
                conn = sqlite3.connect(self.db_path)        
                c = conn.cursor()
                cursor = c.execute(
                    "SELECT * from 'Vocablary' where Vocablary_Word = '"+word+"'")
                for row in cursor:            
                    example = str(row[5])
                    cexample = str(row[6])
                    chinese = str(row[3])
                    part =  str(row[4])

        self.user_animation_card = None
        if not self.user_animation_card:
            self.user_animation_card = MDUserAnimationCard(
                user_name=word,
                path_to_avatar='{}/assets/word/'
                '{}.jpg'.format(self.directory,word[0].lower()),
                )
            self.user_animation_card.box_content.add_widget(
                ContentForAnimCard(id=word,chinese=chinese,part =part,example = example,cexample=cexample,range=self.main_widget.ids.scr_mngr.current,relationword=True))
        self.user_animation_card.open()

    def add_cards(self,tabsa,tabsb,tabsc,tabsd): #標記單字字卡
        conn = sqlite3.connect(self.db_path)        
        c = conn.cursor()
        data=['vocablaryLA','vocabluaryGEPTLA','vocabluaryGEPTLB','vocabluaryGEPTLC']
        loaddata1=[] #國中單字
        loaddata2=[] #全民初級
        loaddata3=[] #全民中級
        loaddata4=[] #全民中高級
        temp=[]
        tabsa.clear_widgets()
        tabsb.clear_widgets()
        tabsc.clear_widgets()
        tabsd.clear_widgets()

        cursor = c.execute("SELECT * from tag_Vocablary where tagVocablary_range ='vocablaryLA'")
        for row in cursor:
            temp=[]
            temp.append(str(row[0]))
            temp.append(str(row[2]))
            temp.append(str(row[3]))
            temp.append(str(row[4]))
            temp.append(str(row[5]))                
            loaddata1.append(temp)       
              
        for i in loaddata1:        
            content = ContentForAnimCard(id=i[0],chinese=i[1],part =i[2],example=i[3],cexample=i[4],range="vocablaryLA",relationword=False)
            tabsa.add_widget(
            MDExpansionPanel(content=content,
                                icon='assets/vocabulary.png',
                                title=i[0]))
        
        cursor = c.execute("SELECT * from tag_Vocablary where tagVocablary_range ='vocabluaryGEPTLA'")
        for row in cursor:
            temp=[]
            temp.append(str(row[0]))
            temp.append(str(row[2]))
            temp.append(str(row[3]))
            temp.append(str(row[4]))
            temp.append(str(row[5]))                
            loaddata2.append(temp)  

        for i in loaddata2:        
            content = ContentForAnimCard(id=i[0],chinese=i[1],part =i[2],example=i[3],cexample=i[4],range="vocabluaryGEPTLA",relationword=False)
            tabsb.add_widget(
            MDExpansionPanel(content=content,
                                icon='assets/vocabulary.png',
                                title=i[0]))
        cursor = c.execute("SELECT * from tag_Vocablary where tagVocablary_range ='vocabluaryGEPTLB'")
        for row in cursor:
            temp=[]
            temp.append(str(row[0]))
            temp.append(str(row[2]))
            temp.append(str(row[3]))
            temp.append(str(row[4]))
            temp.append(str(row[5]))                
            loaddata3.append(temp)  

        for i in loaddata3:        
            content = ContentForAnimCard(id=i[0],chinese=i[1],part =i[2],example=i[3],cexample=i[4],range="vocabluaryGEPTLB",relationword=False)
            tabsc.add_widget(
            MDExpansionPanel(content=content,
                                icon='assets/vocabulary.png',
                                title=i[0]))

        cursor = c.execute("SELECT * from tag_Vocablary where tagVocablary_range ='vocabluaryGEPTLC'")
        for row in cursor:
            temp=[]
            temp.append(str(row[0]))
            temp.append(str(row[2]))
            temp.append(str(row[3]))
            temp.append(str(row[4]))
            temp.append(str(row[5]))                
            loaddata4.append(temp)  

        for i in loaddata4:        
            content = ContentForAnimCard(id=i[0],chinese=i[1],part =i[2],example=i[3],cexample=i[4],range="vocabluaryGEPTLC",relationword=False)
            tabsd.add_widget(
            MDExpansionPanel(content=content,
                                icon='assets/vocabulary.png',
                                title=i[0]))

   
    def creattestlay(self,main,x="null"): #選擇題目畫面
        main.clear_widgets()
        self.numchoselay=BoxLayout()
        self.typeset=BoxLayout()
        self.item1=MDLabel(text="選擇題目數", font_style="H5", theme_text_color='Primary',halign='center')
        self.item3=MDLabel(text="選擇測驗類型", font_style="H5", theme_text_color='Primary',halign='center')
        if (self.main_widget.ids.scr_mngr.current == "Wordtest"):
            self.title1 = MDLabel(id="title1",text="單字測驗",font_style="H4",theme_text_color='Custom',halign='center')
            self.title2 = MDLabel(id="title2",font_style="H6",text="選擇題，請依據題目選出正確答案\n每題只有一個答案", theme_text_color='Primary',halign='center')
            self.item2 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],dropdown_width_mult=3,pos_hint={'center_x': 0.5, 'center_y': 0.3})            
            self.item2.items =["選擇題目類型","國中英文單字","全民英檢初級","全民英檢中級","全民英檢中高級","GPS情境單字測驗"]            
            self.item4 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],pos_hint={'center_x': 0.5, 'center_y': 0.3})   
            self.item4.items =["選擇題數","10","50","100"]
            
            self.numchoselay.add_widget(self.item1)
            self.numchoselay.add_widget(self.item2)
            self.item4.color=(1,1,1,1)
            self.item2.color=(1,1,1,1)
            self.typeset.add_widget(self.item3)             
            self.typeset.add_widget(self.item4)             
        if (self.main_widget.ids.scr_mngr.current == "SpellWordtest"):
            self.title1 = MDLabel(id="title1",text="拚字測驗",font_style="H2", theme_text_color='Primary',halign='center')
            self.title2 = MDLabel(id="title2",font_style="H5",text="拼音測驗，", theme_text_color='Primary',halign='center')
            self.item2 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],dropdown_width_mult=3,pos_hint={'center_x': 0.5, 'center_y': 0.3})            
            self.item2.items =["選擇題目類型","國中英文單字","全民英檢初級","全民英檢中級","全民英檢中高級","GPS情境單字測驗"]            
            self.item4 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],pos_hint={'center_x': 0.5, 'center_y': 0.3})   
            self.item4.items =["選擇題數","10","50","100"]
            self.numchoselay.add_widget(self.item1)
            self.numchoselay.add_widget(self.item2)
            self.typeset.add_widget(self.item3)             
            self.typeset.add_widget(self.item4)   
            self.title1.color=(1,1,1,1)
        if (self.main_widget.ids.scr_mngr.current == "Lisitingtest"):
            self.title1 = MDLabel(id="title1",text="聽力測驗",font_style="H2", theme_text_color='Primary',halign='center')
            self.title2 = MDLabel(id="title2",font_style="H5",text="聽力測驗", theme_text_color='Primary',halign='center')
            self.item2 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],dropdown_width_mult=3,pos_hint={'center_x': 0.5, 'center_y': 0.3})            
            self.item2.items =["選擇題目類型","國中英文單字","全民英檢初級","全民英檢中級","全民英檢中高級","GPS情境單字測驗"]            
            self.item4 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],pos_hint={'center_x': 0.5, 'center_y': 0.3})   
            self.item4.items =["選擇題數","10","50","100"]
            self.numchoselay.add_widget(self.item1)
            self.numchoselay.add_widget(self.item2)       
            self.typeset.add_widget(self.item3)             
            self.typeset.add_widget(self.item4)   
        if (self.main_widget.ids.scr_mngr.current == "Showpicture"):
            self.title1 = MDLabel(id="title1",text="圖像測驗",font_style="H2", theme_text_color='Primary',halign='center')
            self.title2 = MDLabel(id="title2",font_style="H5",text="圖像測驗", theme_text_color='Primary',halign='center')
            self.item2 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],dropdown_width_mult=3,pos_hint={'center_x': 0.5, 'center_y': 0.3})            
            self.item2.items =["選擇題目類型","國中英文單字","全民英檢初級","全民英檢中級","全民英檢中高級","GPS情境單字測驗"]
            
            self.item4 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],pos_hint={'center_x': 0.5, 'center_y': 0.3})   
            self.item4.items =["選擇題數","10","50","100"]
            self.numchoselay.add_widget(self.item1)
            self.numchoselay.add_widget(self.item2)
            self.typeset.add_widget(self.item3)
            self.typeset.add_widget(self.item4)
        if (self.main_widget.ids.scr_mngr.current == "Sentencetest"):
            self.title1 = MDLabel(id="title1",text="句子測驗",font_style="H2", theme_text_color='Primary',halign='center')
            self.title2 = MDLabel(id="title2",font_style="H5",text="選擇題，請依據題目選出正確答案，每題只有一個答案", theme_text_color='Primary',halign='center')
            self.item2 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],dropdown_width_mult=3,pos_hint={'center_x': 0.5, 'center_y': 0.3})            
            self.item2.items =["選擇題目類型","全民英檢中級","全民英檢中高級","GPS情境單字測驗"]            
            self.item4 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],pos_hint={'center_x': 0.5, 'center_y': 0.3})   
            self.item4.items =["選擇題數","10","50","100"]
            self.numchoselay.add_widget(self.item1)
            self.numchoselay.add_widget(self.item2)
            self.typeset.add_widget(self.item3)             
            self.typeset.add_widget(self.item4)    
        if (self.main_widget.ids.scr_mngr.current == "Rearrangement"):
            self.title1 = MDLabel(id="title1",text="句子測驗",font_style="H2", theme_text_color='Primary',halign='center')
            self.title2 = MDLabel(id="title2",font_style="H5",text="選擇題，請依據題目選出正確答案，每題只有一個答案", theme_text_color='Primary',halign='center')
            self.item2 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],dropdown_width_mult=3,pos_hint={'center_x': 0.5, 'center_y': 0.3})            
            self.item2.items =["選擇題目類型","全民英檢初級","全民英檢中級","全民英檢中高級","GPS情境單字測驗"]            
            self.item4 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],pos_hint={'center_x': 0.5, 'center_y': 0.3})   
            self.item4.items =["選擇題數","10","50","100"]
            self.numchoselay.add_widget(self.item1)
            self.numchoselay.add_widget(self.item2)
            self.typeset.add_widget(self.item3)             
            self.typeset.add_widget(self.item4)    
        if (self.main_widget.ids.scr_mngr.current == "Connecttest"):
            self.title1 = MDLabel(id="title1",text="句子測驗",font_style="H2",pos_hint={'center_x': 0.5, 'center_y': 0.9}, theme_text_color='Primary',halign='center')
            self.title2 = MDLabel(id="title2",font_style="H5",text="選擇題，請依據題目選出正確答案，每題只有一個答案",pos_hint={'center_x': 0.5, 'center_y': 0.7}, theme_text_color='Primary',halign='center')
            self.item2 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],dropdown_width_mult=3,pos_hint={'center_x': 0.5, 'center_y': 0.5})            
            self.item2.items =["選擇題目類型","國中英文單字","全民英檢初級","全民英檢中級","全民英檢中高級","GPS情境單字測驗"]            
            self.item4 = MDDropDownItem(dropdown_bg=[1, 1, 1, 1],pos_hint={'center_x': 0.5, 'center_y': 0.5})   
            self.item4.items =["選擇題數","10","50","100"]
            self.item1.pos_hint={'center_x': 0.5, 'center_y': 0.5}
            self.numchoselay.pos_hint={'center_x': 0.5, 'center_y': 0.5}
            self.numchoselay.padding=[20,20,20,20]
            self.typeset.pos_hint={'center_x': 0.5, 'center_y': 0.3}
            self.typeset.padding=[20,20,20,20]
            self.numchoselay.add_widget(self.item1)
            self.numchoselay.add_widget(self.item2)                       
        self.btn = MDFillRoundFlatButton(text="開始", pos_hint= {'center_x': 0.5, 'center_y': 0.5})
        self.btn.bind(on_release=lambda x :creatquestion(self,self.item4.current_item,self.item2.current_item))
        if (self.main_widget.ids.scr_mngr.current == "Connecttest"):                                                                
            self.btn.pos_hint= {'center_x': 0.5, 'center_y': 0.1}

        self.title1.color=(1,1,1,1)
        self.title2.color=(1,1,1,1)
        self.item3.color=(1,1,1,1)
        self.item1.color=(1,1,1,1)

        main.add_widget(self.title1)
        main.add_widget(self.title2)
        main.add_widget(self.typeset)
        main.add_widget(self.numchoselay)
        main.add_widget(self.btn)
       
        def creatquestion(self,questionnum,questiontype):
            
            self.answerstore={} #裝使用者的答案
            self.answer={} #解答
            self.quest=[] #題目
            self.questionid=[] #題目ID
            if (self.main_widget.ids.scr_mngr.current == "Rearrangement"):
                if questionnum =="" or questiontype =="":
                    KitchenSink.toast(self,"部分項目未選取")
                    return
                if questiontype  == "國中英文單字":
                    self.testrange = "VocabularyLA"
                if questiontype ==  "全民英檢初級":
                    self.testrange = "VocabularyGEPTLA"
                if questiontype ==  "全民英檢中級":
                    self.testrange = "VocabularyGEPTLB"
                if questiontype =="全民英檢中高級":
                    self.testrange = "VocabularyGEPTLC"                                    
                if questiontype == "GPS情境單字測驗":
                    self.englishplace=[]
                    mythread = threading.Thread(target=self.testloadgps)
                    mythread.start()   
                    while True:
                        if (self.englishplace!=[]):
                            break
                    self.testrange = self.englishplace
                    self.my_data = {'type':"GpsRearrangement",'count': str(questionnum),'range':str(self.testrange)}                    
                else:
                    self.my_data = {'type':"Rearrangement",'count': str(questionnum),'range':str(self.testrange)}                
                r = requests.post('http://120.96.63.79/oit_English/getvactest', data = self.my_data)
                data = r.json()
                self.quest  = data["question"]
                self.answer  = data["answer"]
                self.disablebutton = {}
                self.i=0
                main.clear_widgets()
                self.questionnum = MDLabel(font_style='H4',
                            text="第"+str(self.i+1)+"/"+str(len(self.quest))+"題", theme_text_color='Primary',halign='center') #題號              
                self.questionnum.color=(1,1,1,1)      
                main.add_widget(self.questionnum)
                self.textfiled = MDTextField(multiline=True,disabled=True)
                self.textfiled.foreground_color=(1,1,1,1)
                self.textfiled.color=(1,1,1,1)
                self.textfiled.disabled_foreground_color=(1,1,1,1)
                main.add_widget(self.textfiled)
                self.ansertboxlay=BoxLayout()
                word = self.quest[self.i]
                self.wordlist= word.split(" ")
                totelwidth=0
                totelcols=0
                if core_platform == "android":
                    from jnius import autoclass
                    DisplayMetrics = autoclass('android.util.DisplayMetrics')
                    metrics = DisplayMetrics()
                    metrics.setToDefaults()           
                    for i in word:
                        if (len(i)<7):
                            totelwidth+=80
                        else:
                            totelwidth+=len(i)*10
                        if totelwidth < metrics.xdpi:
                            totelcols+=1
                else:
                    for i in word:
                        if (len(i)<7):
                            totelwidth+=80
                        else:
                            totelwidth+=len(i)*10
                        if totelwidth < main.width:
                            totelcols+=1
                self.layout = GridLayout(rows=5,cols=totelcols,row_default_height=50)
                self.button = []
                btnindex=0
                for i in self.wordlist:
                    self.button.append(MDFillRoundFlatButton(id=str(btnindex),text=str(i),pos_hint= {'center_x': 0.5, 'center_y': 0.5}))
                    btnindex+=1
                for j in self.button:
                    j.bind(on_press=lambda x:addtextfile(self,x,self.textfiled))
                    self.layout.add_widget(j)
                self.resetbtn = MDFillRoundFlatButton(text="重設",pos_hint= {'center_x': 0.5, 'center_y': 0.5})
                self.resetbtn.bind(on_press=lambda x:addtextfile(self,self.textfiled,self.button,reset=True))
                main.add_widget(self.resetbtn)
                self.prentxbtnlay = BoxLayout(spacing=dp(5))

                self.prebtn = MDFillRoundFlatButton(text="上一題", size_hint=(0.5, 0.2))
                self.prebtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Nextbtn = MDFillRoundFlatButton(text="下一題", size_hint=(0.5, 0.2))
                self.Nextbtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Finbtn = MDFillRoundFlatButton(text="結束答題", size_hint=(0.5, 0.2))
                self.Finbtn.bind(on_press=lambda x:show_checkwindows(x,self.testrange))

                self.prentxbtnlay.add_widget(self.prebtn)
                self.prentxbtnlay.add_widget(self.Nextbtn)
                self.prentxbtnlay.add_widget(self.Finbtn)

                self.prebtn.disabled = True

                self.ansertboxlay.add_widget(self.layout)

                main.add_widget(self.ansertboxlay)
                main.add_widget(self.prentxbtnlay)

                def addtextfile(self,x,y,reset=False):
                    if (str(self.i) in self.disablebutton):
                        disablelist=self.disablebutton[str(self.i)]
                    else:
                        disablelist=[]                        
                    if (reset):
                        for i in y:
                            i.disabled=False
                        x.text=""
                        disablelist.clear()
                    else:                    
                        x.disabled = True
                        y.text +=x.text+" "
                        disablelist.append(x.id)                
                    self.answerstore[str(self.i)] = self.textfiled.text.rstrip()
                    self.disablebutton[str(self.i)] = disablelist
                    
            if (self.main_widget.ids.scr_mngr.current == "Sentencetest"):
                if questionnum =="" or questiontype =="":
                    KitchenSink.toast(self,"部分項目未選取")
                    return                    
                if questiontype  == "小學英文單字":
                    self.testrange = "VocabularyLA"
                if questiontype ==  "全民英檢初級":
                    self.testrange = "VocabularyGEPTLA"
                if questiontype ==  "全民英檢中級":
                    self.testrange = "VocabularyGEPTLB"
                if questiontype =="全民英檢中高級":
                    self.testrange = "VocabularyGEPTLC"                                    
                if questiontype == "GPS情境單字測驗":
                    self.englishplace=[]
                    mythread = threading.Thread(target=self.testloadgps)
                    mythread.start()   
                    while True:
                        if (self.englishplace!=[]):
                            break
                    self.testrange = self.englishplace
                    self.my_data = {'type':"GpsSentencetest",'count': str(questionnum),'range':str(self.testrange)}                    
                else:
                    self.my_data = {'type':"Sentencetest",'count': str(questionnum),'range':str(self.testrange)}
                
                r = requests.post('http://120.96.63.79/oit_English/getvactest', data = self.my_data)
                data = r.json()
                self.quest = data['question']
                self.option = data['option']
                self.answer = data['answer']
                self.i=0 #題號
                aaa = self.option[str(self.i)][0]
                bbb = self.option[str(self.i)][1]

                word = aaa.split(" ")
          
                self.area = GridLayout(padding=[30,100,30,20],row_default_height=50,cols=3,rows=3 ,pos_hint= {'center_y': 0.5},size_hint_y=None)
                            
                try:
                    worda=MDLabel(text= str(' '.join(str(e) for e in word[0:word.index(bbb)][0:word.index(bbb)])),size_hint=(None,None), theme_text_color='Primary')
                    wordb=MDTextField(text="",padding_x=[20,0])
                    wordc=MDLabel(text= str(' '.join(str(e) for e in word[word.index(bbb)+1:])), theme_text_color='Primary')
                except:
                    worda=MDLabel(text= str(' '.join(str(e) for e in word[0:word.index(bbb+".")][0:word.index(bbb+".")])), theme_text_color='Primary')
                    wordb=MDTextField(text="",padding_x=[20,0],pos_hint= {'center_x': .1})
                    wordc=""
                self.area.add_widget(worda)
                self.area.add_widget(wordb)
                
                try:
                    self.area.add_widget(wordc)
                except:
                    pass                
                
                #輸入框
                wordb.line_color_normal=(1,1,1,1)
                wordb.color=(1,1,1,1)    
                wordb.foreground_color=(1,1,1,1)
                wordb.bind(text=lambda x,y:on_text(x,y,self.answerstore,self.i))
                
                main.clear_widgets()              
                
                self.answeroptionlay = GridLayout(cols=2,size_hint_y= dp(5))
                self.questionnum = MDLabel(font_style='H4',
                            text="第"+str(self.i+1)+"/"+str(len(self.quest))+"題", theme_text_color='Primary',halign='center') #題號
                self.questionword = MDLabel(font_style='H4',
                            text= self.quest[self.i], theme_text_color='Primary',halign='center') #題目內容
                self.questionnum.color=(1,1,1,1)      
                self.questionword.color=(1,1,1,1)   
                main.add_widget(self.questionnum)
                main.add_widget(self.questionword)
                                
                prentxbtnlay = BoxLayout(spacing=dp(5))
                self.prebtn = MDRaisedButton(text="上一題", size_hint=(
                    0.5, 0.5))
                self.prebtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Nextbtn = MDRaisedButton(text="下一題", size_hint=(0.5, 0.5))
                self.Nextbtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Finbtn = MDRaisedButton(text="結束答題", size_hint=(0.5, 0.5))
                self.Finbtn.bind(on_press=lambda x:show_checkwindows(x,self.testrange))

                prentxbtnlay.add_widget(self.prebtn)
                prentxbtnlay.add_widget(self.Nextbtn)
                prentxbtnlay.add_widget(self.Finbtn)
                self.prebtn.disabled = True
                main.add_widget(self.area)
                main.add_widget(self.answeroptionlay)
                main.add_widget(prentxbtnlay)
                
                def on_text(self,instance,anserstore,num): #使用者輸入文字時儲存答案
                    temp=""
                    try:
                        temp+=worda.text
                        temp+=" "
                        temp+=wordb.text
                        temp+=wordc.text
                    except:
                        temp+=worda.text
                        temp+=" "
                        temp+=wordb.text                    
                    anserstore[str(num)] = temp
            if (self.main_widget.ids.scr_mngr.current == "Wordtest" ): #一般測驗
                if questionnum =="" or questiontype =="":
                    KitchenSink.toast(self,"部分項目未選取")
                    return                    
                if questiontype  == "國中英文單字":
                    self.testrange = "VocabularyLA"
                if questiontype ==  "全民英檢初級":
                    self.testrange = "VocabularyGEPTLA"
                if questiontype ==  "全民英檢中級":
                    self.testrange = "VocabularyGEPTLB"
                if questiontype =="全民英檢中高級":
                    self.testrange = "VocabularyGEPTLC"                                    
                if questiontype == "GPS情境單字測驗":
                    self.gpsrange=[]
                    mythread = threading.Thread(target=self.testloadgps)
                    mythread.start()
                    while True:
                        if (self.gpsrange!=[]):
                            break
                    self.testrange =  self.gpsrange
                    self.my_data = {'type':"GpsWordtest",'count': str(questionnum),'range':str(self.testrange)}                    
                else:
                    self.my_data = {'type':"Wordtest",'count': str(questionnum),'range':str(self.testrange)}
                r = requests.post('http://120.96.63.79/oit_English/getvactest', data = self.my_data)
                data = r.json()
                self.questionid = data['questionid']
                self.quest = data['question']
                self.answer = data['answer']
                self.answerword = data['option']      
                self.i=0
                main.clear_widgets()
                self.questionnum = MDLabel(font_style='H4',
                            text="第"+str(self.i+1)+"/"+str(len(self.quest))+"題", theme_text_color='Primary',halign='center') #題號
                self.questionword = MDLabel(font_style='H4',
                            text= self.quest[self.i], theme_text_color='Primary',halign='center') #題目內容
                self.questionnum.color=(1,1,1,1)      
                self.questionword.color=(1,1,1,1)           

                main.add_widget(self.questionnum)
                main.add_widget(self.questionword)
                answeroptionlay = GridLayout(cols=2,size_hint_y= dp(5))
                
                self.c1 = MDCheckbox(id="A", group="test")
                self.c1.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾選
                self.q1 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="A", theme_text_color='Primary')
                self.q1.color=(1,1,1,1)

                self.c2 = MDCheckbox(id="B", group="test")
                self.c2.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾
                self.q2 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="B", theme_text_color='Primary')
                self.q2.color=(1,1,1,1)

                self.c3 = MDCheckbox(id="C", group="test")
                self.c3.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾選
                self.q3 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="C", theme_text_color='Primary')
                self.q3.color=(1,1,1,1)

                self.c4 = MDCheckbox(id="D", group="test")
                self.c4.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾選
                self.q4 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="D", theme_text_color='Primary')
                self.q4.color=(1,1,1,1)

                answeroptionlay.add_widget(self.c1)
                answeroptionlay.add_widget(self.q1)
                answeroptionlay.add_widget(self.c2)
                answeroptionlay.add_widget(self.q2)
                answeroptionlay.add_widget(self.c3)
                answeroptionlay.add_widget(self.q3)
                answeroptionlay.add_widget(self.c4)
                answeroptionlay.add_widget(self.q4)

                prentxbtnlay = BoxLayout(spacing=dp(5))

                self.prebtn = MDFillRoundFlatButton(text="上一題", size_hint=(
                    0.7, 0.7))
                self.prebtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Nextbtn = MDFillRoundFlatButton(text="下一題", size_hint=(0.7, 0.7))
                self.Nextbtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Finbtn = MDFillRoundFlatButton(text="結束答題", size_hint=(0.7,0.7))
                self.Finbtn.bind(on_press=lambda x:show_checkwindows(x,self.testrange))

                prentxbtnlay.add_widget(self.prebtn)
                prentxbtnlay.add_widget(self.Nextbtn)
                prentxbtnlay.add_widget(self.Finbtn)

                self.prebtn.disabled = True

                main.add_widget(answeroptionlay)
                main.add_widget(prentxbtnlay)

                self.q1.text = self.answerword[str(self.i)][0]
                self.q2.text = self.answerword[str(self.i)][1]
                self.q3.text = self.answerword[str(self.i)][2]
                self.q4.text = self.answerword[str(self.i)][3]        

                def usercheck(value):
                    if value.active:
                        if (value.id =="A"):
                            self.answerstore[str(self.i)]= str(self.q1.text)
                        if (value.id =="B"):
                            self.answerstore[str(self.i)]= str(self.q2.text)
                        if (value.id =="C"):
                            self.answerstore[str(self.i)]= str(self.q3.text)
                        if (value.id =="D"):
                            self.answerstore[str(self.i)]= str(self.q4.text)                    
                    else:
                        if str(self.i) in self.answerstore.keys():
                            del self.answerstore[str(self.i)]          
            if (self.main_widget.ids.scr_mngr.current == "SpellWordtest"): #拼字測驗
                if questionnum =="" or questiontype =="":
                    KitchenSink.toast(self,"部分項目未選取")
                    return                    
                if questiontype  == "國中英文單字":
                    self.testrange = "VocabularyLA"
                if questiontype ==  "全民英檢初級":
                    self.testrange = "VocabularyGEPTLA"
                if questiontype ==  "全民英檢中級":
                    self.testrange = "VocabularyGEPTLB"
                if questiontype =="全民英檢中高級":
                    self.testrange = "VocabularyGEPTLC"                                    
                if questiontype == "GPS情境單字測驗":
                    self.englishplace=[]
                    mythread = threading.Thread(target=self.testloadgps)
                    mythread.start()   
                    while True:
                        if (self.englishplace!=[]):
                            break
                    self.testrange = self.englishplace
                    self.my_data = {'type':"GpsSpellWordtest",'count': str(questionnum),'range':str(self.testrange)}                    
                else:
                    self.my_data = {'type':"SpellWordtest",'count': str(questionnum),'range':str(self.testrange)}
                r = requests.post('http://120.96.63.79/oit_English/getvactest', data = self.my_data)
                data = r.json()
                self.Answerarea=[] #作答區
                self.englishq=[]  #裝拆過的單字
                self.questionid = data['questionid']
                self.quest = data['question']
                self.answer = data['answer']

                self.i=0

              

                for i in self.answer.values():
                    if (len(i)) >= 2:
                        a = list(i)
                        rdnum = random.randint(1,len(a)-1) #挖幾個字元
                        emptyindex=[] #要挖的索引
                        while True:
                            rd = random.randint(1,len(a)-1)
                            if rd not in emptyindex:
                                emptyindex.append(rd)
                            if len(emptyindex) == rdnum:
                                break
                        for i in emptyindex:
                            a[i]="_"
                            question = "".join(a)                        
                    else:
                        question="_" 
                    self.englishq.append(question)   

                main.clear_widgets()
        
                self.questionnum = MDLabel(font_style='H4',
                            text="第"+str(self.i+1)+"/"+str(len(self.quest))+"題", theme_text_color='Primary',halign='center') #題號
                self.questionword = MDLabel(font_style='H4',
                            text= self.quest[self.i], theme_text_color='Primary',halign='center') #題目內容
                self.questionnum.color=(1,1,1,1)
                self.questionword.color=(1,1,1,1)
                main.add_widget(self.questionnum)
                main.add_widget(self.questionword)
                mythread = threading.Thread(target=self.testloadgps)
                self.listeningbtn = MDRaisedButton(text="發音",pos_hint= {'center_x': 0.5, 'center_y': 0.5}) #題目內容
                self.listeningbtn.bind(on_release=lambda x:KitchenSink.listening(self,self.answer[str(self.i)]))

                main.add_widget(self.listeningbtn)
                
                self.mainlayout= BoxLayout(id="questionlayout",spacing=dp(1),padding=[5,5,5,5],do_scroll_x=False)
                for i in self.englishq[0]:
                    if i =="_":
                        self.Answerarea.append(MDTextField(text="",padding_x=[20,0],id="textfiled"))
                    else:
                        self.Answerarea.append(MDLabel(text=str(i), font_style="H5", theme_text_color='Primary',halign='center',pos_hint= {'center_y': 0.5}))                            
                
                self.textfiledid=[]
                for i in range(len(self.Answerarea)):
                    if self.Answerarea[i].id == "textfiled":
                        self.textfiledid.append(i)
                
                for i in self.Answerarea:                                        
                    self.mainlayout.add_widget(i)
                    i.bind(text=lambda x,y:on_text(x,y,self.Answerarea,self.answerstore,self.i,self.textfiledid))
                    i.line_color_normal=(1,1,1,1)
                    i.foreground_color=(1,1,1,1)
                    
                main.add_widget(self.mainlayout)

                prentxbtnlay = BoxLayout(id="premtxbtnlay",spacing=dp(5))

                self.prebtn = MDRaisedButton(text="上一題", size_hint=(0.5, 0.5))
                self.prebtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Nextbtn = MDRaisedButton(text="下一題", size_hint=(0.5, 0.5))
                self.Nextbtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Finbtn = MDRaisedButton(text="結束答題", size_hint=(0.5, 0.5))
                self.Finbtn.bind(on_press=lambda x:show_checkwindows(x,self.testrange))
                prentxbtnlay.add_widget(self.prebtn)
                prentxbtnlay.add_widget(self.Nextbtn)
                prentxbtnlay.add_widget(self.Finbtn)
                self.prebtn.disabled = True
                main.add_widget(prentxbtnlay)
                
                def on_text(self,instance,Answerarea,anserstore,num,textfiledid): #使用者輸入文字時儲存答案
                    if (self.text!=""):                        
                        try:                    
                            Answerarea[textfiledid[textfiledid.index(Answerarea.index(self))+1]].focus=True
                        except IndexError:
                            pass
                    else:
                        Answerarea[textfiledid[0]].focus=True
                    
                    temp=""
                    for i in Answerarea:
                        if len(i.text)>1:
                            i.text=str(i.text[0])
                        if i.text=="":
                            temp+=" "
                        temp +=i.text  
                    anserstore[str(num)] = temp

            if (self.main_widget.ids.scr_mngr.current == "Showpicture"):
                if questionnum =="" or questiontype =="":
                    KitchenSink.toast(self,"部分項目未選取")
                    return                    
                if questiontype  == "國中英文單字":
                    self.testrange = "VocabularyLA"
                if questiontype ==  "全民英檢初級":
                    self.testrange = "VocabularyGEPTLA"
                if questiontype ==  "全民英檢中級":
                    self.testrange = "VocabularyGEPTLB"
                if questiontype =="全民英檢中高級":
                    self.testrange = "VocabularyGEPTLC"                                    
                if questiontype == "GPS情境單字測驗":
                    self.englishplace=[]
                    mythread = threading.Thread(target=self.testloadgps)
                    mythread.start()   
                    while True:
                        if (self.englishplace!=[]):
                            break
                    self.testrange = self.englishplace
                    self.my_data = {'type':"Showpicture",'count': str(questionnum),'range':str(self.testrange)}                    
                else:
                    self.my_data = {'type':"Showpicture",'count': str(questionnum),'range':str(self.testrange)}
                r = requests.post('http://120.96.63.79/oit_English/getvactest', data = self.my_data)
                data = r.json()
                self.questionid = data['questionid']
                self.quest = data['question']
                self.answer = data['answer']
                self.answerword = data['option']      
                self.i=0
                
                main.clear_widgets()

              
                self.questionnum = MDLabel(font_style='H4',
                            text="第"+str(self.i+1)+"/"+str(len(self.quest))+"題", theme_text_color='Primary',halign='center') #題號
                self.questionnum.color=(1,1,1,1)
                word=self.quest[self.i]
                headers = {'user-agent': 'my-app/0.0.1'}
                print(word)
                main.add_widget(self.questionnum)
                word=self.quest[self.i]                    
                url = "https://www.shutterstock.com/zh-Hant/search/"+word
                r = requests.get(
                    url, headers={
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'
                    })
                soup = BeautifulSoup(r.text, 'html.parser')
                b_tag = soup.find("img", class_="z_g_j z_g_a z_g_b")  
                self.imgbtn = ImageButton(source=str(b_tag.get('src')))
                main.add_widget(self.imgbtn)       
                answeroptionlay = GridLayout(cols=2,size_hint_y= dp(5))
                
                self.c1 = MDCheckbox(id="A", group="test")
                self.c1.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾選

                self.q1 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="A", theme_text_color='Primary')

                self.c2 = MDCheckbox(id="B", group="test")
                self.c2.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾
                self.q2 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="B", theme_text_color='Primary')

                self.c3 = MDCheckbox(id="C", group="test")
                self.c3.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾選
                self.q3 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="C", theme_text_color='Primary')

                self.c4 = MDCheckbox(id="D", group="test")
                self.c4.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾選

                self.q4 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="D", theme_text_color='Primary')
                answeroptionlay.add_widget(self.c1)
                answeroptionlay.add_widget(self.q1)
                answeroptionlay.add_widget(self.c2)
                answeroptionlay.add_widget(self.q2)
                answeroptionlay.add_widget(self.c3)
                answeroptionlay.add_widget(self.q3)
                answeroptionlay.add_widget(self.c4)
                answeroptionlay.add_widget(self.q4)

                prentxbtnlay = BoxLayout(spacing=dp(5))

                self.prebtn = MDRaisedButton(text="上一題", size_hint=(
                    0.5, 0.5))
                self.prebtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Nextbtn = MDRaisedButton(text="下一題", size_hint=(0.5, 0.5))
                self.Nextbtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Finbtn = MDRaisedButton(text="結束答題", size_hint=(0.5, 0.5))
                self.Finbtn.bind(on_press=lambda x:show_checkwindows(x,self.testrange))

                prentxbtnlay.add_widget(self.prebtn)
                prentxbtnlay.add_widget(self.Nextbtn)
                prentxbtnlay.add_widget(self.Finbtn)

                self.prebtn.disabled = True

                main.add_widget(answeroptionlay)
                main.add_widget(prentxbtnlay)

                self.q1.text = self.answerword[str(self.i)][0]
                self.q2.text = self.answerword[str(self.i)][1]
                self.q3.text = self.answerword[str(self.i)][2]
                self.q4.text = self.answerword[str(self.i)][3]        
                self.q1.color=(1,1,1,1)
                self.q2.color=(1,1,1,1)
                self.q3.color=(1,1,1,1)
                self.q4.color=(1,1,1,1)
                def usercheck(value):
                    if value.active:
                        if (value.id =="A"):
                            self.answerstore[str(self.i)]= str(self.q1.text)
                        if (value.id =="B"):
                            self.answerstore[str(self.i)]= str(self.q2.text)
                        if (value.id =="C"):
                            self.answerstore[str(self.i)]= str(self.q3.text)
                        if (value.id =="D"):
                            self.answerstore[str(self.i)]= str(self.q4.text)
                    else:
                        if str(self.i) in self.answerstore.keys():
                            del self.answerstore[str(self.i)]
            if (self.main_widget.ids.scr_mngr.current == "Lisitingtest" ):
                if questionnum =="" or questiontype =="":
                    KitchenSink.toast(self,"部分項目未選取")
                    return                    
                if questiontype  == "國中英文單字":
                    self.testrange = "VocabularyLA"
                if questiontype ==  "全民英檢初級":
                    self.testrange = "VocabularyGEPTLA"
                if questiontype ==  "全民英檢中級":
                    self.testrange = "VocabularyGEPTLB"
                if questiontype =="全民英檢中高級":
                    self.testrange = "VocabularyGEPTLC"                                    
                if questiontype == "GPS情境單字測驗":
                    self.englishplace=[]
                    mythread = threading.Thread(target=self.testloadgps)
                    mythread.start()
                    while True:
                        if (self.englishplace!=[]):
                            break
                    self.testrange = self.englishplace
                    self.my_data = {'type':"GpsLisitingtest",'count': str(questionnum),'range':str(self.testrange)}                    
                else:
                    self.my_data = {'type':"Lisitingtest",'count': str(questionnum),'range':str(self.testrange)}
                r = requests.post('http://120.96.63.79/oit_English/getvactest', data = self.my_data)
                data = r.json()
                self.questionid = data['questionid']
                self.quest = data['question']
                self.answer = data['answer']
                self.answerword = data['option']            
                self.i=0
                main.clear_widgets()

                self.questiontitle = MDLabel(font_style='H2',
                            text="聽力", theme_text_color='Primary',halign='center')
                self.questionnum = MDLabel(font_style='H4',
                            text="第"+str(self.i+1)+"/"+str(len(self.quest))+"題", theme_text_color='Primary',halign='center') #題號
                self.questionword = MDRaisedButton(text="發音",pos_hint= {'center_x': 0.5, 'center_y': 0.5}) #題目內容
                self.questionword.bind(on_release=lambda x:KitchenSink.listening(self, self.quest[self.i]))                               

                main.add_widget(self.questiontitle)              
                main.add_widget(self.questionnum)
                main.add_widget(self.questionword)
                answeroptionlay = GridLayout(cols=2,size_hint_y= dp(5))
                
                self.c1 = MDCheckbox(id="A", group="test")
                self.c1.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾選

                self.q1 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="A", theme_text_color='Primary')

                self.c2 = MDCheckbox(id="B", group="test")
                self.c2.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾
                self.q2 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="B", theme_text_color='Primary')

                self.c3 = MDCheckbox(id="C", group="test")
                self.c3.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾選
                self.q3 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="C", theme_text_color='Primary')

                self.c4 = MDCheckbox(id="D", group="test")
                self.c4.bind(on_release=lambda x:usercheck(x)) #x check y是否已勾選

                self.q4 = MDLabel(size_hint=(0.6, 0.5), font_style='H5',
                            text="D", theme_text_color='Primary')
                answeroptionlay.add_widget(self.c1)
                answeroptionlay.add_widget(self.q1)
                answeroptionlay.add_widget(self.c2)
                answeroptionlay.add_widget(self.q2)
                answeroptionlay.add_widget(self.c3)
                answeroptionlay.add_widget(self.q3)
                answeroptionlay.add_widget(self.c4)
                answeroptionlay.add_widget(self.q4)

                prentxbtnlay = BoxLayout(spacing=dp(5))

                self.prebtn = MDRaisedButton(text="上一題", size_hint=(
                    0.5, 0.5))
                self.prebtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Nextbtn = MDRaisedButton(text="下一題", size_hint=(0.5, 0.5))
                self.Nextbtn.bind(on_press=lambda x:SwitchQuestion(self,x))

                self.Finbtn = MDRaisedButton(text="結束答題", size_hint=(0.5, 0.5))
                self.Finbtn.bind(on_press=lambda x:show_checkwindows(x,self.testrange))

                prentxbtnlay.add_widget(self.prebtn)
                prentxbtnlay.add_widget(self.Nextbtn)
                prentxbtnlay.add_widget(self.Finbtn)

                self.prebtn.disabled = True

                main.add_widget(answeroptionlay)
                main.add_widget(prentxbtnlay)

                self.q1.text = self.answerword[str(self.i)][0]
                self.q2.text = self.answerword[str(self.i)][1]
                self.q3.text = self.answerword[str(self.i)][2]
                self.q4.text = self.answerword[str(self.i)][3]

                def usercheck(value):
                    if value.active:
                        if (value.id =="A"):
                            self.answerstore[str(self.i)]= str(self.q1.text)
                        if (value.id =="B"):
                            self.answerstore[str(self.i)]= str(self.q2.text)
                        if (value.id =="C"):
                            self.answerstore[str(self.i)]= str(self.q3.text)
                        if (value.id =="D"):
                            self.answerstore[str(self.i)]= str(self.q4.text)
                    else:
                        if str(self.i+1) in self.answerstore.keys():
                            del self.answerstore[str(self.i+1)]
                
            if (self.main_widget.ids.scr_mngr.current == "Connecttest"):
                if  questiontype =="":
                    KitchenSink.toast(self,"部分項目未選取")
                    return                    
                if questiontype == "國中英文單字":
                    self.testrange = "VocabularyLA"
                if questiontype ==  "全民英檢初級":
                    self.testrange = "VocabularyGEPTLA"
                if questiontype ==  "全民英檢中級":
                    self.testrange = "VocabularyGEPTLB"
                if questiontype =="全民英檢中高級":
                    self.testrange = "VocabularyGEPTLC"                                    
                if questiontype == "GPS情境單字測驗":
                    self.englishplace=[]
                    mythread = threading.Thread(target=self.testloadgps)
                    mythread.start()   
                    while True:
                        if (self.englishplace!=[]):
                            break
                    self.testrange = self.englishplace
                    self.my_data={'type': 'Connecttest', 'count': str(10), 'range': str(self.testrange)}
                else:
                    self.my_data = {'type':"Connecttest",'count': str(10),'range':str(self.testrange)}
                r = requests.post('http://120.96.63.79/oit_English/getvactest', data = self.my_data)
                
                data=r.json()
                self.questionid = data['questionid']
                self.answer = data['answer']
                self.totelpos = []
                self.num=0
                self.Lnum=-1 #連線條的次數            
                self.old_path = [] #放上一個起始點的位置方向
                self.old_bth = [] #放前一個點的按鈕
                self.lines = [] #線條物件
                self.ansindex=0 #ans index
                self.answerstore={} #answer
                self.disablebtn={}
                self.ans=[]
                self.anstemp=["",""]
                self.connect=[]
                self.btntemp=["",""]
                main.clear_widgets()            
                             
                item = []
                for i in range(20):
                    item.append(MDFillRoundFlatButton())
                index=0

                lnum=[] #產生隨機題目
                
                for i in range(10):
                    lnum.append(i)                    
                random.shuffle(lnum)
                centery=0.2
                for i in item:                    
                    if index %2 == 0:
                        i.pos_hint= {'center_x': 0.3, 'center_y': centery}
                        i.bind(on_release= lambda m :get(self,m,'L',m.text))
                    else:
                        i.pos_hint= {'center_x': 0.7, 'center_y': centery}
                        i.bind(on_release= lambda m :get(self,m,'R',m.text))
                        centery+=0.08
                    index+=1
                    main.add_widget(i)

                index=-1
                for i in range(0,20,2):
                    item[i].text=str(data['question'][0][lnum[index]])
                    index+=1
                
                index=-1
                for i in range(1,20,2):
                    item[i].text=str(data['question'][1][index])
                    index+=1
                
                prentxbtnlay = BoxLayout(spacing=dp(5),size_hint=(None,None),pos_hint= {'center_x': .4})

                self.PreLine = MDRaisedButton(text="上一步")
                self.PreLine.bind(on_press=lambda x:Pre(self))
                self.PreLine.disabled=True
              
                self.Reset = MDRaisedButton(text="重設")
                self.Reset.bind(on_press=lambda x:Reset(self))

                self.Finbtn = MDRaisedButton(text="結束答題")
                self.Finbtn.bind(on_press=lambda x:show_checkwindows(x,self.testrange))

                prentxbtnlay.add_widget(self.PreLine)
                prentxbtnlay.add_widget(self.Reset)
                prentxbtnlay.add_widget(self.Finbtn)


                main.add_widget(prentxbtnlay)    

                def Pre(self): #上一步        
                    self.Lnum -=1
                    if self.anstemp[0]!="" or self.anstemp[1]!="":
                        return
                    if len(self.answerstore) == 1:
                        self.PreLine.disabled=True
                    self.disablebtn[str(list(self.disablebtn.keys())[-1])][0].disabled=False
                    self.disablebtn[str(list(self.disablebtn.keys())[-1])][1].disabled=False

                    del self.disablebtn[str(list(self.disablebtn.keys())[-1])]
                    del self.answerstore[str(list(self.answerstore.keys())[-1])]
                    main.remove_widget(self.lines[-1])                    
                    del self.lines[-1]
                    if len(self.disablebtn) ==0:
                        self.Lnum=-1

                def Reset(self): #重設畫線                    
                    self.PreLine.disabled=True
                    for i in self.lines:
                        main.remove_widget(i)
                    for i in item:
                        i.disabled = False
                    self.totelpos = []
                    self.num=0
                    self.Lnum=-1 #連線條的次數            
                    self.old_path = [] #放上一個起始點的位置方向
                    self.old_bth = [] #放前一個點的按鈕
                    self.lines = [] #線條物件
                    self.ans =[]         
                    self.ansindex=0 #ans index
                    self.answerstore={} #answer
                    self.anstemp=["",""]
                    self.btntemp=["",""]
                    self.disablebtn={}

                def get(self,x,path,ans):        
                    # 如果點同一方向的按鈕，不連線只改變起始點
                    if path == self.old_path:
                        self.old_bth.disabled = False
                        self.totelpos=[]
                        self.num -= 1
                    x.disabled = True
                    self.num += 1
                    # 起始點左邊                    
                    if path == 'L' and self.num%2 == 1:                        
                        self.ansindex = data['question'][0].index(ans)
                        self.totelpos.append(x.pos[0]+x.width)
                        self.totelpos.append(x.pos[1]+x.height/2)
                        self.a = ans
                        self.old_path = path
                        self.old_bth = x
                        self.anstemp[0] = ans
                        self.btntemp[1] = x
                    elif path == 'R' and self.num%2 == 0:
                        self.anstemp[1] = ans
                        self.btntemp[0] = x
                        self.Lnum += 1
                        self.totelpos.append(x.pos[0])
                        self.totelpos.append(x.pos[1]+x.height/2)
                        self.pos = self.totelpos
                        self.lines.append(LineEllipse1(pos=x.pos))
                        main.add_widget(self.lines[self.Lnum])
                        self.totelpos=[]
                        self.old_path = ''
                        
                    # 起始點右邊
                    if path == 'R' and self.num%2 == 1:
                        self.anstemp[1] = ans
                        self.btntemp[0] = x
                        self.totelpos.append(x.pos[0])
                        self.totelpos.append(x.pos[1]+x.height/2)
                        self.a = ans
                        self.old_path = path
                        self.old_bth = x
                    elif path == 'L' and self.num%2 == 0:       
                        self.anstemp[0] = ans
                        self.btntemp[1] = x                 
                        self.ansindex = data['question'][0].index(ans)
                        self.Lnum += 1
                        self.totelpos.append(x.pos[0]+x.width)
                        self.totelpos.append(x.pos[1]+x.height/2)
                        self.pos = self.totelpos
                        self.lines.append(LineEllipse1(pos=x.pos))
                        main.add_widget(self.lines[self.Lnum])
                        # self.root.add_widget(LineEllipse1(pos=x.pos))
                        self.totelpos=[]
                        self.old_path = ''
                    if self.anstemp[0]!="" and self.anstemp[1]!="":
                        self.answerstore[str(self.ansindex)] = self.anstemp
                        self.anstemp=["",""]
                        self.disablebtn[str(self.ansindex)] = self.btntemp
                        self.btntemp=["",""]
                        self.PreLine.disabled=False

                    
            def SwitchQuestion(self,fortest): #切換題目
                if (fortest.text=="下一題"):
                    self.i+=1
                    if (self.main_widget.ids.scr_mngr.current == "Wordtest" or self.main_widget.ids.scr_mngr.current == "SpellWordtest" or self.main_widget.ids.scr_mngr.current =="Sentencetest"):
                        self.questionword.text = self.quest[self.i]
                    self.questionnum.text = "第"+str(self.i+1)+"/"+str(len(self.quest))+"題"                    
                    if (self.i == len(self.quest)-1):
                        self.Nextbtn.disabled = True
                    if (self.i != len(self.quest)-1):
                        self.prebtn.disabled = False                    
                if (fortest.text=="上一題"):
                    self.i-=1
                    if (self.main_widget.ids.scr_mngr.current == "Wordtest" or self.main_widget.ids.scr_mngr.current == "SpellWordtest"  or self.main_widget.ids.scr_mngr.current =="Sentencetest"):
                        self.questionword.text = self.quest[self.i]                          
                    self.questionnum.text = "第"+str(self.i+1)+"/"+str(len(self.quest))+"題"
                    if (self.i == 0):
                        self.prebtn.disabled = True
                    if (self.i != 0):
                        self.Nextbtn.disabled = False
                if (self.main_widget.ids.scr_mngr.current == "Showpicture"):
                    word=self.quest[self.i]                    
                    url = "https://www.shutterstock.com/zh-Hant/search/"+word
                    r = requests.get(
                        url, headers={
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'
                        })
                    soup = BeautifulSoup(r.text, 'html.parser')
                    b_tag = soup.find("img", class_="z_g_j z_g_a z_g_b")  
                    #print(b_tag.get('src'))
                    try:
                        self.imgbtn.source = b_tag.get('src')   
                    except:
                        self.imgbtn.source="www"
                    
                    #self.imgbtn.source ="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"
                if (self.main_widget.ids.scr_mngr.current == "Wordtest" or self.main_widget.ids.scr_mngr.current == "Lisitingtest" or self.main_widget.ids.scr_mngr.current == "Showpicture"):
                    self.q1.text = self.answerword[str(self.i)][0]
                    self.q2.text = self.answerword[str(self.i)][1]
                    self.q3.text = self.answerword[str(self.i)][2]
                    self.q4.text = self.answerword[str(self.i)][3]
                    if str(self.i) in self.answerstore.keys():
                        print("store="+str(self.answerstore[str(self.i)]))
                        if (self.answerstore[str(self.i)] ==self.answerword[str(self.i)][0]):
                            self.c1.active=True
                            self.c2.active=False
                            self.c3.active=False
                            self.c4.active=False
                        if (self.answerstore[str(self.i)] ==self.answerword[str(self.i)][1]):
                            self.c1.active=False
                            self.c2.active=True
                            self.c3.active=False
                            self.c4.active=False
                        if (self.answerstore[str(self.i)] ==self.answerword[str(self.i)][2]):
                            self.c1.active=False
                            self.c2.active=False
                            self.c3.active=True
                            self.c4.active=False
                        if (self.answerstore[str(self.i)] ==self.answerword[str(self.i)][3]):
                            self.c1.active=False
                            self.c2.active=False
                            self.c3.active=False
                            self.c4.active=True
                    else:
                        self.c1.active=False
                        self.c2.active=False
                        self.c3.active=False
                        self.c4.active=False   

                if (self.main_widget.ids.scr_mngr.current == "Sentencetest"):
                    self.answeroptionlay.remove_widget(self.worda)
                    
                if (self.main_widget.ids.scr_mngr.current == "SpellWordtest"):
                    for i in self.Answerarea:
                        self.mainlayout.remove_widget(i)  
                    self.Answerarea=[]
                    
                    for i in self.englishq[self.i]:
                        if i =="_":
                            self.Answerarea.append(MDTextField(id="textfiled",padding_x=0,hint_text="No helper text",padding_y=[12,0]))
                        else:
                            self.Answerarea.append(MDLabel(text=str(i), font_style="H5", theme_text_color='Primary',halign='center',pos_hint= {'center_y': 0.5}))                
                    self.textfiledid=[]
                    for i in range(len(self.Answerarea)):
                        if self.Answerarea[i].id == "textfiled":
                            self.textfiledid.append(i)
                            
                    for i in self.Answerarea:
                        i.bind(text=lambda x,y:on_text(x,y,self.Answerarea,self.answerstore,self.i,self.textfiledid))
                        i.padding_x =  [i.center[0], 0]
                        i.line_color_normal=(1,1,1,1)
                        self.mainlayout.add_widget(i)

                 
                    if (str(self.i) in self.answerstore):
                        num=0
                        for j in self.answerstore[str(self.i)]:
                            if j==" ":
                                self.Answerarea[num].text = ""
                            else:
                                self.Answerarea[num].text = j
                            num+=1
                if (self.main_widget.ids.scr_mngr.current == "Rearrangement"):
                    self.ansertboxlay.clear_widgets()                                      
                    self.textfiled.text=""
                    word = self.quest[self.i]
                    self.wordlist= word.split(" ")
                    totelwidth=0
                    totelcols=0
                    for i in word:
                        if (len(i)<7):
                            totelwidth+=88
                        else:
                            totelwidth+=len(i)*10
                        if totelwidth < main.width:
                            totelcols+=1
                    self.layout = GridLayout(rows=5,cols=totelcols,row_default_height=50)
                    self.button = []
                    btnindex=0
                    for i in self.wordlist:
                        self.button.append(MDFillRoundFlatButton(id=str(btnindex),text=str(i),pos_hint= {'center_x': 0.5, 'center_y': 0.5}))
                        btnindex+=1
                    for j in self.button:
                        j.bind(on_press=lambda x:addtextfile(self,x,self.textfiled))
                        self.layout.add_widget(j)
                    self.ansertboxlay.add_widget(self.layout)
                    if (str(self.i) in self.answerstore):
                        self.textfiled.text = self.answerstore[str(self.i)]
                        for i in self.disablebutton[str(self.i)]:
                            self.button[int(i)].disabled=True

            def show_checkwindows(self,testrange): #結束答題確認視窗                
                ok_cancel_dialog = MDDialog(
                    title='提示視窗', size_hint=(.8, .4), text_button_ok='確定',
                    text="您確定要結束答題嗎?", text_button_cancel='取消',
                    events_callback=lambda x,y:get_score(x,y,range))
                ok_cancel_dialog.open()

            def get_score(x,y,testrange): #取得成績     
                today = date.today()
                score = 0
                errortag=[]
                for i in range(len(self.answer)):
                    errortag.append("False")
                if (x == "確定"):
                    main.clear_widgets()                      
                    for i in self.answerstore.keys():
                        if self.answerstore[str(i)] == self.answer[str(i)]:
                            score +=10
                            errortag[int(i)] = "True"
                    # 取得最後作答紀錄索引
                    my_data = {'userid':self.userdata['id']}  
                    r = requests.post('http://120.96.63.79/oit_English/getlastindex', data = my_data)
                    data=r.json()
                    # 新增作答紀錄
                    my_data = {'userid':self.userdata['id'],"index":data['content'],"range":self.my_data['range'],"type":self.main_widget.ids.scr_mngr.current,"date":today,"score":score,"cscore":score}
                    r = requests.post('http://120.96.63.79/oit_English/createhistroy', data = my_data)
                    
                    # 新增錯誤紀錄，用來計算使用者錯誤單字                    
                    my_data = {'userid':self.userdata['id'],'index':data['content'],'range':self.my_data['range'],"word": str(self.questionid),'errortag':str(errortag)}    
                    r = requests.post('http://120.96.63.79/oit_English/inserterrorword', data = my_data)
                    if self.main_widget.ids.scr_mngr.current == "Connecttest":
                        finishlab = MDLabel(font_style='H3',text="恭喜完成作答", theme_text_color='Primary',halign='center',pos_hint= {'center_x': 0.5, 'center_y': 0.8})
                        scorelab =  MDLabel(font_style='H3',text="分數："+str(int(score)), theme_text_color='Primary',halign='center',pos_hint= {'center_x': 0.5, 'center_y': 0.6})
                        integralab =  MDLabel(font_style='H3',text="積分+"+str(int(score)), theme_text_color='Primary',halign='center',pos_hint= {'center_x': 0.5, 'center_y': 0.4})
                        finishlab.color=(1,1,1,1)
                        scorelab.color=(1,1,1,1)
                        integralab.color=(1,1,1,1)
                        rebtn=MDRaisedButton(text="重新測驗", size_hint=(0.5, 0.2),pos_hint= {'center_x': 0.5, 'center_y': 0.2})
                        rebtn.bind(on_press=lambda x:self.creattestlay(main,x))
                        main.add_widget(finishlab)
                        main.add_widget(scorelab)
                        main.add_widget(integralab)                                        
                        main.add_widget(rebtn)
                    else:
                        finishlab = MDLabel(font_style='H3',text="恭喜完成作答", theme_text_color='Primary',halign='center')
                        scorelab =  MDLabel(font_style='H3',text="分數："+str(int(score)), theme_text_color='Primary',halign='center')
                        integralab =  MDLabel(font_style='H3',text="積分+"+str(int(score)), theme_text_color='Primary',halign='center')
                        finishlab.color=(1,1,1,1)
                        scorelab.color=(1,1,1,1)
                        integralab.color=(1,1,1,1)
                        rebtn=MDRaisedButton(text="重新測驗", size_hint=(0.5, 0.5),pos_hint= {'center_x': 0.5, 'center_y': 0.5})
                        rebtn.bind(on_press=lambda x:self.creattestlay(main,x))
                        main.add_widget(finishlab)
                        main.add_widget(scorelab)
                        main.add_widget(integralab)                                        
                        main.add_widget(rebtn)
                    
    def build(self):
        self.main_widget = Builder.load_string(main_widget_kv)
        return self.main_widget

    def testloadgps(self):#使用產生地點資料
        try:
            self.start(1000, 0)
            while True:
                if self.gpsA != 0:
                    gps.stop()
                    break
        except NotImplementedError:
            import traceback
            traceback.print_exc()
            KitchenSink.toast(self,'本裝置不支援GPS功能，使用虛擬座標')        
            self.gpsA=25.0016368
            self.gpsB=121.4553579
                
        google_key = "AIzaSyC3Zs_Aln2ewhniP1cvuCb5hajMZyYbJxE"
        gmaps = googlemaps.Client(key=google_key)
        # 資料庫的類別
        types_list = [['restaurant', '餐廳'], ['food', '小吃'], ['bakery', '麵包店'], ['clothing store', '服飾店'], ['school', '學校'],
                      ['park', '公園'], ['cafe', '咖啡店'], ['convenience store', '便利店'], ['spa', 'SPA'], ['shopping mall', '購物中心'],
                      ['movie theater', '電影院'], ['home goods store', '家居用品店'], ['stadium', '體育場'], ['parking', '停車場'],
                      ['lodging', '住宿'], ['real estate agency', '房地產仲介'], ['health', '醫院診所'], ['atm', '自動提款機'],
                      ['political', '政治設施'], ['bank', '銀行'], ['beauty salon', '美容院'], ['electronics store', '電子產品商店'],
                      ['bar', '酒吧'], ['hair care', '頭髮護理'], ['bus station', '巴士站'], ['point of interest','景點'], ['hardware store','五金店'],
                      ['police','警察局'], ['museum','博物館'], ['grocery or supermarket','雜貨店或便利商店'], ['place of worship','寺廟'],
                      ['transit station','公車站'], ['train station','火車站'], ['route','道路'], ['store','商店'], ['storage','批發'],
                      ['jewelry store','珠寶店'], ['department store','百貨公司'], ['gas station','加油站'], ['travel agency','旅行社'],
                      ['airport','機場'], ['night club','夜店'], ['city hall','政府'], ['florist','花店'], ['pharmacy','藥房'], ['church','教會'],
                      ['establishment','機構'], [ 'university','大學'], ['fire station','消防局'], ['subway station','地鐵站'],
                      ['car repair','汽車保養廠']]
        # 要刪除的類別
        remove_list = [ 'colloquial_area', 'locality','neighborhood', 'finance']

        # 剛get的類別
        temp = []
        # 處理過後的類別
        types = []
        
        nearby_results = gmaps.places_nearby(
            location=(self.gpsA,self.gpsB), radius=100)  # 以取得座標為中心 ，半徑X公尺的店家資訊
       
        nearby_results = nearby_results['results']
        for nearby_result in nearby_results:
            place_id = nearby_result['place_id']
            detail_results = gmaps.place(place_id, language="zh-tw")
            for t in detail_results['result']['types']:
                temp.append(t)
                # print(detail_results['result']['name'])  # 印出名字
                # print(detail_results['result']['types'])  # 印出類別

        temp = set(temp)  # 移除重複
        for s in temp:
            types.append(s)

        for er in remove_list:
            if er in types:
                types.remove(er)  # 移除不必要的類別

        for num in range(len(types)):
            n = types[num]
            types[num] = n.replace('_', ' ')

            # print(types)
            # print('您的附近有：',end='')
            
        self.place = [] #中文地點類別
        self.englishplace = [] #英文地點類別
        for i in types:
            for j in range(len(types_list)):
                if i == types_list[j][0]:
                    if i != types[-1]:
                        self.place.append(types_list[j][1])
                        self.englishplace.append(types_list[j][0])
        self.gpsrange = self.englishplace
        toastword=""
        for i in self.place:
            toastword+=i+"，"
        toast("偵測到的地址:"+toastword[0:len(toastword)-1])

    def start(self, minTime, minDistance):
        gps.start(minTime, minDistance)

    def stop(self):
        gps.stop()

    def on_location(self, **kwargs):
        self.gpsA = kwargs["lat"]
        self.gpsB = kwargs['lon']

    def on_status(self, stype, status):
        print(self.gps_status)
        # self.gps_status = 'type={}\n{}'.format(stype, status)

    def on_pause(self):
        gps.stop()
        return True

    def on_resume(self):
        gps.start(1000, 0)
        pass

class AvatarSampleWidget(ILeftBody, Image):
    pass

class OftenWrongCard(BoxLayout):
    callback = ObjectProperty(lambda x: None)
    
    chinese = StringProperty()
    part = StringProperty()
    example = StringProperty()
    examplec = StringProperty()
    def __init__(self, chinese,part, example,examplec,**kwargs):
        super(OftenWrongCard, self).__init__(**kwargs)
        self.chinese =chinese
        self.part=part
        self.example=example
        self.examplec=examplec

class TextPopup(Popup):
    title = StringProperty()
    color = StringProperty()
    answer = ObjectProperty()

    def __init__(self, title, **kwargs):
        super(TextPopup, self).__init__(**kwargs)
        self.set_description(title)

    def set_description(self, title):
        self.title = title

    def get_answer(self):
        return self.answer.text

class LearningPopup(Popup):
    title = StringProperty()
    color = StringProperty()
    answer = ObjectProperty()
    def __init__(self, title, **kwargs):
        super(LearningPopup, self).__init__(**kwargs)
        self.set_description(title)
    def set_description(self, title):
        self.title = title

class speaktestpopup(Popup): 
    title = StringProperty()
    word = StringProperty()
    def __init__(self,word, **kwargs):
        super(speaktestpopup, self).__init__(**kwargs)
        self.word = word
    def start_listening(self):
        if stt.listening:
            self.stop_listening()
            return

        start_button = self.ids.start_button
        start_button.text = '停止'

        self.ids.results.text = ''
        self.ids.partial.text = ''

        stt.start()

        Clock.schedule_interval(self.check_state, 1 / 5)
    
    def stop_listening(self):
        start_button = self.ids.start_button
        start_button.text = '開始聆聽'

        stt.stop()
        self.update()

        Clock.unschedule(self.check_state)

    def check_state(self, dt):
        # if the recognizer service stops, change UI
        if not stt.listening:
            self.stop_listening()
    
    def update(self):
        self.ids.partial.text = '\n'.join(stt.partial_results)
        self.ids.results.text = '\n'.join(stt.results)        

class wordtesthistroylist(BoxLayout):
    callback = ObjectProperty(lambda x: None)
    index = NumericProperty()
    ttype= StringProperty()
    trange = StringProperty()
    score = NumericProperty()
    date = StringProperty()

    def __init__(self, index,ttype,trange,score,date, **kwargs):
        super(wordtesthistroylist, self).__init__(**kwargs)
        self.index=index
        self.ttype= ttype
        self.trange=trange
        self.score= score
        self.date=date
    
class Choicequestionpopup(Popup):
    title = StringProperty()
    color = StringProperty()
    answer = ObjectProperty()
    def __init__(self, title, **kwargs):
        super(Choicequestionpopup, self).__init__(**kwargs)
        self.set_description(title)
    def set_description(self, title):
        self.title = title    

class Leakwordpopup(Popup):
    title = StringProperty()
    color = StringProperty()
    answer = ObjectProperty()

   
    def __init__(self, title, **kwargs):
        super(Leakwordpopup, self).__init__(**kwargs)
        self.set_description(title)
        self.num=0
    def set_description(self, title):
        self.title = title
    def add(self,widget):
        self.num+=1

        layM = BoxLayout(id=str(self.num),height=dp(200),spacing=dp(50),size_hint_y=None,orientation= 'vertical')
        
        layM.add_widget(MDLabel(text="第"+str(self.num)+"題",pos_hint= {'center_x': 0.5, 'center_x': 0.5}))

        layA = BoxLayout(spacing=dp(50))
        layA.add_widget(MDLabel(text="選項A"))
        layA.add_widget(MDTextField(text="",pos_hint= {'center_y': 0.5,'center_x': 0.3}))
        layA.add_widget(MDLabel(text="選項B"))
        layA.add_widget(MDTextField(text="",pos_hint= {'center_y': 0.5}))

        layB = BoxLayout(spacing=dp(50))
        layB.add_widget(MDLabel(text="選項C"))
        layB.add_widget(MDTextField(text="",pos_hint= {'center_y': 0.5,'center_x': 0.3}))
        layB.add_widget(MDLabel(text="選項D"))
        layB.add_widget(MDTextField(text="",pos_hint= {'center_y': 0.5}))

        layC = BoxLayout(spacing=dp(10))
        layC.add_widget(MDLabel(text="答案選擇"))
        layC.add_widget(MDCheckbox(group="1",size_hint=(None, None),pos_hint= {'center_x': .5, 'center_y': .5}))
        layC.add_widget(MDLabel(text="A"))
        layC.add_widget(MDCheckbox(group="1",size_hint=(None, None),pos_hint= {'center_x': .5, 'center_y': .5}))
        layC.add_widget(MDLabel(text="B"))
        layC.add_widget(MDCheckbox(group="1",size_hint=(None, None),pos_hint= {'center_x': .5, 'center_y': .5}))
        layC.add_widget(MDLabel(text="C"))
        layC.add_widget(MDCheckbox(group="1",size_hint=(None, None),pos_hint= {'center_x': .5, 'center_y': .5}))                        
        layC.add_widget(MDLabel(text="D"))

        widget.spacing=dp(50)

        layM.add_widget(layA)
        layM.add_widget(layB)
        layM.add_widget(layC)


        widget.add_widget(layM)

class SignupPopup(Popup):
    title = StringProperty()
    def __init__(self, title, **kwargs):
        super(SignupPopup, self).__init__(**kwargs)
        self.set_description(title)
    def set_description(self, title):
        self.title = title 
    
class LineEllipse1(Widget):
    pos = ListProperty()
    def __init__(self, pos, **kwargs):        
        super(LineEllipse1, self).__init__(**kwargs)
        self.set_pos(pos)
    def set_pos(self,pos):
        self.pos=pos

class NewGpswordPopup(Popup): #新增單字彈出式視窗
    title = StringProperty()
    num =  StringProperty()
    word =  StringProperty()
    chinese =  StringProperty()
    example =  StringProperty()
    cexample =  StringProperty()
    chinese = StringProperty()
    etype = StringProperty()
    wordpos = StringProperty()
    ctype = StringProperty()
    type =  StringProperty()
    def __init__(self, title,num,type,**kwargs):
        super(NewGpswordPopup, self).__init__(**kwargs)
        self.num = num
        self.type = type
        self.title=title

        my_data={'num': num,"etype":type}
        r = requests.post('http://120.96.63.79/oit_English/getgpswordinformation', data = my_data)
        data=r.json()
        if data['gps_class_number'] != "null":
            self.ids.ctype.disabled=True
            self.ids.etype.disabled=True
            self.ids.newcgbtn.text="修改"
            self.num =str(data['gps_class_number'])
            self.word=str(data['gps_class_word'])
            self.etype =str(data['gps_class_etype'])
            self.ctype=str(data['gps_class_ctype'])
            self.wordpos = str(data['gps_class_pos'])
            self.chinese = str(data['gps_class_chinese'])
            self.example = str(data['gps_class_example'])
            self.cexample = str(data['gps_class_cexample'])
        else:
            self.ids.ctype.disabled=False
            self.ids.etype.disabled=False
            self.ids.newcgbtn.text="新增"
            self.num = str(0)
            self.word=""
            self.etype ="請選擇"
            self.ctype="請選擇"
            self.wordpos = "請選擇"
            self.chinese =""
            self.example = ""
            self.cexample = ""        

        self.wordposs=[
                {'viewclass': 'MDRaisedButton',
                'text': 'adj.(形容詞)',
                'on_release': lambda : self.changepos("adj.(形容詞)")},
                {'viewclass': 'MDRaisedButton',
                'text': 'adv.(副詞)',
                'on_release': lambda :self.changepos("adv.(副詞)")}, 
                {'viewclass': 'MDRaisedButton',
                'text': 'n.(名詞)',
                'on_release': lambda :self.changepos("n.(名詞)")},
                {'viewclass': 'MDRaisedButton',
                'text': 'vi.(不及物動詞)',
                'on_release': lambda :self.changepos("vi.(不及物動詞)")},
                {'viewclass': 'MDRaisedButton',
                'text': 'vt.(及物動詞)',
                'on_release': lambda :self.changepos("vt.(及物動詞)")},
                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
                ]
        self.newgpswordtype = [
                {'viewclass': 'MDRaisedButton',
                'text': 'SPA',
                'on_release': lambda : self.changetype(["SPA","spa"])},
                {'viewclass': 'MDRaisedButton',
                'text': '五金店',
                'on_release': lambda :self.changetype(["五金店","hardware store"])}, 
                {'viewclass': 'MDRaisedButton',
                'text': '住宿',
                'on_release': lambda :self.changetype(["住宿","lodging"])},
                {'viewclass': 'MDRaisedButton',
                'text': '便利店',
                'on_release': lambda :self.changetype(["便利店","convenience store"])},
                {'viewclass': 'MDRaisedButton',
                'text': '停車處',
                'on_release': lambda :self.changetype(["停車處","parking"])},
                {'viewclass': 'MDRaisedButton',
                'text': '健康設施',
                'on_release': lambda :self.changetype(["健康設施","health"])},
                {'viewclass': 'MDRaisedButton',
                'text': '公園',
                'on_release': lambda :self.changetype(["公園","park"])},
                {'viewclass': 'MDRaisedButton',
                'text': '公車站',
                'on_release': lambda :self.changetype(["公車站","bus station"])},
                {'viewclass': 'MDRaisedButton',
                'text': '加油站',
                'on_release': lambda :self.changetype(["加油站","gas station"])},
                {'viewclass': 'MDRaisedButton',
                'text': '博物館',
                'on_release': lambda :self.changetype(["博物館","museum"])},
                {'viewclass': 'MDRaisedButton',
                'text': '咖啡店',
                'on_release': lambda :self.changetype(["咖啡店","cafe"])},
                {'viewclass': 'MDRaisedButton',
                'text': '商店',
                'on_release': lambda :self.changetype(["商店","store"])},
                {'viewclass': 'MDRaisedButton',
                'text': '地鐵站',
                'on_release': lambda :self.changetype(["地鐵站","subway station"])},  
                {'viewclass': 'MDRaisedButton',
                'text': '夜店',
                'on_release': lambda :self.changetype(["夜店","night club"])},  
                {'viewclass': 'MDRaisedButton',
                'text': '家居用品店',
                'on_release': lambda :self.changetype(["家居用品店","home goods store"])},  
                {'viewclass': 'MDRaisedButton',
                'text': '大學',
                'on_release': lambda :self.changetype(["大學","university"])},  
                {'viewclass': 'MDRaisedButton',
                'text': '學校',
                'on_release': lambda :self.changetype(["學校","school"])},  
                {'viewclass': 'MDRaisedButton',
                'text': '寺廟',
                'on_release': lambda :self.changetype(["寺廟","place of worship"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '小吃',
                'on_release': lambda :self.changetype(["小吃","food"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '巴士站',
                'on_release': lambda :self.changetype(["巴士站","transit station"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '房地產仲介',
                'on_release': lambda :self.changetype(["房地產仲介","real estate agency"])},    
                {'viewclass': 'MDRaisedButton',
                'text': '批發',
                'on_release': lambda :self.changetype(["批發","storage"])},                                                                                                                                                                                                                                                                                               
                {'viewclass': 'MDRaisedButton',
                'text': '政府',
                'on_release': lambda :self.changetype(["政府","city hall"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '政治設施',
                'on_release': lambda :self.changetype(["政治設施","political"])},  
                {'viewclass': 'MDRaisedButton',
                'text': '教會',
                'on_release': lambda :self.changetype(["教會","church"])},  
                {'viewclass': 'MDRaisedButton',
                'text': '旅行社',
                'on_release': lambda :self.changetype(["旅行社","travel agency"])},    
                {'viewclass': 'MDRaisedButton',
                'text': '景點',
                'on_release': lambda :self.changetype(["景點","point of interest"])},     
                {'viewclass': 'MDRaisedButton',
                'text': '服飾店',
                'on_release': lambda :self.changetype(["服飾店","clothing store"])},     
                {'viewclass': 'MDRaisedButton',
                'text': '機場',
                'on_release': lambda :self.changetype(["機場","airport"])},                                                                                                                                                   
                {'viewclass': 'MDRaisedButton',
                'text': '機構',
                'on_release': lambda :self.changetype(["機構","establishment"])}, 
                {'viewclass': 'MDRaisedButton',
                'text': '汽車保養廠',
                'on_release': lambda :self.changetype(["汽車保養廠","car repair"])}, 
                {'viewclass': 'MDRaisedButton',
                'text': '消防局',
                'on_release': lambda :self.changetype(["消防局","fire station"])}, 
                {'viewclass': 'MDRaisedButton',
                'text': '火車站',
                'on_release': lambda :self.changetype(["火車站","train station"])},
                {'viewclass': 'MDRaisedButton',
                'text': '珠寶店',
                'on_release': lambda :self.changetype(["珠寶店","jewelry store"])},      
                {'viewclass': 'MDRaisedButton',
                'text': '百貨公司',
                'on_release': lambda :self.changetype(["百貨公司","department store"])},      
                {'viewclass': 'MDRaisedButton',
                'text': '美容院',
                'on_release': lambda :self.changetype(["美容院","beauty salon"])},      
                {'viewclass': 'MDRaisedButton',
                'text': '自動提款機',
                'on_release': lambda :self.changetype(["自動提款機","atm"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '花店',
                'on_release': lambda :self.changetype(["花店","florist"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '藥房',
                'on_release': lambda :self.changetype(["藥房","pharmacy"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '警察局',
                'on_release': lambda :self.changetype(["警察局","police"])},    
                {'viewclass': 'MDRaisedButton',
                'text': '購物中心',
                'on_release': lambda :self.changetype(["購物中心","shopping mall"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '道路',
                'on_release': lambda :self.changetype(["道路","route"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '酒吧',
                'on_release': lambda :self.changetype(["酒吧","bar"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '銀行',
                'on_release': lambda :self.changetype(["銀行","bank"])},         
                {'viewclass': 'MDRaisedButton',
                'text': '雜貨店或便利商店',
                'on_release': lambda :self.changetype(["雜貨店或便利商店","grocery or supermarket"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '電子產品商店',
                'on_release': lambda :self.changetype(["電子產品商店","electronics store"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '電影院',
                'on_release': lambda :self.changetype(["電影院","movie theater"])},   
                {'viewclass': 'MDRaisedButton',
                'text': '頭髮護理',
                'on_release': lambda :self.changetype(["頭髮護理","hair care"])},    
                {'viewclass': 'MDRaisedButton',
                'text': '餐廳',
                'on_release': lambda :self.changetype(["餐廳","restaurant"])},
                {'viewclass': 'MDRaisedButton',
                'text': '體育場',
                'on_release': lambda :self.changetype(["體育場","stadium"])}, 
                {'viewclass': 'MDRaisedButton',
                'text': '麵包店',
                'on_release': lambda :self.changetype(["麵包店","bakery"])},
                ]
    def changetype(self,word):
        self.ids.etype.text=word[1]
        self.ids.ctype.text = word[0]
    def changepos(self,pos):
        self.ids.pos.text=pos
    def insertnewgpsword(self,num,word,chinese,ctype,etype,pos,example,cexample):        
        if ctype == "請選擇" or etype == "請選擇" or pos == "請選擇" :
            toast("部分項目未填")
            return            
        if num == "0": #新增
            my_data={'word': word,"ctype":ctype,"etype":etype,"chinese":chinese,"pos":pos,"example":example,"cexample":cexample}
            r = requests.post('http://120.96.63.79/oit_English/gpswordadmininsertword', data = my_data)
        else: #更新
            my_data={'num':num,'word': word,"ctype":ctype,"etype":etype,"chinese":chinese,"pos":pos,"example":example,"cexample":cexample}
            r = requests.post('http://120.96.63.79/oit_English/gpswordadminupdateword', data = my_data)
            print(my_data)
            data=r.json()
            
        self.dismiss()
        toast("完成!")
            
    def delnewgpsword(self,num,word,chinese,ctype,etype,pos,example,cexample):        
        my_data={'num': num,"etype":etype}
        r = requests.post('http://120.96.63.79/oit_English/gpswordadmindelword', data = my_data)
        data=r.json()
        self.dismiss()
        toast("完成!")
if __name__ == '__main__':
    KitchenSink().run()


'''
canvas:
    Color: 
        rgba: 0.5, 0.5, 0.5, 1
    Rectangle:
        pos: self.x + 10, self.y + 10
        size: self.width - 20, self.height - 20
'''

'''
Python for android ended.

Traceback
'''
