import os
import time
import threading
from kivymd.uix.label import MDLabel
from kivy.uix.scrollview import ScrollView
import googlemaps
from kivy.uix.popup import Popup
from kivy.clock import Clock
import sqlite3
from kivymd.uix.list import MDList
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.metrics import dp
from kivymd.uix.selectioncontrol import MDCheckbox
from kivy.uix.image import Image
from kivymd.uix.tab import MDTabsBase
from kivy.properties import ObjectProperty, StringProperty, ListProperty, NumericProperty
import requests
from plyer import gps
from kivymd.utils.cropimage import crop_image
from kivymd.uix.button import MDIconButton
from kivy.properties import StringProperty
from kivymd.uix.behaviors.ripplebehavior import CircularRippleBehavior
import googlemaps
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.boxlayout import BoxLayout
from kivymd.uix.list import ILeftBody, ILeftBodyTouch, IRightBodyTouch
from kivymd.uix.list import OneLineListItem
from kivy.core.window import Window
import time
import Zoom
from kivy.metrics import Metrics
from kivymd.uix.managerswiper import MDSwiperPagination
from queue import Queue
from googletrans import Translator
from bs4 import BeautifulSoup
from kivymd.uix.card import MDCard
from kivy.utils import platform as core_platform
from googletrans import Translator
from urllib.parse import quote

def toast(text):
    # FIXME: crush with Python3.
    try:
        from kivymd.toast import toast
    except TypeError:
        from kivymd.toast.kivytoast import toast
    toast(text)

menu = '''
<Menu@Screen>
    name: 'Menu'
    do_scroll_x: False
    BoxLayout:
        canvas:
            Rectangle:
                size: self.size
                pos: self.pos
                source: './assets/coffee_crop.jpg'
        height: root.height
        spacing:20
        padding:[40,50,40,20]
        orientation: 'vertical'
        size_hint_y: None
        BoxLayout:
            size_hint: 1,.5
            spacing:20
            MDRaisedButton:
                on_release:
                    app.show_SpellWordtestPage()
                    app.set_title_toolbar(self.text)
                size_hint: .3,1
                theme_text_color: 'Primary'
                _radius:30.0
                md_bg_color:(0.9215686274509804,0.2549019607843137,0.2549019607843137,1)
                BoxLayout:
                    orientation: 'vertical'
                    Image:
                        source: 'assets/pen.png'
                        size: self.texture_size
                    Label:
                        markup: True
                        text: '拼字測驗'
                        bold: True
                        font_size:root.width/20
                        
            MDRaisedButton:
                _radius:30.0
                md_bg_color:(0.9803921568627,0.7764705882352,0.03921568627450,1)
                size_hint: .7,1
                font_style: 'Body1'
                theme_text_color: 'Primary'
                on_release: app.on_call_Learningpopup()
                BoxLayout:
                    orientation: 'vertical'
                    Image:
                        source: 'assets/book.png'
                        size: self.texture_size
                    Label:
                        markup: True
                        text: '單字學習'
                        bold: True
                        font_size:root.width/20
        BoxLayout:
            spacing:20
            BoxLayout:
                spacing:20                
                orientation: 'vertical'
                MDRaisedButton:
                    _radius:30.0
                    md_bg_color:(0.298039215686,0.6980392156862,0.2980392156862,1)
                    size_hint: 1,0.45
                    font_style: 'Body1'
                    theme_text_color: 'Primary'
                    on_release: app.show_WordtestPage()
                    BoxLayout:
                        orientation: 'vertical'
                        Image:
                            source: 'assets/book.png'
                            size: self.texture_size
                        Label:
                            markup: True
                            text: '一般測驗'
                            bold: True
                            font_size:root.width/20
                MDRaisedButton:
                    _radius:30.0
                    md_bg_color:(0.03921568627450,0.4784313725490,0.9803921568627,1)
                    size_hint: 1,0.55
                    font_style: 'Body1'
                    theme_text_color: 'Primary'
                    on_release: 
                        app.show_GpstestPage()
                        app.set_title_toolbar(self.text)
                    BoxLayout:
                        orientation: 'vertical'
                        Image:
                            source: 'assets/book.png'
                            size: self.texture_size
                        Label:
                            markup: True
                            text: '情境感知單字'
                            bold: True
                            font_size:root.width/20
            BoxLayout:
                orientation: 'vertical'
                spacing:20
                MDRaisedButton:
                    _radius:30.0
                    md_bg_color:(0.0588235294117,0.2117647058823,0.98039215686274,1)
                    size_hint: 1,.7
                    font_style: 'Body1'
                    theme_text_color: 'Primary'
                    on_release: 
                        app.show_CoceptMapPage()
                    BoxLayout:
                        orientation: 'vertical'
                        Image:
                            source: 'assets/concept.png'
                            size: self.texture_size
                        Label:
                            markup: True
                            text: '關聯表'
                            bold: True
                            font_size:root.width/20
                MDRaisedButton:
                    _radius:30.0
                    md_bg_color:(0.62745098039215,0.05882352941176,0.98039215686274,1)
                    size_hint: 1,.3
                    font_style: 'Body1'
                    theme_text_color: 'Primary'
                    on_release:  
                        app.show_sentencetestPage()
                        app.set_title_toolbar(self.text)
                    BoxLayout:
                        orientation: 'vertical'
                        Image:
                            source: 'assets/book.png'
                            size: self.texture_size
                        Label:
                            markup: True
                            text: '句子測驗'
                            bold: True
                            font_size:root.width/30
        MDRaisedButton:
            _radius:30.0
            md_bg_color:(0.98039215686274,0.62745098039215,0.05882352941176,0.88)
            size_hint: 1,.5
            font_style: 'Body1'
            theme_text_color: 'Primary'
            on_release: app.show_ProfilePage()
            BoxLayout:
                orientation: 'vertical'
                Image:
                    source: 'assets/book.png'
                    size: self.texture_size
                Label:
                    markup: True
                    text: '儀錶板'
                    bold: True
                    font_size:root.width/20
'''
toeic = '''
#:import OneLineIconListItem kivymd.uix.list.OneLineIconListItem
#:import images_path kivymd.images_path
#:import MDTextFieldRect kivymd.uix.textfield.MDTextField
#:import MDIconButton kivymd.uix.button.MDIconButton
#:import ThreeLineListItem kivymd.uix.list.ThreeLineListItem
#:import VocablaryLA Vocabularyword

<vocablaryLAitem@TwoLineAvatarListItem>:
    on_release:app.show_user_example_animation_card(root.text)
    type: "two-line"
    text: "Two-line item..."
    AvatarSampleWidget:
        source: './assets/voicon.png'


<VocablaryLA@Screen>
    name: 'vocablaryLA'
    on_enter: app.set_list_md_icons(screen="vocablaryLA")

    BoxLayout:
        orientation: 'vertical'
        spacing: dp(10)
        padding: dp(20)

        BoxLayout:
            size_hint_y: None
            height: self.minimum_height

            MDIconButton:
                icon: 'magnify'

            MDTextField:
                id: search_field
                hint_text: '12312323'
                on_text: app.set_list_md_icons(self.text, True,screen="vocablaryLA")

        RecycleView:
            id: rv
            key_viewclass: 'viewclass'
            key_size: 'height'

            RecycleBoxLayout:
                padding: dp(10)
                default_size: None, dp(48)
                default_size_hint: 1, None
                size_hint_y: None
                height: self.minimum_height
                orientation: 'vertical'
'''

vocablaryLA = '''
#:import OneLineIconListItem kivymd.uix.list.OneLineIconListItem
#:import images_path kivymd.images_path
#:import MDTextFieldRect kivymd.uix.textfield.MDTextField
#:import MDIconButton kivymd.uix.button.MDIconButton
#:import ThreeLineListItem kivymd.uix.list.ThreeLineListItem
#:import VocablaryLA Vocabularyword

<vocablaryLAitem@TwoLineAvatarListItem>:
    on_release:app.show_user_example_animation_card(root.text)
    type: "two-line"
    text: "Two-line item..."
    AvatarSampleWidget:
        source: './assets/voicon.png'


<VocablaryLA@Screen>
    name: 'vocablaryLA'
    on_enter: app.set_list_md_icons(screen="vocablaryLA")

    BoxLayout:
        orientation: 'vertical'
        spacing: dp(10)
        padding: dp(20)

        BoxLayout:
            size_hint_y: None
            height: self.minimum_height

            MDIconButton:
                icon: 'magnify'

            MDTextField:
                id: search_field
                hint_text: '單字搜尋'
                on_text: app.set_list_md_icons(self.text, True,screen="vocablaryLA")

        RecycleView:
            id: rv
            key_viewclass: 'viewclass'
            key_size: 'height'

            RecycleBoxLayout:
                padding: dp(10)
                default_size: None, dp(48)
                default_size_hint: 1, None
                size_hint_y: None
                height: self.minimum_height
                orientation: 'vertical'
'''

vocabluaryGEPTLA = ''' 
#:import images_path kivymd.images_path
#:import MDTextFieldRect kivymd.uix.textfield.MDTextField
#:import MDIconButton kivymd.uix.button.MDIconButton
#:import ThreeLineListItem kivymd.uix.list.ThreeLineListItem
#:import VocabluaryGEPTLA Vocabularyword.VocabluaryGEPTLA

<vocabluaryGEPTLAitem@TwoLineAvatarListItem>:
    on_release:app.show_user_example_animation_card(root.text)
    type: "two-line"
    text: "Two-line item..."
    AvatarSampleWidget:
        source: './assets/voicon.png'

<VocabluaryGEPTLA@Screen>
    name: 'vocabluaryGEPTLA'
    on_enter: app.set_list_md_icons(screen="vocabluaryGEPTLA",tee=VocabluaryGEPTLA)

    BoxLayout:
        orientation: 'vertical'
        spacing: dp(10)
        padding: dp(20)

        BoxLayout:
            size_hint_y: None
            height: self.minimum_height

            MDIconButton:
                icon: 'magnify'

            MDTextField:
                id: search_field
                hint_text: '單字搜尋'
                on_text: app.set_list_md_icons(self.text, True,screen="vocabluaryGEPTLA",tee=VocabluaryGEPTLA)

        RecycleView:
            id: rv
            key_viewclass: 'viewclass'
            key_size: 'height'

            RecycleBoxLayout:
                padding: dp(10)
                default_size: None, dp(48)
                default_size_hint: 1, None
                size_hint_y: None
                height: self.minimum_height
                orientation: 'vertical'
'''

vocabluaryGEPTLB = '''
#:import images_path kivymd.images_path
#:import MDTextFieldRect kivymd.uix.textfield.MDTextField
#:import MDIconButton kivymd.uix.button.MDIconButton
#:import ThreeLineListItem kivymd.uix.list.ThreeLineListItem
#:import VocabluaryGEPTLB Vocabularyword.VocabluaryGEPTLB

<vocabluaryGEPTLBitem@TwoLineAvatarListItem>:
    on_release:app.show_user_example_animation_card(root.text)
    type: "two-line"
    text: "Two-line item..."
    AvatarSampleWidget:
        source: './assets/voicon.png'

<VocabluaryGEPTLB@Screen>
    name: 'vocabluaryGEPTLB'
    on_enter: app.set_list_md_icons(screen="vocabluaryGEPTLB",tee=VocabluaryGEPTLB)

    BoxLayout:
        orientation: 'vertical'
        spacing: dp(10)
        padding: dp(20)

        BoxLayout:
            size_hint_y: None
            height: self.minimum_height

            MDIconButton:
                icon: 'magnify'

            MDTextField:
                id: search_field
                hint_text: '單字搜尋'
                on_text: app.set_list_md_icons(self.text, True,screen="vocabluaryGEPTLB",tee=VocabluaryGEPTLB)

        RecycleView:
            id: rv
            key_viewclass: 'viewclass'
            key_size: 'height'

            RecycleBoxLayout:
                padding: dp(10)
                default_size: None, dp(48)
                default_size_hint: 1, None
                size_hint_y: None
                height: self.minimum_height
                orientation: 'vertical'
'''


vocabluaryGEPTLC = '''
#:import images_path kivymd.images_path
#:import MDTextFieldRect kivymd.uix.textfield.MDTextField
#:import MDIconButton kivymd.uix.button.MDIconButton
#:import ThreeLineListItem kivymd.uix.list.ThreeLineListItem
#:import VocabluaryGEPTLC Vocabularyword.VocabluaryGEPTLC

<vocabluaryGEPTLCitem@TwoLineAvatarListItem>:
    on_release:app.show_user_example_animation_card(root.text)
    type: "two-line"
    text: "Two-line item..."
    AvatarSampleWidget:
        source: './assets/voicon.png'

<VocabluaryGEPTLC@Screen>
    name: 'vocabluaryGEPTLC'
    on_enter: app.set_list_md_icons(screen="vocabluaryGEPTLC",tee=VocabluaryGEPTLC)

    BoxLayout:
        orientation: 'vertical'
        spacing: dp(10)
        padding: dp(20)

        BoxLayout:
            size_hint_y: None
            height: self.minimum_height

            MDIconButton:
                icon: 'magnify'

            MDTextField:
                id: search_field
                hint_text: '單字搜尋'
                on_text: app.set_list_md_icons(self.text, True,screen="vocabluaryGEPTLC",tee=VocabluaryGEPTLC)

        RecycleView:
            id: rv
            key_viewclass: 'viewclass'
            key_size: 'height'

            RecycleBoxLayout:
                padding: dp(10)
                default_size: None, dp(48)
                default_size_hint: 1, None
                size_hint_y: None
                height: self.minimum_height
                orientation: 'vertical'
'''

cards = '''
#:import MDBottomNavigation kivymd.uix.bottomnavigation.MDBottomNavigation
#:import MDBottomNavigationItem kivymd.uix.bottomnavigation.MDBottomNavigationItem  
<Cards@Screen>
    name: 'cards'
    on_enter:app.add_cards(tabsa,tabsb,tabsc,tabsd)
    
    MDBottomNavigation:
        id: panel

        MDBottomNavigationItem:
            name: 'files1'
            text: '國中英文單字'
            icon: 'language-python'
            ScrollView:
                BoxLayout:
                    orientation: 'vertical'
                    size_hint_y: None
                    height: self.minimum_height
                    spacing: dp(10)
                    pos_hint: {'center_x': .5, 'center_y': .5}
                    id:tabsa
                

        MDBottomNavigationItem:
            name: 'files2'
            text: '全民英檢初級單字'
            icon: 'language-cpp'

            ScrollView:
                BoxLayout:
                    orientation: 'vertical'
                    size_hint_y: None
                    height: self.minimum_height
                    spacing: dp(10)
                    pos_hint: {'center_x': .5, 'center_y': .5}
                    id:tabsb

        MDBottomNavigationItem:
            name: 'files3'
            text: '全民英檢中級單字'
            icon: 'language-javascript'

            ScrollView:
                BoxLayout:
                    orientation: 'vertical'
                    size_hint_y: None
                    height: self.minimum_height
                    spacing: dp(10)
                    pos_hint: {'center_x': .5, 'center_y': .5}
                    id:tabsc

        MDBottomNavigationItem:
            name: 'files4'
            text: '全民英檢中高級單字'
            icon: 'language-javascript'
            ScrollView:
                BoxLayout:
                    orientation: 'vertical'
                    size_hint_y: None
                    height: self.minimum_height
                    spacing: dp(10)
                    pos_hint: {'center_x': .5, 'center_y': .5}
                    id:tabsd          
'''
wordtest = '''

<Wordtest@Screen>
    on_enter:app.creattestlay(main)
    name: 'Wordtest'
    BoxLayout:
        canvas:
            Rectangle:
                id:bgimg
                size: self.size
                pos: self.pos
                source: './assets/wordtest.png'
        id: main
        padding:[50,50,80,self.height/4]
        orientation: 'vertical'
'''

sentencetest = '''

<Sentencetest@Screen>
    on_enter:app.creattestlay(main)
    name: 'Sentencetest'
    BoxLayout:
        canvas:
            Rectangle:
                id:bgimg
                size: self.size
                pos: self.pos
                source: './assets/Sentencetest.jpg'
        id: main
        padding:[20,20,20,20]
        do_scroll_x: False
        orientation: 'vertical'
'''

singlewordtest = '''

<SpellWordtest@Screen>
    on_enter:app.creattestlay(main)
    name: 'SpellWordtest'
    BoxLayout:
        canvas:
            Rectangle:
                id:bgimg
                size: self.size
                pos: self.pos
                source: './assets/singletest.png'
        id: main
        padding:[50,50,80,self.height/4]
        do_scroll_x: False
        orientation: 'vertical'
'''

lisitingtest = '''

<Lisitingtest@Screen>
    on_enter:app.creattestlay(main)
    name: 'Lisitingtest'
    BoxLayout:
        canvas:
            Rectangle:
                id:bgimg
                size: self.size
                pos: self.pos
                source: './assets/listening.png'
        id: main
        padding:[50,50,80,self.height/4]
        do_scroll_x: False
        orientation: 'vertical'
'''
showpicture = '''
<Showpicture@Screen>
    on_enter:app.creattestlay(main)
    name: 'Showpicture'
    BoxLayout:
        canvas:
            Rectangle:
                id:bgimg
                size: self.size
                pos: self.pos
                source: './assets/Showpictureimg.jpg'
        id: main
        padding:[20,20,20,20]
        do_scroll_x: False
        orientation: 'vertical'
'''
# 重組
rearrangement = ''' 
<Rearrangement@Screen>
    on_enter:app.creattestlay(main)
    name: 'Rearrangement'
    BoxLayout:
        canvas:
            Rectangle:
                id:bgimg
                size: self.size
                pos: self.pos
                source: './assets/rearrangement.jpg'
        id: main
        padding:[20,20,20,20]
        do_scroll_x: False
        orientation: 'vertical'
'''

connecttest = ''' 
<Connecttest@Screen>
    on_enter:app.creattestlay(main)
    name: 'Connecttest'
    FloatLayout:
        canvas:
            Rectangle:
                id:bgimg
                size: self.size
                pos: self.pos
                source: './assets/rearrangement.jpg'
        id:main
        orientation: 'vertical'
        padding:[20,20,20,20]
'''

profile = '''
#:import MDRoundFlatButton kivymd.uix.button.MDRoundFlatButton
#:import MDTextFieldRect kivymd.uix.textfield.MDTextFieldRect
<Profile@Screen>
    name: 'Profiles'
    BoxLayout:
        spacing:dp(10)
        minimum_height:dp(10)
        padding:[self.width/4,70,self.width/4,70]     
        canvas:
            Rectangle:
                size: self.size
                pos: self.pos
                source: './assets/profilesetting.png'
        orientation: 'vertical'
       
        BoxLayout:
            MDLabel:
                text:'帳號'
                size_hint_x:0.3
                pos_hint: {'center_y': .5}
                theme_text_color: 'Custom'
                text_color: (1,1,1,1)
            MDTextField:
                theme_text_color :'Custom'
                id:account
                size_hint_x:0.7
                text:"王大明"
                pos_hint: {'center_y': .5}
                line_color_normal:(1,1,1,1)
                line_color_focus:(1,1,1,1)
                foreground_color:(1,1,1,1)
                
        BoxLayout:            
            MDLabel:
                size_hint_x:0.3
                text:'密碼'
                pos_hint: {'center_y': .5}
                theme_text_color: 'Custom'
                text_color: (1,1,1,1)            
            MDTextField:
                theme_text_color :'Custom'
                id:password
                size_hint_x:0.7
                password: True
                pos_hint: {'center_y': .5}
                line_color_normal:(1,1,1,1)
                line_color_focus:(1,1,1,1)     
                foreground_color:(1,1,1,1)
                
        BoxLayout:
            MDLabel: 
                size_hint_x:0.3
                text:'確認密碼'
                pos_hint: {'center_y': .5}
                theme_text_color: 'Custom'
                text_color: (1,1,1,1)  
            MDTextField:
                theme_text_color :'Custom'
                id:repassword
                size_hint_x:0.7
                password: True
                pos_hint: {'center_y': .5}
                line_color_normal:(1,1,1,1)
                line_color_focus:(1,1,1,1)  
                foreground_color:(1,1,1,1)
        BoxLayout:
            padding:[20,0,20,0]       
            pos_hint: {'center_x': .5}
            size: self.minimum_size
            size_hint: None, None
            MDFillRoundFlatButton:
                text: "儲存"
                pos_hint: {'center_x': .5}  
                on_release:
                    app.shangeprofilesetting(account,password,repassword)  
            MDRoundFlatButton:
                text: "取消"
                icon: "language-python"
                pos_hint: {'center_x': .5}      
                theme_text_color :'Custom'
                background_color: (1,1,1,1)
                text_color:(0,0,0,1)
                md_bg_color: (1,1,1,1)
           
'''


qustionAdmin = '''

#:import MDDropdownMenu kivymd.uix.menu.MDDropdownMenu
#:import MDRoundFlatIconButton kivymd.uix.button.MDRoundFlatIconButton
#:import MDTextField kivymd.uix.textfield.MDTextField

<EachItem@BoxLayout>:
    value: []
    
    GridLayout:
        rows:1
        cols:5
        BoxLayout:
            MDLabel:
                id: gps_class_number
                font_size:14
                text_size: None, None
                size_hint: 1,1
                text: str(" ".join(root.value[0:1]))
                mipmap: True
            MDLabel:
                id: gps_class_etype
                markup: True
                font_size:14
                text_size: None, None
                size_hint: 1,1
                text: str(" ".join(root.value[1:2]))
                mipmap: True
            MDLabel:
                id: marketCap
                markup: True
                font_size:14
                text_size: None, None
                size_hint:(1, 1)
                text: str(" ".join(root.value[2:3]))
                mipmap: True
            MDLabel:
                id: change
                font_size:14
                text_size: None, None
                size_hint: 1,1
                text: str(" ".join(root.value[3:4]))
                mipmap: True
            MDLabel:
                id: change
                font_size:14
                text_size: None, None
                size_hint: 1,1
                text: str(" ".join(root.value[4:5]))
                mipmap: True
            MDLabel:
                id: change
                font_size:14
                text_size: None, None
                size_hint: 1,1
                text: str(" ".join(root.value[5:6]))
                mipmap: True           
            MDRaisedButton:
                text: "編輯"
                on_release:
                    app.on_call_NewGpswordPopup(num=gps_class_number.text,type=gps_class_etype.text)


<QustionAdmin@Screen>
    name: 'QustionAdmin'
    on_enter: app.loadgpsword()
    BoxLayout:
        orientation: 'vertical'
        spacing: dp(10)
        padding: dp(20)

        BoxLayout:
            spacing:dp(20)
            size_hint_y: None
            height: self.minimum_height
            MDRaisedButton:

                theme_text_color: 'Primary'
                text: "新增單字"
                halign: 'center'
                on_release:app.on_call_NewGpswordPopup()
  
            MDRaisedButton:
                theme_text_color: 'Primary'
                text: "篩選單字類型"
                halign: 'center'
                on_release:
                    MDDropdownMenu(items=app.gpswordadmin, width_mult=3).open(self)
                
            MDTextField:
                hint_text: "輸入英文單字"
                pos_hint: {'center_y': .3}

                id:search
            MDRaisedButton:
                theme_text_color: 'Primary'
                text: "搜尋單字"
                halign: 'center'
                on_release:app.loadgpsword(wordsearch=search.text)
            MDRaisedButton:
                theme_text_color: 'Primary'
                text: "清除搜尋結果"
                halign: 'center'
                on_release:app.loadgpsword()

        BoxLayout:
            height: "50"
            size_hint_y: None
            GridLayout:
                padding: 10, 0, 10, 0
                canvas:
                    Color:
                        rgba: 1, 1, 1, .1
                    Rectangle:
                        size: self.size
                rows:1
                cols:10
                MDLabel:
                    text: "編號"
                MDLabel:
                    text: "英文種類"
                                       
        RecycleView:
            id: rv
            bar_width: 8
            scroll_type: ['bars', 'content']
            scroll_wheel_distance: 120
            viewclass: 'EachItem' 

            RecycleBoxLayout:
                size_hint_y: None
                height: self.minimum_height
                default_size: None, 50
                default_size_hint: 1, None
                orientation: 'vertical'
                spacing: 3                

'''

wordtesthistroy = '''
#:import MDList kivymd.uix.list.MDList
#:import OneLineListItem kivymd.uix.list.OneLineListItem
#:import TwoLineListItem kivymd.uix.list.TwoLineListItem
#:import ThreeLineListItem kivymd.uix.list.ThreeLineListItem
#:import OneLineAvatarListItem kivymd.uix.list.OneLineAvatarListItem
#:import OneLineIconListItem kivymd.uix.list.OneLineIconListItem
#:import OneLineAvatarIconListItem kivymd.uix.list.OneLineAvatarIconListItem
<wordtesthistroylist>:
    orientation: 'vertical'
    padding: dp(10)
    spacing: dp(10)
    size_hint_y: None
    height: dp(130)
    ThreeLineIconListItem:
        on_release:app.show_viewhistroycontect(root.index,root.trange)
        height:dp(130)
        text:
            "序號:[color=%s]%d \\n [/color]" \
              % (get_hex_from_color(app.theme_cls.primary_color),root.index)
        secondary_text:            
            "作答範圍:[color=%s]%s[/color]\\n作答類型:[color=%s]%s[/color]\\n成績:[color=%s]%d[/color]\\n作答時間:[color=%s]%s[/color]" \
              % (get_hex_from_color(app.theme_cls.primary_color),root.trange,get_hex_from_color(app.theme_cls.primary_color),root.ttype,get_hex_from_color(app.theme_cls.primary_color),root.score,get_hex_from_color(app.theme_cls.primary_color),root.date)
        IconLeftSampleWidget:
            id: li_icon_3
            icon: 'check' 

<Wordtesthistroy@Screen>
    on_enter:app.loadhistroy(rv)
    name: 'Wordtesthistroy'
    BoxLayout:
        orientation: 'vertical'
        spacing: dp(10)
        padding: dp(20)

        BoxLayout:
            size_hint_y: None
            height: self.minimum_height

            MDLabel:
                icon: 'magnify'
                text:"總積分"

        RecycleView:
            ScrollView:
                GridLayout:
                    cols:1                
                    id:rv
                    height: self.minimum_height
                    size_hint_y: None
'''


#瀏覽錯誤紀錄
viewhistroycontect = '''
#:import MDDropdownMenu kivymd.uix.menu.MDDropdownMenu

<content@BoxLayout>:
    value: []
    GridLayout:
        rows:1
        cols:3
        BoxLayout:
            
            MDLabel:                
                markup: True
                font_size:14
                text_size: None, None
                size_hint: 1,1
                halign: 'center'
                text: str(" ".join(root.value[0:1]))
                mipmap: True
            MDLabel:
                id:word
                markup: True
                font_size:14
                text_size: None, None
                size_hint: 1,1
                halign: 'center'
                text: str(" ".join(root.value[1:2]))
                mipmap: True
            MDLabel:
                markup: True
                font_size:14
                text_size: None, None
                size_hint: 1,1
                halign: 'center'
                text: str(" ".join(root.value[2:3]))
                mipmap: True

            MDRaisedButton:
                id:openbtn
                text: "開啟單字卡"
                on_release:         
                    app.show_user_example_animation_card(word.text,gpsid=root.value[3:4],gpstype=root.value[4:5])

<Viewhistroycontect@Screen>
    name: 'Viewhistroycontect'
    on_enter: 
        app.loadanswercontent(self.id)
    BoxLayout:
        orientation: 'vertical'
        spacing: dp(10)
        padding: dp(20)

        BoxLayout:
            height: "50"
            size_hint_y: None
            GridLayout:
                padding: 10, 0, 10, 0
                canvas:
                    Color:
                        rgba: 1, 1, 1, .1
                    Rectangle:
                        size: self.size
                rows:1
                cols:3
           
                MDLabel:
                    text: "序號"
                    halign: 'center'
                MDLabel:
                    text: "單字"
                    halign: 'center'
                MDLabel:
                    text: "是否錯誤"
                    halign: 'center'

        RecycleView:
            id: rv
            bar_width: 8
            scroll_type: ['bars', 'content']
            scroll_wheel_distance: 120
            viewclass: 'content' 

            RecycleBoxLayout:
                id:test
                size_hint_y: None
                height: self.minimum_height
                default_size: None, 50
                default_size_hint: 1, None
                orientation: 'vertical'
                spacing: 3
'''

oftenwrongword = '''
#:import MDBottomNavigation kivymd.uix.bottomnavigation.MDBottomNavigation
#:import MDBottomNavigationItem kivymd.uix.bottomnavigation.MDBottomNavigationItem
#:import MDTextField kivymd.uix.textfield.MDTextField
#:import MDLabel kivymd.uix.label.MDLabel

<OftenWrongCard>:
    orientation: 'vertical'
    padding: dp(10)
    spacing: dp(10)
    size_hint_y: None
    height: self.minimum_height

    BoxLayout:
        size_hint_y: None
        height: self.minimum_height

        Widget:
        MDRoundFlatButton:
            text: "發音"
            on_press: 
                mythread = threading.Thread(target=app.listening,args=(root.id,))
                mythread.start()   
        Widget:
       

    TwoLineIconListItem:
        text: "中文"
        on_press: root.callback(self.text)
        secondary_text:
            "[color=%s]%s[/color]" % (get_hex_from_color(app.theme_cls.primary_color),root.chinese)
        IconLeftSampleWidget:
            icon: 'phone'

    TwoLineIconListItem:
        text: "詞性"
        on_press: root.callback(self.text)
        secondary_text:
            "[color=%s]%s[/color]" % (get_hex_from_color(app.theme_cls.primary_color),root.part)
        IconLeftSampleWidget:
            icon: 'phone'

    ThreeLineIconListItem:
        text: "範例"
        secondary_text:
            "[color=%s]%s\\n%s[/color]" % (get_hex_from_color(app.theme_cls.primary_color),root.example,root.examplec)
        IconLeftSampleWidget:
            id: li_icon_3
            icon: 'sd'

<Oftenwrongword@Screen>
    on_enter:app.loadoftenword(anim_list,"VocabularyLA")
    
    name: 'Oftenwrongword'
    BoxLayout:
        MDBottomNavigation:

            MDBottomNavigationItem:
                name: 'movies'
                text: '小學英文單字'
                icon: "movie"
                ScrollView:
                    GridLayout:
                        id: anim_list
                        cols: 1
                        size_hint_y: None
                        height: self.minimum_height
                                              
    
            MDBottomNavigationItem:
                on_enter:app.loadoftenword(anim_list1,"VocabularyGEPTLA")
                name: 'files'
                text: "全民英檢初級單字"
                icon: "file"

                ScrollView:
                    GridLayout:
                        id: anim_list1
                        cols: 1
                        size_hint_y: None
                        height: self.minimum_height

            MDBottomNavigationItem:
                name: 'android'
                text: "全民英檢中級單字"
                icon: "android"
                on_enter:app.loadoftenword(anim_list2,"VocabularyGEPTLB")

                ScrollView:
                    GridLayout:
                        id: anim_list2
                        cols: 1
                        size_hint_y: None
                        height: self.minimum_height

            MDBottomNavigationItem:
                name: 'python-language'
                text: "全民英檢中高級單字"
                icon: 'language-python'
                on_enter:app.loadoftenword(anim_list3,"VocabularyGEPTLC")
                GridLayout:
                    id: anim_list3
                    cols: 1
                    size_hint_y: None
                    height: self.minimum_height
                
'''

gpstest = '''
#:import MDToolbar kivymd.uix.toolbar.MDToolbar
#:import MDLabel kivymd.uix.label.MDLabel
#:import ThreeLineListItem kivymd.uix.list.ThreeLineListItem
#:import MDTextField kivymd.uix.textfield.MDTextField
#:import MDTextFieldClear kivymd.uix.textfield.MDTextFieldClear
#:import MDFillRoundFlatButton kivymd.uix.button.MDFillRoundFlatButton
<gpswordlist>:
    size_hint_y: None
    height:self.minimum_height
    OneLineIconListItem:
        on_release:app.show_user_example_animation_card(root.englishword,gpsid=root.id,gpstype=root.gpswordtype)
        text:root.englishword
        IconLeftSampleWidget:
            id: li_icon_1
            icon: 'wordpress'

<GpsTab>:
    ScrollView:
        BoxLayout:
            height: self.minimum_height
            orientation: 'vertical'
            size_hint_y: None
            id:tabdata
                          
<Gpstest@Screen>
    name: 'Gpstest'
    MDTabs:
        id:android_tabs
   
'''

wordsearchpage='''
#:import OneLineListItem kivymd.uix.list.OneLineListItem
<wordlistcontent@BoxLayout>:
    value: []
    GridLayout:
        id:wordcontent
        rows:1
        cols:1
        OneLineListItem:                  
            text: str(" ".join(root.value[0:1]))
            on_release:app.show_user_example_animation_card(self.text,relationword=True)
           
<WordSearchPage@Screen>
    name: 'WordSearchPage'
    BoxLayout:
        orientation: 'vertical'
        spacing: dp(10)
        padding: dp(20)

        BoxLayout:
            size_hint_y: None
            height: self.minimum_height

            MDIconButton:
                icon: 'magnify'

            MDTextField:
                id: search_field
                hint_text: '單字搜尋'            
            MDIconButton:
                icon: 'file-search'        
                md_bg_color: app.theme_cls.primary_color        
                on_release:
                    print(search_field.text)
                    app.addword(search_field.text)

        RecycleView:
            id: rv
            bar_width: 8
            scroll_type: ['bars', 'content']
            scroll_wheel_distance: 120
            viewclass: 'wordlistcontent' 

            RecycleBoxLayout:
                id:test
                size_hint_y: None
                height: self.minimum_height
                default_size: None, 50
                default_size_hint: 1, None
                orientation: 'vertical'
                spacing: 3
      
'''

concept='''
#:import requests requests
#:import AsyncImage kivy.uix.image.AsyncImage
#:import time time
<ConceptMap@Screen>
    on_enter:
        print(app.matplotliburldata)
        r = requests.post('http://120.96.63.79/oit_English/getmatplotlib', data = app.matplotliburldata)
        time.sleep(5)
        app.changeurl()
        test124.clear_widgets()
        test124.add_widget(AsyncImage(source=app.url))
        print(app.url)
    name: 'ConceptMap'
    BoxLayout:
        orientation: 'vertical'
        spacing: dp(10)
        padding: dp(20)
        GridLayout:
            rows: 2
            BoxLayout:
                Zoom:
                    id:test124
            

'''

festivalword='''
#:import requests requests
#:import AsyncImage kivy.uix.image.AsyncImage
#:import Screens screens
#:import main main

<MyCard>:
    
    orientation: 'vertical'
    
    size_hint_y: None
    height: dp(350)
    pos_hint: {'top': 1}

    ImageButton:
        id:imgurl
        source:root.path
        size_hint_y: None
        size:root.width, dp(150)

    MDLabel:
        theme_text_color: 'Custom'
        bold: True
        text_color: app.theme_cls.primary_color
        text: root.text
        size_hint_y: None
        font_style: 'H5'
        halign: 'center'
   
    OneLineIconListItem:
        text:  "中文：[color=%s]%s[/color]" \
            % (get_hex_from_color(app.theme_cls.primary_color),root.chinese)        
        IconLeftSampleWidget:
            icon: 'wordpress'

    OneLineIconListItem:
        text:  "詞性：[color=%s]%s[/color]" \
            % (get_hex_from_color(app.theme_cls.primary_color),root.part)           
        IconLeftSampleWidget:
            icon: 'ab-testing'
    
<FestivalScreenOne@Screen>:
    name: 'screen one'
    MyCard:
        text:Screens.Screens.Festivaldata[0][1]
        path:Screens.Screens.Festivaldata[0][0]
        chinese:Screens.Screens.Festivaldata[0][2]
        part:Screens.Screens.Festivaldata[0][3]
       

<FestivalScreenTwo@Screen>:
    name: 'screen two'
    MyCard:
        text:Screens.Screens.Festivaldata[1][1]
        path:Screens.Screens.Festivaldata[1][0]
        chinese:Screens.Screens.Festivaldata[1][2]
        part:Screens.Screens.Festivaldata[1][3]
       
<FestivalScreenThree@Screen>:
    name: 'screen three'
    MyCard:
        text:Screens.Screens.Festivaldata[2][1]
        path:Screens.Screens.Festivaldata[2][0]
        chinese:Screens.Screens.Festivaldata[2][2]
        part:Screens.Screens.Festivaldata[2][3]
        
<FestivalScreenFour@Screen>:
    name: 'screen four'
    MyCard:
        text:Screens.Screens.Festivaldata[3][1]
        path:Screens.Screens.Festivaldata[3][0]
        chinese:Screens.Screens.Festivaldata[3][2]
        part:Screens.Screens.Festivaldata[3][3]
       
<FestivalScreenFive@Screen>:
    name: 'screen five'
    MyCard:
        text:Screens.Screens.Festivaldata[4][1]
        path:Screens.Screens.Festivaldata[4][0]
        chinese:Screens.Screens.Festivaldata[4][2]
        part:Screens.Screens.Festivaldata[4][3]
      
<Festivalword@Screen>
    name: 'Festivalword'
    BoxLayout:
        id:test123
        orientation: 'vertical'
        canvas:
            Color:
                rgba: 0, 0, 0, .2
            Rectangle:
                pos: self.pos
                size: self.size
        BoxLayout:            
            orientation: 'vertical'
            BoxLayout:
                padding: dp(30)
                size_hint: None, None
                size: self.minimum_size
                spacing: dp(10)
                pos_hint: {'center_x': .5}
                Label:
                    id:festword
                    markup: True
                    bold: True
                    font_size:root.width/20
                    
            MDSwiperManager:
                id: swiper_manager

                FestivalScreenOne:

                FestivalScreenTwo:

                FestivalScreenThree:

                FestivalScreenFour:

                FestivalScreenFive:
        BoxLayout:               
            padding: dp(10)
            size_hint: None, None
            size: self.minimum_size
            spacing: dp(10)
            pos_hint: {'center_x': .5}
            MDRaisedButton:
                text: "發音"
                on_release:Screens.listening(swiper_manager.current)
                size_hint:None,None
            MDRaisedButton:
                text: "練習發音"
                on_press: 
                    Screens.speaktest(swiper_manager.current)
'''

weatherword='''
#:import requests requests
#:import AsyncImage kivy.uix.image.AsyncImage
#:import Screens screens
#:import main main

<MyCard>:
    orientation: 'vertical'
    size_hint_y: None
    height: dp(350)
    pos_hint: {'top': 1}

    ImageButton:
        id:imgurl
        source:root.path
        size_hint_y: None
        size:root.width, dp(150)

    MDLabel:
        theme_text_color: 'Custom'
        bold: True
        text_color: app.theme_cls.primary_color
        text: root.text
        size_hint_y: None
        font_style: 'H5'
        halign: 'center'
   
    OneLineIconListItem:
        text:  "中文：[color=%s]%s[/color]" \
            % (get_hex_from_color(app.theme_cls.primary_color),root.chinese)        
        IconLeftSampleWidget:
            icon: 'wordpress'

    OneLineIconListItem:
        text:  "詞性：[color=%s]%s[/color]" \
            % (get_hex_from_color(app.theme_cls.primary_color),root.part)           
        IconLeftSampleWidget:
            icon: 'ab-testing'
    
<WeatherScreenOne@Screen>:
    name: 'screen one'
    MyCard:
        text:Screens.Screens.Weatherdata[0][1]
        path:Screens.Screens.Weatherdata[0][0]
        chinese:Screens.Screens.Weatherdata[0][2]
        part:Screens.Screens.Weatherdata[0][3]
       

<WeatherScreenTwo@Screen>:
    name: 'screen two'
    MyCard:
        text:Screens.Screens.Weatherdata[1][1]
        path:Screens.Screens.Weatherdata[1][0]
        chinese:Screens.Screens.Weatherdata[1][2]
        part:Screens.Screens.Weatherdata[1][3]
       
<WeatherScreenThree@Screen>:
    name: 'screen three'
    MyCard:
        text:Screens.Screens.Weatherdata[2][1]
        path:Screens.Screens.Weatherdata[2][0]
        chinese:Screens.Screens.Weatherdata[2][2]
        part:Screens.Screens.Weatherdata[2][3]
        
<WeatherScreenFour@Screen>:
    name: 'screen four'
    MyCard:
        text:Screens.Screens.Weatherdata[3][1]
        path:Screens.Screens.Weatherdata[3][0]
        chinese:Screens.Screens.Weatherdata[3][2]
        part:Screens.Screens.Weatherdata[3][3]
       
<WeatherScreenFive@Screen>:
    name: 'screen five'
    MyCard:
        text:Screens.Screens.Weatherdata[4][1]
        path:Screens.Screens.Weatherdata[4][0]
        chinese:Screens.Screens.Weatherdata[4][2]
        part:Screens.Screens.Weatherdata[4][3]
      
<Weatherword@Screen>
    name: 'Weatherword'
    BoxLayout:
        id:test123
        orientation: 'vertical'
        canvas:
            Color:
                rgba: 0, 0, 0, .2
            Rectangle:
                pos: self.pos
                size: self.size
        BoxLayout:            
            orientation: 'vertical'
            BoxLayout:
                orientation: 'vertical'
                padding: dp(30)
                size_hint: None, None
                size: self.minimum_size
                spacing: dp(10)
                pos_hint: {'center_x': .5}
                Label:
                    id:LocationLabel
                    markup: True
                    bold: True
                    font_size:root.width/20
                    pos_hint: {'center_x': .5}
                
            MDSwiperManager:
                id: swiper_manager

                WeatherScreenOne:

                WeatherScreenTwo:

                WeatherScreenThree:

                WeatherScreenFour:

                WeatherScreenFive:
        BoxLayout:               
            padding: dp(10)
            size_hint: None, None
            size: self.minimum_size
            spacing: dp(10)
            pos_hint: {'center_x': .5}
            MDRaisedButton:
                text: "發音"
                on_release:Screens.listening(swiper_manager.current)
                size_hint:None,None
            MDRaisedButton:
                text: "練習發音"
                on_press: 
                    Screens.speaktest(swiper_manager.current)  
'''

def listening(word):
        mythread = threading.Thread(target=speak,args=(word,))
        mythread.start()             
                    
def speak(word):     #發音
       
    from screens import Screens
    playcount=0
    if word =="screen one":
        try:
            word = Screens.Weatherdata[0][1]
        except:
            word = Screens.Festivaldata[0][1]
    elif word == "screen two":
        try:
            word = Screens.Weatherdata[1][1]
        except:
            word = Screens.Festivaldata[1][1]
    elif word == "screen three":
        try:
            word = Screens.Weatherdata[2][1]
        except:
            word = Screens.Festivaldata[2][1]
    elif word == "screen four":
        try:
            word = Screens.Weatherdata[3][1]
        except:
            word = Screens.Festivaldata[3][1]
    elif word == "screen five":
        try:
            word = Screens.Weatherdata[4][1]
        except:
            word = Screens.Festivaldata[4][1]
    from gtts import gTTS             
    if core_platform == "android":
        tts = gTTS(text = word, lang='en')
        tts.save('test.mp3')
        from jnius import autoclass
        from time import sleep
        # get the MediaPlayer java class
        MediaPlayer = autoclass('android.media.MediaPlayer')

        # create our player
        mPlayer = MediaPlayer()
        mPlayer.setDataSource('test.mp3')
        mPlayer.prepare()
    
        # play
        
        mPlayer.start()
        
        sleep(mPlayer.getDuration()/1000)
    else:
        from pygame import mixer
        try:
            playcount += 1
            print(playcount)
            tts = gTTS(text=word, lang='en')
            tts.save(f'speech{playcount%2}.mp3')
            mixer.init()
            mixer.music.load(f'speech{playcount%2}.mp3')
            mixer.music.play()
        except:
            playcount += 1
            from pygame import mixer
            tts.save(f'speech{playcount%2}.mp3')
            mixer.init()
            mixer.music.load(f'speech{playcount%2}.mp3')
            mixer.music.play()
        
def speaktest(word):
    if word =="screen one":
        word = Screens.Festivaldata[0][1]
    elif word == "screen two":
        word =Screens.Festivaldata[1][1]
    elif word == "screen three":
        word =Screens.Festivaldata[2][1] 
    elif word == "screen four":
        word =Screens.Festivaldata[3][1] 
    elif word == "screen five":
        word =Screens.Festivaldata[4][1] 
    poti = speaktestpopup(word=word,background = 'atlas://data/images/defaulttheme/vkeyboard_key_down')
    poti.open()

class MyCard(BoxLayout):
    
    text = StringProperty('')
    path = StringProperty('')
    chinese = StringProperty('')
    example=StringProperty('')
    cexample=StringProperty('')
    part = StringProperty('')
    def __init__(self, **kwargs):        
        super(MyCard, self).__init__(**kwargs)
        

class speaktestpopup(Popup): 
    title = StringProperty()
    word = StringProperty()
    def __init__(self,word, **kwargs):
        super(speaktestpopup, self).__init__(**kwargs)
        self.word = word
    def start_listening(self):
        if stt.listening:
            self.stop_listening()
            return

        start_button = self.ids.start_button
        start_button.text = '停止'

        self.ids.results.text = ''
        self.ids.partial.text = ''

        stt.start()

        Clock.schedule_interval(self.check_state, 1 / 5)
    
    def stop_listening(self):
        start_button = self.ids.start_button
        start_button.text = '開始聆聽'

        stt.stop()
        self.update()

        Clock.unschedule(self.check_state)

    def check_state(self, dt):
        # if the recognizer service stops, change UI
        if not stt.listening:
            self.stop_listening()
    
    def update(self):
        self.ids.partial.text = '\n'.join(stt.partial_results)
        self.ids.results.text = '\n'.join(stt.results)        

    
class FestivalMyThread(threading.Thread):
    def __init__(self, queue, index, word,screenname):
        super().__init__()
        self.screenname = screenname
        self.queue = queue
        self.index = index
        self.word = word
        self.temp=[]
        self.data=[]
        self.chinese=""
    def run(self):
        from screens import Screens
        while not self.queue.empty():
            self.temp=[]
            url = self.queue.get()
            #print(url)
            r = requests.get(
                url, headers={
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'
                })
            soup = BeautifulSoup(r.text, 'html.parser')
            b_tag = soup.find("img", class_="z_g_j z_g_a z_g_b")  
            self.temp.append(b_tag.get('src'))
            self.temp.append(self.word)
            
            try:
                translator = Translator()
                chinese = translator.translate(self.word, dest='zh-tw').text  # 中文
                self.temp.append(chinese)
            except:
                self.temp.append("無資料")
            r = requests.get('https://www.wordsapi.com/mashape/words/'+self.word +
                            '?when=2019-09-02T05:36:35.449Z&encrypted=8cfdb283e7229a9bea9307bfea58bdbfaeb3280934fb9eb8')
            data = r.json()
            try:
                part = translator.translate(
                    data['results'][1]['partOfSpeech'], dest='zh-tw').text
                self.temp.append(part)
            except:
                part = "無資料"
                self.temp.append(part)
            r = requests.get('https://www.wordsapi.com/mashape/words/'+self.word +
                            '/examples?when=2019-09-02T03:53:09.529Z&encrypted=8cfdb283e7229a9bea9307bfec58bbbaaeb0240935fd9eb8')
            data = r.json()
            try:
                example = data['examples'][0]  # 範例
                cexample = translator.translate(
                    data['examples'][0], dest='zh-tw').text  # 範例中文
                self.temp.append(example)
                self.temp.append(cexample)
            except:
                example = "無資料"
                cexample = "無資料"
                self.temp.append(example)
                self.temp.append(cexample)
            if self.screenname =="Festivalword":
                Screens.Festivaldata.append(self.temp)
            elif self.screenname =="Weatherword":
                Screens.Weatherdata.append(self.temp)
class IconLeftSampleWidget(ILeftBodyTouch, MDIconButton):
    pass
class Screens(object):

    Weatherdata=[]
    weatherword=None
    def show_WeatherPage(self): 
        if not self.weatherword:
            from kivymd.uix.dialog import MDDialog
            self.ok_cancel_dialog = MDDialog(
                title='定位要求', size_hint=(.8, .4), text_button_ok='確定',
                text="本功能需要GPS來根據您的地點產生相關單字，需要花費數秒，確定繼續", text_button_cancel='取消',
                events_callback=self.askforgpsweather)
            self.ok_cancel_dialog.open()
        else:
            self.main_widget.ids.scr_mngr.current = 'Weatherword'
    def askforgpsweather(self,*args):
        if (args[0]) == "確定":
            self.loginpop = loadingscreen('註冊',background = '[372, 326, 64, 64]',size=(Window.width/3,Window.height/8))
            self.loginpop.open()   
            self.gpsA=0
            self.gpsB=0       
            try:
                gps.configure(on_location=self.on_location,
                            on_status=self.on_status)                                                                         
            except NotImplementedError:
                import traceback
                traceback.print_exc()
            finally:    
                mythread = threading.Thread(target=self.loadweathergps)
                mythread.start()

    def loadweathergps(self):       #使用threading載入資料
        x=0
        while True:
            time.sleep(1)
            x+=1
            print(x)
            if x==1 : 
                break
        try:
            self.start(1000, 0)
            while True:
                if self.gpsA != 0:
                    gps.stop()
                    break
        except NotImplementedError:
            import traceback
            traceback.print_exc()
            toast('本裝置不支援GPS功能，使用先前定位座標')        
            self.gpsA=25.0200986
            self.gpsB=121.537726
        translator = Translator()
        
        url = 'https://www.openstreetmap.org/geocoder/search_osm_nominatim_reverse?lat='+str(self.gpsA)+'&latlon_digits=true&lon='+str(self.gpsB) + \
            '&zoom=11&minlon=121.43875122070314&minlat=24.807304640022597&maxlon=121.73538208007814&maxlat=25.131609209315805'
        r = requests.get(url)
        soup = BeautifulSoup(r.text, 'html.parser')
        a_tags = soup.find('a')
        data = a_tags.get('data-name')

        countryindex = 0
        index = 0
        for i in data.split(","):
            if i[-1] == "縣" or i[-1] == "市":
                countryindex = index
            index += 1
        self.location = str(data.split(",")[countryindex].split()[0])
       
        r = requests.get("https://openweathermap.org/data/2.5/find?q="+translator.translate(data.split(",")[countryindex].split()[
                         0][:-1], dest='en').text+"&type=like&sort=population&cnt=30&appid=b6907d289e10d714a6e88b30761fae22&_=1574754053838")
        data = r.json()
        r = requests.get('https://relatedwords.org/api/related?term=' +
                         data['list'][0]['weather'][0]['main'])
        data = r.json()
        self.relationwordlist = []
        for i in data:
            if " " not in i['word']:
                self.relationwordlist.append(i['word'])
            if len(self.relationwordlist) >= 5:
                break
        r = requests.get("https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-C0032-001?Authorization=CWB-6131D6DF-935F-4A74-9923-99B88AE043DA&elementName=Wx&limit=1&locationName="+self.location)
        data = r.json()
        self.weatherchinese=data['records']['location'][0]['weatherElement'][0]['time'][0]['parameter']['parameterName']
        
        
        start = time.time()
        queue = Queue()
        for i in self.relationwordlist:
            queue.put('https://www.shutterstock.com/zh-Hant/search/'+i)
        thread1 = FestivalMyThread(queue, 0, self.relationwordlist[0],"Weatherword")
        thread2 = FestivalMyThread(queue, 1, self.relationwordlist[1],"Weatherword")
        thread3 = FestivalMyThread(queue, 2, self.relationwordlist[2],"Weatherword")
        thread4 = FestivalMyThread(queue, 3, self.relationwordlist[3],"Weatherword")
        thread5 = FestivalMyThread(queue, 4, self.relationwordlist[4],"Weatherword")

        thread1.start()
        thread2.start()
        thread3.start()
        thread4.start()
        thread5.start()

        thread1.join()
        thread2.join()
        thread3.join()
        thread4.join()
        thread5.join()

        end = time.time()
        
        Clock.schedule_once(self.add_weatherwidget,0)
        
    def add_weatherwidget(self,sec):
        Builder.load_string(weatherword)
        self.weatherword = Factory.Weatherword()
        self.main_widget.ids.scr_mngr.add_widget(self.weatherword)
        self.main_widget.ids.scr_mngr.current = 'Weatherword'
       
        self.main_widget.ids.scr_mngr.get_screen('Weatherword').ids.LocationLabel.text = "地址:"+self.location+"    "+"天氣:"+self.weatherchinese
        self.swiper_manager  = self.main_widget.ids.scr_mngr.get_screen('Weatherword').ids.swiper_manager
        paginator = MDSwiperPagination()
        paginator.screens = self.swiper_manager.screen_names
        paginator.manager = self.swiper_manager
        self.swiper_manager.paginator = paginator
        self.main_widget.ids.scr_mngr.get_screen('Weatherword').ids.test123.add_widget(paginator)

        self.loginpop.dismiss()  

    def addword(self,word): #單字搜尋
        if word == "":
            return
        headers = {'User-Agent': 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'}
        r = requests.get('https://dictionary.cambridge.org/zht/autocomplete/amp?dataset=english-chinese-traditional&q='+word+'&__amp_source_origin=https%3A%2F%2Fdictionary.cambridge.org',headers=headers)
        data = r.json()
        wordlist=[]
        for i in data:
            wordlist.append(i['word'])
        self.main_widget.ids.scr_mngr.get_screen('WordSearchPage').ids.rv.data =  [{"value": [a,b]} for a,b in zip(wordlist,wordlist)]        
        
    home = None

    def show_homePage(self):
        self.main_widget.ids.scr_mngr.current = 'Home'

    wordsearchpage = None
    Festivaldata = []
    FestivalLabWord ="" #顯示節日名稱Label
    festivalword  = None
    def show_festivalwordPage(self):  
        if not self.festivalword:
            mythread = threading.Thread(target=self.loadingfestivaldata)
            mythread.start() 
        else:
            self.main_widget.ids.scr_mngr.current = 'Festivalword'
        

    def loadingfestivaldata(self):
        word =""
        if core_platform == "android":
            from jnius import autoclass
            DisplayMetrics = autoclass('android.util.DisplayMetrics')
            metrics = DisplayMetrics()
            metrics.setToDefaults()            
            self.loginpop = loadingscreen('註冊',background = '[372, 326, 64, 64]',size=(metrics.xdpi,Window.height/8))
        else:
            self.loginpop = loadingscreen('註冊',background = '[372, 326, 64, 64]',size=(Window.width/3,Window.height/8))
        self.loginpop.open()
        translator = Translator()

        conn = sqlite3.connect(os.path.join(os.path.dirname(os.path.abspath(__file__)), "database.db"))
        c = conn.cursor()        
        
        cursor = c.execute("SELECT *,abs(julianday(date('now','localtime'))-julianday(FestivalDate)) as diffdate from 'FestivalWord' where diffdate in (SELECT min(abs(julianday(date('now','localtime'))-julianday(FestivalDate))) as diffdate from 'FestivalWord' )")        
        for row in cursor:
            
            word = row[1]
            self.FestivalLabWord = row[2]

        festival = word
        r = requests.get('https://relatedwords.org/api/related?term='+festival)
        data = r.json()
        relationwordlist = []
        for i in data:
            if " " not in i['word']:
                relationwordlist.append(i['word'])
            if len(relationwordlist) >= 5:
                break
        conn.close()
        start = time.time()
        
        queue = Queue()
        for i in relationwordlist:
            queue.put('https://www.shutterstock.com/zh-Hant/search/'+i)
        thread1 = FestivalMyThread(queue, 0, relationwordlist[0],"Festivalword")
        thread2 = FestivalMyThread(queue, 1, relationwordlist[1],"Festivalword")
        thread3 = FestivalMyThread(queue, 2, relationwordlist[2],"Festivalword")
        thread4 = FestivalMyThread(queue, 3, relationwordlist[3],"Festivalword")
        thread5 = FestivalMyThread(queue, 4, relationwordlist[4],"Festivalword")

        thread1.start()
        thread2.start()
        thread3.start()
        thread4.start()
        thread5.start()

        thread1.join()
        thread2.join()
        thread3.join()
        thread4.join()
        thread5.join()

        end = time.time()

        Clock.schedule_once(self.add_festivalwidget,0)

    def add_festivalwidget(self,time):
        if not self.festivalword:
            Builder.load_string(festivalword)
            self.festivalword = Factory.Festivalword()
            self.main_widget.ids.scr_mngr.add_widget(self.festivalword)
        self.main_widget.ids.scr_mngr.current = 'Festivalword'
        
        self.main_widget.ids.scr_mngr.get_screen('Festivalword').ids.festword.text = self.FestivalLabWord
        self.swiper_manager  = self.main_widget.ids.scr_mngr.get_screen('Festivalword').ids.swiper_manager
        paginator = MDSwiperPagination()
        paginator.screens = self.swiper_manager.screen_names
        paginator.manager = self.swiper_manager
        self.swiper_manager.paginator = paginator
        self.main_widget.ids.scr_mngr.get_screen('Festivalword').ids.test123.add_widget(paginator)
        self.loginpop.dismiss()   

    concept  = None

    def show_CoceptMapPage(self):  
        if not self.concept:
            Builder.load_string(concept)
            self.concept = Factory.ConceptMap()
            self.main_widget.ids.scr_mngr.add_widget(self.concept)
        self.main_widget.ids.scr_mngr.current = 'ConceptMap'
    

    def show_WordSearchPage(self):  #單字搜尋
        if not self.wordsearchpage:
            Builder.load_string(wordsearchpage)
            self.wordsearchpage = Factory.WordSearchPage()
            self.main_widget.ids.scr_mngr.add_widget(self.wordsearchpage)
        wordlist=["輸入單字"]
        self.main_widget.ids.scr_mngr.get_screen('WordSearchPage').ids.rv.data = [{"value": [a,b]} for a,b in zip(wordlist,wordlist)]        
        self.main_widget.ids.scr_mngr.current = 'WordSearchPage'
    
    menu = None

    def show_MainPage(self):
        if not self.menu:
            Builder.load_string(menu)
            self.menu = Factory.Menu()
            self.main_widget.ids.scr_mngr.add_widget(self.menu)
        self.main_widget.ids.scr_mngr.current = 'Menu'

    vocabluaryGEPTLA = None

    def show_vocabluaryGEPTLA(self, app, text): #小學英文單字
        from Vocabularyword import VocabluaryGEPTLA

        if not self.vocabluaryGEPTLA:
            Builder.load_string(vocabluaryGEPTLA)
            self.vocabluaryGEPTLA = Factory.VocabluaryGEPTLA()
            self.main_widget.ids.scr_mngr.add_widget(self.vocabluaryGEPTLA)
            app.set_list_md_icons(
                screen="vocabluaryGEPTLA")
        self.main_widget.ids.scr_mngr.current = 'vocabluaryGEPTLA'

    vocablaryLA = None

    def show_vocablaryLA(self, app, text): #小學英文單字
        from Vocabularyword import VocablaryLA
        if not self.vocablaryLA:
            Builder.load_string(vocablaryLA)
            self.vocablaryLA = Factory.VocablaryLA()
            self.main_widget.ids.scr_mngr.add_widget(self.vocablaryLA)
            app.set_list_md_icons(screen="vocablaryLA")
        self.main_widget.ids.scr_mngr.current = 'vocablaryLA'

    vocabluaryGEPTLB = None

    def show_vocabluaryGEPTLB(self, app, text):
        from Vocabularyword import VocabluaryGEPTLB

        if not self.vocabluaryGEPTLB:
            Builder.load_string(vocabluaryGEPTLB)
            self.vocabluaryGEPTLB = Factory.VocabluaryGEPTLB()
            self.main_widget.ids.scr_mngr.add_widget(self.vocabluaryGEPTLB)
            app.set_list_md_icons(
                screen="vocabluaryGEPTLB")
        self.main_widget.ids.scr_mngr.current = 'vocabluaryGEPTLB'

    vocabluaryGEPTLC = None

    def show_vocabluaryGEPTLC(self, app, text):
        from Vocabularyword import VocabluaryGEPTLC

        if not self.vocabluaryGEPTLC:
            Builder.load_string(vocabluaryGEPTLC)
            self.vocabluaryGEPTLC = Factory.VocabluaryGEPTLC()
            self.main_widget.ids.scr_mngr.add_widget(self.vocabluaryGEPTLC)
            app.set_list_md_icons(
                screen="vocabluaryGEPTLC")
        self.main_widget.ids.scr_mngr.current = 'vocabluaryGEPTLC'

    sentencetest = None

    def show_sentencetestPage(self):  # 拼字測驗
        if not self.sentencetest:
            Builder.load_string(sentencetest)
            self.sentencetest = Factory.Sentencetest()
            self.main_widget.ids.scr_mngr.add_widget(self.sentencetest)
        self.main_widget.ids.scr_mngr.current = 'Sentencetest'

    cards = None

    def show_cards(self):
        if not self.cards:
            Builder.load_string(cards)
            self.cards = Factory.Cards()
            self.main_widget.ids.scr_mngr.add_widget(self.cards)           
        self.main_widget.ids.scr_mngr.current = 'cards'

    wordtest = None
    
    def show_WordtestPage(self):  # 單字
        if not self.wordtest:
            Builder.load_string(wordtest)
            self.wordtest = Factory.Wordtest()
            self.main_widget.ids.scr_mngr.add_widget(self.wordtest)
        self.main_widget.ids.scr_mngr.current = 'Wordtest'

    singlewordtest = None

    def show_SpellWordtestPage(self):  # 拼字測驗
        if not self.singlewordtest:
            Builder.load_string(singlewordtest)
            self.singlewordtest = Factory.SpellWordtest()
            self.main_widget.ids.scr_mngr.add_widget(self.singlewordtest)
        self.main_widget.ids.scr_mngr.current = 'SpellWordtest'

    lisitingtest = None

    def show_LisitingtestPage(self):  # 聽音測驗
        if not self.lisitingtest:
            Builder.load_string(lisitingtest)
            self.lisitingtest = Factory.Lisitingtest()
            self.main_widget.ids.scr_mngr.add_widget(self.lisitingtest)
        self.main_widget.ids.scr_mngr.current = 'Lisitingtest'

    showpicture = None

    def show_PicturetestPage(self):  # 圖像測驗
        if not self.showpicture:
            Builder.load_string(showpicture)
            self.showpicture = Factory.Showpicture()
            self.main_widget.ids.scr_mngr.add_widget(self.showpicture)
        self.main_widget.ids.scr_mngr.current = 'Showpicture'

    profile = None

    def show_ProfilePage(self):  # 個人設定
        if not self.profile:
            Builder.load_string(profile)
            self.profile = Factory.Profile()
            self.main_widget.ids.scr_mngr.add_widget(self.profile)
        self.main_widget.ids.scr_mngr.current = 'Profiles'

    qustionAdmin = None

    def show_QusetionAdmPage(self):  # 個人設定
        if not self.qustionAdmin:
            Builder.load_string(qustionAdmin)
            self.qustionAdmin = Factory.QustionAdmin()
            self.main_widget.ids.scr_mngr.add_widget(self.qustionAdmin)
        self.main_widget.ids.scr_mngr.current = 'QustionAdmin'

    oftenwrongword = None

    def show_oftenwrongword(self):
        def callback(text):
            toast('{} to {}'.format(text, content.name_item))
        if not self.oftenwrongword:
            Builder.load_string(oftenwrongword)
            self.oftenwrongword = Factory.Oftenwrongword()
            self.main_widget.ids.scr_mngr.add_widget(self.oftenwrongword)
        self.main_widget.ids.scr_mngr.current = 'Oftenwrongword'

    gpstest = None
    def show_GpstestPage(self): #gps單字               
        if not self.gpstest:
            from kivymd.uix.dialog import MDDialog
            self.ok_cancel_dialog = MDDialog(
                title='定位要求', size_hint=(.8, .4), text_button_ok='確定',
                text="本功能需要GPS來根據您的地點產生相關單字，需要花費數秒，確定繼續", text_button_cancel='取消',
                events_callback=self.askforgps)
            self.ok_cancel_dialog.open()        
        else:
            self.main_widget.ids.scr_mngr.current = 'Gpstest'
        
    def askforgps(self,*args):
        
        if (args[0]) == "確定":
            self.loginpop = loadingscreen('註冊',background = '[372, 326, 64, 64]',size=(Window.width/3,Window.height/8))
            self.loginpop.open() 
            self.gpsA=0
            self.gpsB=0       
            try:
                gps.configure(on_location=self.on_location,
                            on_status=self.on_status)                                                                         
            except NotImplementedError:
                import traceback
                traceback.print_exc()
            finally:    
                mythread = threading.Thread(target=self.loadgps)
                mythread.start()

    def loadgps(self):       #使用threading載入資料
        x=0
        while True:
            time.sleep(1)
            x+=1
            print(x)
            if x==3 : 
                break                     
        try:
            self.start(1000, 0)
            while True:
                if self.gpsA != 0:
                    gps.stop()
                    break
     
        except NotImplementedError:
            import traceback
            traceback.print_exc()
            toast('本裝置不支援GPS功能，使用先前定位座標')        
            self.gpsA=25.0200986
            self.gpsB=121.537726
                
        google_key = "AIzaSyC3Zs_Aln2ewhniP1cvuCb5hajMZyYbJxE"
        gmaps = googlemaps.Client(key=google_key)
        # 資料庫的類別
        types_list =[['restaurant', '餐廳'], ['food', '小吃'], ['bakery', '麵包店'], ['clothing store', '服飾店'], ['school', '學校'],
                      ['park', '公園'], ['cafe', '咖啡店'], ['convenience store', '便利店'], ['spa', 'SPA'], ['shopping mall', '購物中心'],
                      ['movie theater', '電影院'], ['home goods store', '家居用品店'], ['stadium', '體育場'], ['parking', '停車場'],
                      ['lodging', '住宿'], ['real estate agency', '房地產仲介'], ['health', '醫院診所'], ['atm', '自動提款機'],
                      ['political', '政治設施'], ['bank', '銀行'], ['beauty salon', '美容院'], ['electronics store', '電子產品商店'],
                      ['bar', '酒吧'], ['hair care', '頭髮護理'], ['bus station', '巴士站'], ['point of interest','景點'], ['hardware store','五金店'],
                      ['police','警察局'], ['museum','博物館'], ['grocery or supermarket','雜貨店或便利商店'], ['place of worship','寺廟'],
                      ['transit station','公車站'], ['train station','火車站'], ['route','道路'], ['store','商店'], ['storage','批發'],
                      ['jewelry store','珠寶店'], ['department store','百貨公司'], ['gas station','加油站'], ['travel agency','旅行社'],
                      ['airport','機場'], ['night club','夜店'], ['city hall','政府'], ['florist','花店'], ['pharmacy','藥房'], ['church','教會'],
                      ['establishment','機構'], [ 'university','大學'], ['fire station','消防局'], ['subway station','地鐵站'],
                      ['car repair','汽車保養廠']]
        # 要刪除的類別
        remove_list = [ 'colloquial_area', 'locality','neighborhood', 'finance']

        # 剛get的類別
        temp = []
        # 處理過後的類別
        types = []
        
        nearby_results = gmaps.places_nearby(
            location=(self.gpsA,self.gpsB), radius=100)  # 以取得座標為中心 ，半徑X公尺的店家資訊
        nearby_results = nearby_results['results']
        for nearby_result in nearby_results:
            place_id = nearby_result['place_id']
            detail_results = gmaps.place(place_id, language="zh-tw")
            for t in detail_results['result']['types']:
                temp.append(t)
                #print(detail_results['result']['name'])  # 印出名字
                #print(detail_results['result']['types'])  # 印出類別

        temp = set(temp)  # 移除重複
        for s in temp:
            types.append(s)

        for er in remove_list:
            if er in types:
                types.remove(er)  # 移除不必要的類別

        for num in range(len(types)):
            n = types[num]
            types[num] = n.replace('_', ' ')

            # print(types)
            # print('您的附近有：',end='')
            
        self.place = [] #中文地點類別
        self.englishplace = [] #英文地點類別
        for i in types:
            for j in range(len(types_list)):
                if i == types_list[j][0]:
                    if i != types[-1]:
                        self.place.append(types_list[j][1])
                        self.englishplace.append(types_list[j][0])
        
        self.placeenglishword = [] #相關單字
        temp = []
        
        index = 0
        for i in self.englishplace:
            my_data={'type': str(i)}
            r = requests.post('http://120.96.63.79/oit_English/gpswordadmingetword', data = my_data)
            data=r.json()
            self.placeenglishword.append(data['gps_class_word'])
        
        Clock.schedule_once(self.loaddata, 0)
        
    def loaddata(self,dt): 
        Builder.load_string(gpstest)
        self.gpstest = Factory.Gpstest()
        self.main_widget.ids.scr_mngr.add_widget(self.gpstest)
        index = 0
        for name_tab in self.place:            
            tab = GpsTab(text=str(name_tab),data=self.placeenglishword[index],gpswordtype=self.englishplace[index])
            index += 1
            self.main_widget.ids.scr_mngr.get_screen(
                'Gpstest').ids.android_tabs.add_widget(tab)
        self.main_widget.ids.scr_mngr.current = 'Gpstest'
        self.loginpop.dismiss()

    def start(self, minTime, minDistance):
        gps.start(minTime, minDistance)

    def stop(self):
        gps.stop()

    def on_location(self, **kwargs):
        self.gpsA = kwargs["lat"]
        self.gpsB = kwargs['lon']

    def on_status(self, stype, status):
        print(self.gps_status)
        #self.gps_status = 'type={}\n{}'.format(stype, status)

    def on_pause(self):
        gps.stop()
        return True

    def on_resume(self):
        gps.start(1000, 0)
        pass

    rearrangement = None

    def show_RearrangementPage(self):
        if not self.rearrangement:
            Builder.load_string(rearrangement)
            self.rearrangement = Factory.Rearrangement()
            self.main_widget.ids.scr_mngr.add_widget(self.rearrangement)
        self.main_widget.ids.scr_mngr.current = 'Rearrangement'

    connecttest = None

    def show_ConnecttestPage(self):
        if not self.connecttest:
            Builder.load_string(connecttest)
            self.connecttest = Factory.Connecttest()
            self.main_widget.ids.scr_mngr.add_widget(self.connecttest)
        self.main_widget.ids.scr_mngr.current = 'Connecttest'

    wordtesthistroy = None

    # 作答紀錄摘要
    def show_wordtesthistroyPage(self):
        if not self.wordtesthistroy:
            Builder.load_string(wordtesthistroy)
            self.wordtesthistroy = Factory.Wordtesthistroy()
            self.main_widget.ids.scr_mngr.add_widget(self.wordtesthistroy)
        self.main_widget.ids.scr_mngr.current = 'Wordtesthistroy'

    # 內容

    viewhistroycontect = None

    def show_viewhistroycontect(self, index, trange):
        if trange == "全民英檢初級":
            trange = "VocabularyGEPTLA"
        if trange == "全民英檢中級":
            trange = "VocabularyGEPTLB"
        if trange == "全民英檢中高級":
            trange = "VocabularyGEPTLC"
        if trange == "國中英文單字":
            trange = "VocabularyLA"
        if trange == "GPS場景單字":
            trange = "GpsWordtest"
        data = str(index)+","+trange
        
        if not self.viewhistroycontect:
            Builder.load_string(viewhistroycontect)
            self.viewhistroycontect = Factory.Viewhistroycontect(id=data)
            self.main_widget.ids.scr_mngr.add_widget(self.viewhistroycontect)
        self.main_widget.ids.scr_mngr.current = 'Viewhistroycontect'
        self.main_widget.ids.scr_mngr.get_screen('Viewhistroycontect').id=data

class ItemMenu(CircularRippleBehavior, ButtonBehavior, BoxLayout):
    name_item = StringProperty()
    icon_item = StringProperty()


class MenuDialog(BoxLayout):
    background = StringProperty()


class MyCheckbox(IRightBodyTouch, MDCheckbox):
    pass


class MyAvatar(ILeftBody, Image):
    pass


class GpsTab(BoxLayout, MDTabsBase):
    data = ListProperty()
    gpswordtype = StringProperty()
    def __init__(self, data,gpswordtype, **kwargs):
        super(GpsTab, self).__init__(**kwargs)
        self.data = data
        self.gpswordtype = gpswordtype
        num=1
        for i in self.data:
            self.ids.tabdata.add_widget(gpswordlist(englishword=str(i),id=str(num),gpswordtype=gpswordtype))
            num+=1

class gpswordlist(BoxLayout):
    callback = ObjectProperty(lambda x: None)
    englishword = StringProperty()
    gpswordtype = StringProperty()
    
    def __init__(self, englishword,gpswordtype, **kwargs):
        super(gpswordlist, self).__init__(**kwargs)
        self.englishword = englishword
        self.gpswordtype=gpswordtype

class loadingscreen(Popup):
    title = StringProperty()
    color = StringProperty()
    answer = ObjectProperty()
    textinputtext = StringProperty()

    def __init__(self, title, **kwargs):
        super(loadingscreen, self).__init__(**kwargs)
        self.set_description(title)
        self.textinputtext = 'palim'

    def set_description(self, title):
        self.title = title 
