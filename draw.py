import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import warnings
import requests
import random
import matplotlib 
print(matplotlib.matplotlib_fname())

warnings.simplefilter("ignore")

from matplotlib.font_manager import findfont, FontProperties  

'''
my_data={'user': 'tet'}
r = requests.post('http://120.96.63.79/oit_English/getmatplotlib', data = my_data)
data=r.json()
'''

data={'word': ['second-hand','stock','cheese'], 'range': ['VocabularyGEPTLB','store','food']}
print(data) 
colorlist=['aqua','yellow']


colors=[]
edge_list=[]


for i in list(dict.fromkeys(data['range'])): #先關聯使用者與常錯類型   
    if i =="VocabularyLA":
        i ="國中英文單字"
    elif   i=="VocabularyGEPTLA":
        i ="全民英檢初級"
    elif  i =="VocabularyGEPTLB":
        i ="全民英檢中級"
    elif  i =="VocabularyGEPTLC":
        i ="全民英檢高級"
    edge_list.append(('tet', str(i)))

for i in range(len(data['word'])):
    if (data['range'][i] =="VocabularyLA"):
        data['range'][i] ="國中英文單字"
    elif  data['range'][i] =="VocabularyGEPTLA":
        data['range'][i] = "全民英檢初級"
    elif  data['range'][i] =="VocabularyGEPTLB":
        data['range'][i] = "全民英檢中級"
    elif  data['range'][i]  =="VocabularyGEPTLC":
        data['range'][i] ="全民英檢高級"
    
    edge_list.append((data['range'][i],data['word'][i]))

    
for i in (data['word']):
    r = requests.get('https://relatedwords.org/api/related?term='+str(i))
    data1 =r.json()
    relationwordlsit=[]
    for j in data1:
        if " " not in j['word']:
            relationwordlsit.append(j['word'])
        if len(relationwordlsit) >=3:
            break
    for k in relationwordlsit:       
        edge_list.append((i,k))

for i in range(len(edge_list)+1):
    colors.append("cyan")
for i in range(len(list(dict.fromkeys(data['range'])))+1):
    colors[i] ="blue"
for i in range(len(list(dict.fromkeys(data['range'])))+1,len(list(dict.fromkeys(data['range'])))+len(data['range'])+1):
    colors[i] ="red"



colors[0]="yellow"

#colors = ['grey', 'aqua', 'aqua', 'aqua', 'aqua', 'aqua', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow']
#edge_list = [('tet', 'SPA'), ('tet', 'atm'), ('tet', 'florist'),('tet','bakery'),("tet","museum"),("SPA","bath"),("atm","teller"),("atm","atmosphere"),("florist","corsage"),("bakery","baked"),("museum","louvre"),("SPA","sauna")]
g = nx.Graph(edge_list)
pos = nx.spring_layout(g, k=0.3*1/np.sqrt(len(g.nodes())), iterations=20)
plt.figure(3, figsize=(10, 10))
nx.draw(g, pos=pos,with_labels=True, node_size=900, node_color = colors)
nx.draw_networkx_labels(g, pos=pos)
plt.savefig('/var/www/html/image.jpg')
#plt.savefig('image.jpg')

