import os
import time
import threading
from kivymd.uix.dropdownitem import MDDropDownItem
from kivymd.uix.dialog import MDDialog
from kivymd.behaviors.ripplebehavior import CircularRippleBehavior
from kivy.uix.popup import Popup
from kivymd.uix.tab import MDTabsBase
from kivymd.uix.expansionpanel import MDExpansionPanel
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivymd.uix.label import MDLabel
from kivymd.theming import ThemeManager
from kivymd.uix.selectioncontrol import MDCheckbox
from kivymd.uix.list import ILeftBody, ILeftBodyTouch, IRightBodyTouch
from kivymd.uix.button import MDIconButton, MDRoundFlatButton, MDRaisedButton, MDFlatButton, MDFillRoundFlatButton
from kivy.utils import get_hex_from_color
from kivy.uix.image import Image
from kivy.properties import ObjectProperty, StringProperty, ListProperty, NumericProperty, BooleanProperty
from kivy.lang import Builder
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.app import App
from kivymd.uix.useranimationcard import MDUserAnimationCard
from kivy.factory import Factory
from kivymd.uix.menu import MDDropdownMenu
from kivymd.uix.textfield import MDTextField
from kivy.uix.image import AsyncImage
from kivy.uix.behaviors import ButtonBehavior
from kivy.metrics import dp
from kivy.uix.widget import Widget

import certifi
import os
import platform


def toast(text):  # 懸浮提示視窗
    # FIXME: crush with Python3.
    try:
        from kivymd.toast import toast
    except TypeError:
        from kivymd.toast.kivytoast import toast
    toast(text)


# 登入成功主畫面
menu = '''
<Menu@Screen> 
    name: 'Menu'
    do_scroll_x: False
    BoxLayout:
        canvas:
            Rectangle:
                size: self.size
                pos: self.pos
                source: './assets/coffee_crop.jpg'
        height: root.height
        spacing:20
        padding:[40,50,40,20]
        orientation: 'vertical'
        size_hint_y: None
        BoxLayout:
            size_hint: 1,.5
            spacing:20
            MDRaisedButton:        
                size_hint: .3,1
                theme_text_color: 'Primary'
                _radius:30.0
                md_bg_color:(0.9215686274509804,0.2549019607843137,0.2549019607843137,1)
                BoxLayout:
                    orientation: 'vertical'
                    Image:
                        source: 'assets/pen.png'
                        size: self.texture_size
                    Label:
                        markup: True
                        text: '單字學習'
                        bold: True
                        font_size:root.width/20
                        
            MDRaisedButton:
                _radius:30.0
                md_bg_color:(0.9803921568627,0.7764705882352,0.03921568627450,1)
                size_hint: .7,1
                font_style: 'Body1'
                theme_text_color: 'Primary'
                BoxLayout:
                    orientation: 'vertical'
                    Image:
                        source: 'assets/book.png'
                        size: self.texture_size
                    Label:
                        markup: True
                        text: '單字測驗'
                        bold: True
                        font_size:root.width/20
        BoxLayout:
            spacing:20
            BoxLayout:
                spacing:20                
                orientation: 'vertical'
                MDRaisedButton:
                    _radius:30.0
                    md_bg_color:(0.298039215686,0.6980392156862,0.2980392156862,1)
                    size_hint: 1,0.45
                    font_style: 'Body1'
                    theme_text_color: 'Primary'
                    BoxLayout:
                        orientation: 'vertical'
                        Image:
                            source: 'assets/book.png'
                            size: self.texture_size
                        Label:
                            markup: True
                            text: '單字測驗'
                            bold: True
                            font_size:root.width/20
                MDRaisedButton:
                    _radius:30.0
                    md_bg_color:(0.03921568627450,0.4784313725490,0.9803921568627,1)
                    size_hint: 1,0.55
                    font_style: 'Body1'
                    theme_text_color: 'Primary'
                    BoxLayout:
                        orientation: 'vertical'
                        Image:
                            source: 'assets/book.png'
                            size: self.texture_size
                        Label:
                            markup: True
                            text: '單字測驗'
                            bold: True
                            font_size:root.width/20
            BoxLayout:
                orientation: 'vertical'
                spacing:20
                MDRaisedButton:
                    _radius:30.0
                    md_bg_color:(0.0588235294117,0.2117647058823,0.98039215686274,1)
                    size_hint: 1,.7
                    font_style: 'Body1'
                    theme_text_color: 'Primary'
                    BoxLayout:
                        orientation: 'vertical'
                        Image:
                            source: 'assets/book.png'
                            size: self.texture_size
                        Label:
                            markup: True
                            text: '單字測驗'
                            bold: True
                            font_size:root.width/20
                MDRaisedButton:
                    _radius:30.0
                    md_bg_color:(0.62745098039215,0.05882352941176,0.98039215686274,1)
                    size_hint: 1,.3
                    font_style: 'Body1'
                    theme_text_color: 'Primary'
                    BoxLayout:
                        orientation: 'vertical'
                        Image:
                            source: 'assets/book.png'
                            size: self.texture_size
                        Label:
                            markup: True
                            text: '單字測驗'
                            bold: True
                            font_size:root.width/30
        MDRaisedButton:
            _radius:30.0
            md_bg_color:(0.98039215686274,0.62745098039215,0.05882352941176,0.88)
            size_hint: 1,.5
            font_style: 'Body1'
            theme_text_color: 'Primary'
            BoxLayout:
                orientation: 'vertical'
                Image:
                    source: 'assets/book.png'
                    size: self.texture_size
                Label:
                    markup: True
                    text: '儀錶板'
                    bold: True
                    font_size:root.width/20
'''

# 題庫管理畫面
qustionAdmin = '''
#:import MDDropdownMenu kivymd.uix.menu.MDDropdownMenu

<EachItem@BoxLayout>:
    question: ''
    level:''
    rv_key: 0
    GridLayout:
        rows:1
        cols:3
        BoxLayout: 
            MDCheckbox:
                size_hint: None, None
                size: dp(48), dp(48)
                size_size: 0.1,0.1
                active: root.rv_key in app.current_selection
                on_active: app.select_row(root.rv_key, self.active)
            MDLabel:
                font_size:14                                
                text: str(root.rv_key)
                size: dp(48), dp(48)
                size_size: 0.1,0.1
                size_hint: None, None
            MDLabel:                
                font_size:14
                text: root.question
            MDLabel:
                font_size:14
                halign: 'center'
                text_size: None, None
                size_hint: 1,1
                size: dp(48), dp(48)
                text: root.level
            MDRaisedButton:
                size_hint_y:None
                theme_text_color: 'Primary'
                text: "編輯題目"
                on_release:
                    
                    app.callpopup(app.filerword,root.rv_key)
                    
                

<QustionAdmin@Screen>
    name: 'QustionAdmin'
    on_enter: app.loadquestion( '["tb_choice","字彙測驗"]')
    BoxLayout:
        orientation: 'vertical'
        spacing: dp(10)
        padding: dp(20)

        BoxLayout:
            spacing:dp(20)
            size_hint_y: None
            height: self.minimum_height
            MDLabel:
                id:"test"
                text:"選擇題目類型"
                size_hint_x:None
            MDRaisedButton:
                size_hint_y:None
                theme_text_color: 'Primary'
                text: app.filtertype
                on_release:
                    MDDropdownMenu(items=app.questiontype, width_mult=3).open(self)
        BoxLayout:
            spacing:dp(20)
            size_hint_y: None
            height: self.minimum_height
            MDRaisedButton:
                size_hint_y:None
                theme_text_color: 'Primary'
                text: "新增題庫"
                on_release:
                    MDDropdownMenu(items=app.questionadm, width_mult=3).open(self)
            MDRaisedButton:
                size_hint_y:None
                theme_text_color: 'Primary'
                text: "刪除題目"
                on_release:
                    app.deletequestion()

        BoxLayout:
            height: "50"
            size_hint_y: None
            GridLayout:
                padding: 10, 0, 10, 0
                canvas:
                    Color:
                        rgba: 1, 1, 1, .1
                    Rectangle:
                        size: self.size
                rows:1
                cols:3
                MDLabel:
                    size_hint_x:0.3
                    text: "編號"
                MDLabel:
                    text: "題目"
                MDLabel:
                    size_hint_x:0.7                
                    text: "難易度"
        RecycleView:
            id: rv
            bar_width: 8
            scroll_type: ['bars', 'content']
            scroll_wheel_distance: 120
            viewclass: 'EachItem' 
            RecycleBoxLayout:                
                size_hint_y: None
                height: self.minimum_height
                default_size: None, 80
                default_size_hint: 1, None
                orientation: 'vertical'
                spacing: 3
'''


class Screens(object):

    home = None
    manager_swiper = None

    def show_homePage(self):
        self.main_widget.ids.scr_mngr.current = 'Home'
    menu = None

    def show_MainPage(self):
        if not self.menu:
            Builder.load_string(menu)
            self.menu = Factory.Menu()
            self.main_widget.ids.scr_mngr.add_widget(self.menu)
        self.main_widget.ids.scr_mngr.current = 'Menu'

    vocabluaryGEPTLA = None

    qustionAdmin = None

    def show_QusetionAdmPage(self):
        if not self.qustionAdmin:
            Builder.load_string(qustionAdmin)
            self.qustionAdmin = Factory.QustionAdmin()
            self.main_widget.ids.scr_mngr.add_widget(self.qustionAdmin)
        self.main_widget.ids.scr_mngr.current = 'QustionAdmin'
