# -*- coding: utf-8 -*-
from plyer import gps
import ast
import requests
from kivymd.uix.dropdownitem import MDDropDownItem
from kivymd.uix.dialog import MDDialog
from kivymd.behaviors.ripplebehavior import CircularRippleBehavior
from kivy.uix.popup import Popup
from kivymd.uix.tab import MDTabsBase
from kivymd.uix.expansionpanel import MDExpansionPanel
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivymd.uix.label import MDLabel
from kivymd.theming import ThemeManager
from kivymd.uix.selectioncontrol import MDCheckbox
from kivymd.uix.list import ILeftBody, ILeftBodyTouch, IRightBodyTouch
from kivymd.uix.button import MDIconButton,MDRoundFlatButton,MDRaisedButton,MDFlatButton,MDFillRoundFlatButton
from screens import Screens
from kivy.utils import get_hex_from_color
from kivy.uix.image import Image
from kivy.properties import ObjectProperty, StringProperty, ListProperty,NumericProperty,BooleanProperty
from kivy.lang import Builder
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.app import App
from kivymd.uix.useranimationcard import MDUserAnimationCard
from kivy.factory import Factory
from kivymd.uix.menu import MDDropdownMenu
from kivymd.uix.textfield import MDTextField
from kivy.uix.image import AsyncImage
from kivy.uix.behaviors import ButtonBehavior
from kivy.metrics import dp
from kivy.uix.widget import Widget

import certifi
import os
import platform
import googlemaps
google_key = "AIzaSyC3Zs_Aln2ewhniP1cvuCb5hajMZyYbJxE"
gmaps = googlemaps.Client(key=google_key)

'''
from android.permissions import request_permissions, Permission
request_permissions([Permission.RECORD_AUDIO])
'''

def toast(text):
    try:
        from kivymd.toast import toast
    except TypeError:
        from kivymd.toast.kivytoast import toast
    toast(text)

main_widget_kv = """
#:import get_hex_from_color kivy.utils.get_hex_from_color
#:import MDRoundFlatButton kivymd.uix.button.MDRoundFlatButton
#:import NavigationLayout kivymd.uix.navigationdrawer.NavigationLayout
#:import ThreeLineListItem kivymd.uix.list.ThreeLineListItem
#:import MDCheckbox kivymd.uix.selectioncontrol.MDCheckbox
#:import MDAccordion kivymd.uix.accordion.MDAccordion
#:import MDAccordionItem kivymd.uix.accordion.MDAccordionItem
#:import MDAccordionSubItem kivymd.uix.accordion.MDAccordionSubItem
#:import MDToolbar kivymd.uix.toolbar.MDToolbar
#:import MDTextField kivymd.uix.textfield.MDTextField
#:import MDTextFieldRound kivymd.uix.textfield.MDTextFieldRound
#:set color_shadow [0, 0, 0, .2980392156862745]
#:import stt plyer.stt
#:import threading threading
#:import MDRaisedButton kivymd.uix.button.MDRaisedButton
#:import MDSpinner kivymd.uix.spinner.MDSpinner



<CircleWidget@Widget>
    size_hint: None, None
    size: dp(20), dp(20)

    canvas:
        Color:
            rgba: app.theme_cls.primary_color
        Ellipse:
            pos: self.pos
            size: dp(20), dp(20)

<Leakwordpopup>: #克漏字視窗
    answer: "123"
    title: "新增選擇題"
    separator_color:[0,0,0,0]
    title_align:'center'
    size_hint: None, None
    size: app.root.width/1.1, app.root.height/1.1
    auto_dismis: False

    ScrollView:
        BoxLayout:
            id:layout
            orientation: 'vertical'
            size_hint_y: None
            height: self.minimum_height
            padding: dp(48)
            spacing: 10
            MDRaisedButton:
                size_hint:None,None
                id:apply_qut
                text:"新增題目"
                on_release:root.save()
                pos_hint: {'center_x': 0.5, 'center_x': 0.5}
            MDLabel:
                text:"題目內容"
            MDTextField:
                id:question_content
                multiline: True
            MDLabel:
                text:"題目解析"
            MDTextField:
                id:question_analysis
                multiline: True

            MDRaisedButton:
                size_hint:None,None
                id:apply_btn
                text:"新增選項"
                on_release:
                    root.add()
                pos_hint: {'center_x': 0.5, 'center_x': 0.5}
                               
<Choicequestionpopup>: #選擇題視窗
    answer: "123"
    title: "新增選擇題"
    separator_color:[0,0,0,0]
    title_align:'center'
    size_hint: None, None
    size: app.root.width/1.1, app.root.height/1.1
    auto_dismis: False

    ScrollView:
        do_scroll_x: False        
        BoxLayout:           
            height: self.minimum_height
            orientation: 'vertical'
            id:box_top
            padding:[20,20,20,20]
            BoxLayout:
                spacing:dp(40)     
                MDLabel:                             
                    font_style: 'H5'                   
                    text: "類型"                    
                    pos_hint: {'center_y': .5}     
                    size_hint:None,None        
                MDRaisedButton:              
                    id:ctype
                    text:"請選擇"
                    pos_hint: {'center_y': .5}                
                    size_hint_x:0.6
                    theme_text_color: 'Primary'
                    on_release:
                        MDDropdownMenu(items=root.newgpswordtype, width_mult=3).open(self)
            BoxLayout:                                                   
                MDLabel:
                    pos_hint: {'center_y': 0.5}
                    size_hint_y:None
                    size_hint_x:None      
                    font_style: 'H5'
                    text: "題目"                
                    theme_text_color: 'Custom'
                MDTextField:                                        
                    id:question
                    pos_hint: {'center_y': 0.5}
                    helper_text_mode: "on_focus"
                    helper_text: ""                   
            BoxLayout:
                MDLabel:
                    pos_hint: {'center_y': 0.5}
                    size_hint_y:None
                    size_hint_x:None      
                    font_style: 'H5'
                    text: "選項A"                
                    theme_text_color: 'Custom'
                MDTextField:                 
                    id:questionA
                    pos_hint: {'center_y': 0.5}
                    theme_text_color: 'Custom'
                    helper_text_mode: "on_focus"
                    helper_text: ""                    
            BoxLayout:
                MDLabel:
                    pos_hint: {'center_y': 0.5}
                    size_hint_y:None
                    size_hint_x:None        
                    font_style: 'H5'
                    text: "選項B"                                   
                MDTextField:                                      
                    id:questionB
                    pos_hint: {'center_y': 0.5}
                    theme_text_color: 'Custom'
                    helper_text_mode: "on_focus"
                    helper_text: ""                  
            BoxLayout:                                             
                MDLabel:
                    pos_hint: {'center_y': 0.5}
                    size_hint_y:None
                    size_hint_x:None        
                    font_style: 'H5'
                    text: "選項C"                
                    theme_text_color: 'Custom'
                MDTextField:
                    id:questionC
                    pos_hint: {'center_y': 0.5}
                    theme_text_color: 'Custom'
                    helper_text_mode: "on_focus"
                    helper_text: ""
            BoxLayout:
                MDLabel:
                    pos_hint: {'center_y': 0.5}
                    size_hint_y:None
                    size_hint_x:None       
                    font_style: 'H5'
                    text: "選項D"                
                    theme_text_color: 'Custom'
                MDTextField:
                    id:questionD
                    pos_hint: {'center_y': 0.5}
                    theme_text_color: 'Custom'
                    helper_text_mode: "on_focus"
                    helper_text: ""                                              
            BoxLayout:                  
                BoxLayout:
                    size_hint_y:None
                    pos_hint: {'center_y': 0.5}
                    size_hint_x:None                   
                    MDLabel:                        
                        font_style: 'H5'
                        text: "答案"                
                        theme_text_color: 'Custom'
                MDCheckbox:
                    id: ans_chkboxA
                    group:'test'
                MDLabel:
                    font_style: 'H5'
                    theme_text_color: 'Primary'
                    text: "A"
                MDCheckbox:
                    id: ans_chkboxB
                    group:'test'
                MDLabel:
                    font_style: 'H5'
                    theme_text_color: 'Primary'
                    text: "B"
                MDCheckbox:
                    id: ans_chkboxC
                    group:'test'
                MDLabel:
                    font_style: 'H5'
                    theme_text_color: 'Primary'
                    text: "C"
                MDCheckbox:
                    id: ans_chkboxD
                    group:'test'
                MDLabel:
                    font_style: 'H5'
                    theme_text_color: 'Primary'
                    text: "D"
            BoxLayout:                  
                BoxLayout:
                    size_hint_y:None
                    pos_hint: {'center_y': 0.5}
                    size_hint_x:None                   
                    MDLabel:                        
                        font_style: 'H5'
                        text: "解析"                
                        theme_text_color: 'Custom'
                MDTextField:                                       
                    id:choice_analysis
                    pos_hint: {'center_y': 0.5}
                    theme_text_color: 'Custom'
                    helper_text_mode: "on_focus"
                    helper_text: ""
                  
            MDRaisedButton:
                id:apply_btn
                text: "新增題目"
                elevation_normal: 2
                opposite_colors: True
                pos_hint: {'center_x': 0.5, 'center_x': 0.5}
                on_release:root.apply_btn()
                       
<SignupPopup>: #註冊彈出式視窗
    answer: "123"
    title: "註冊"
    separator_color:[0,0,0,0]
    title_align:'center'
    size_hint: None, None
    size: app.root.width/1.1, app.root.height/1.1
    auto_dismis: False

    ScrollView:
        do_scroll_x: False
        BoxLayout:           
            spacing: dp(1)
            orientation: 'vertical'
            padding:[20,20,20,20]
            pos_hint: {'center_x': .5}
            MDLabel:
                font_style: 'H5'
                text: "帳號"                
                theme_text_color: 'Custom'
                text_color: (1,1,1,1)
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                id:Signup_account
                theme_text_color: 'Custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                line_color_normal:(1,1,1,1)
                line_color_focus:(1,1,1,1) 
                max_text_length: 10               
            MDLabel:
                font_style: 'H5'
                theme_text_color: 'Custom'
                text: "使用者名稱"
                text_color: (1,1,1,1)
                password: True
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                id:Signup_username
                color_mode: 'custom'
                line_color_normal:(1,1,1,1)
                helper_text_mode: "on_focus"
                helper_text: ""
                line_color_focus:(1,1,1,1)     
                max_text_length: 20
            MDLabel:
                font_style: 'H5'
                theme_text_color: 'Custom'
                text: "密碼"
                text_color: (1,1,1,1)       
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                id:Signup_password
                color_mode: 'custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                line_color_focus:(1,1,1,1)  
                line_color_normal:(1,1,1,1)
                password: True                
                max_text_length: 20     
            MDLabel:
                font_style: 'H5'
                theme_text_color: 'Custom'
                text: "確認密碼"
                text_color: (1,1,1,1)
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                id:Signup_repassword
                color_mode: 'custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                line_color_focus:(1,1,1,1)  
                password: True
                max_text_length: 20             
                line_color_normal:(1,1,1,1)
            MDLabel:
                font_style: 'H5'
                theme_text_color: 'Custom'
                text: "信箱"
                text_color: (1,1,1,1)
                password: True
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1                    
                id:Signup_email
                color_mode: 'custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                max_text_length: 10
                line_color_focus:(1,1,1,1)  
                line_color_normal:(1,1,1,1)
                max_text_length:50             
            MDLabel:
                font_style: 'H5'
                theme_text_color: 'Custom'
                text: "電話"
                text_color: (1,1,1,1)
                password: True
            MDTextField:
                canvas.before:
                    Color:
                        rgba: 1, 1, 1, 1
                id:Signup_phone
                color_mode: 'custom'
                helper_text_mode: "on_focus"
                helper_text: ""
                line_color_focus:(1,1,1,1)     
                line_color_normal:(1,1,1,1)
                hint_text_color:(1,1,1,1)
                max_text_length: 10
            BoxLayout:
                size_hint: None, None
                size: self.minimum_size
                spacing: dp(10)
                pos_hint: {'center_x': .5}
                MDFillRoundFlatButton:
                    id:signup
                    text: "註冊"
                    pos_hint: {'center_x': .5}
                    theme_text_color :'Custom'
                    background_color: (1,1,1,1)
                    text_color:(1,1,1,1)
                    ripple_colors:(1,1,1,1)
                    on_release:
                        print(signup._current_button_color)
                        app.Signup(root,Signup_account,Signup_username,Signup_password,Signup_repassword,Signup_email,Signup_phone)
                MDRoundFlatButton:
                    id:signup
                    text: "關閉"
                    pos_hint: {'center_x': .5}
                    theme_text_color :'Custom'
                    background_color: (1,1,1,1)
                    text_color:(1,1,1,1)
                    ripple_colors:(1,1,1,1)
                    on_release:
                        root.dismiss()
                Widget:
                    size_hint_y: None
                    height: dp(10) 


NavigationLayout:
    id: nav_layout
    BoxLayout:
        MDAccordion:
            orientation: 'vertical'
            size_hint_x: None
            width: dp(320)
            MDAccordionItem
                title:'登入'
                icon: 'home'
                MDAccordionSubItem:
                    text: "登入畫面"
                    on_release:
                        app.show_homePage()
                        app.set_title_toolbar(self.text)
                        app.root.toggle_nav_drawer()
                MDAccordionSubItem:
                    text: "題庫管理"
                    on_release:
                        app.show_QusetionAdmPage()
                        app.root.toggle_nav_drawer()                       
                
    BoxLayout:
        orientation: 'vertical'
        MDToolbar:
            id: toolbar
            title: '無所不在英文挑戰系統'
            md_bg_color: app.theme_cls.primary_color
            background_palette: 'Primary'
            background_hue: '500'
            left_action_items: [['menu', lambda x: app.root.toggle_nav_drawer()]]
            right_action_items: [['brightness-6', lambda x:app.Change_style()]]

        ScreenManager:
            id: scr_mngr
            Screen:
                name:"Home"
                BoxLayout:
                    id :Login_lay
                    orientation: 'vertical'
                    canvas:
                        Rectangle:
                            size: self.size
                            pos: self.pos
                            source: './assets/loginimg.png'                        
                    BoxLayout:                                          
                        id: box_top
                        orientation: 'vertical'
                        size_hint: None, None
                        size: self.minimum_size
                        pos_hint: {'center_x': .5, 'top': 1}
                        padding: dp(5)
                        BoxLayout:
                            spacing: dp(5)
                            size_hint_y: None
                            height: dp(20)

                            CircleWidget:
                            CircleWidget:
                            Widget:
                        Widget:
                            size_hint_y: None
                            height: dp(5)

                        Label:
                            markup: True
                            text: '[size=35]             無所不在[/size]'
                            size_hint: None, None
                            size: self.texture_size
                            bold: True
                        
                        BoxLayout:
                            spacing: dp(5)
                            size_hint: None, None
                            size: self.minimum_size

                            Label:
                                markup: True
                                text: '[size=50]英文單字測驗系統[/size]'
                                size_hint: None, None
                                size: self.texture_size
                                bold: True
                            CircleWidget:
             
                    BoxLayout:
                        canvas.before:
                            Color: 
                                rgba: 1,1,1, 1
                            Line:
                                width: 1
                                rectangle: self.x + 10, self.y + 10,self.width - 20,self.height - 20
                        id:box_top
                        spacing: dp(1)
                        orientation: 'vertical'
                        padding:[20,20,20,20]
                        pos_hint: {'center_x': .5}
                        MDLabel:
                            font_style: 'H4'
                            text: "帳號"
                            theme_text_color: 'Custom'
                            text_color: (1,1,1,1)
                            
                        MDTextField:
                            canvas.before:
                                Color:
                                    rgba: 1, 1, 1, 1
                            id:login_account
                            color_mode: 'custom'
                            helper_text_mode: "on_focus"
                            helper_text: ""
                            
                            line_color_focus:(1,1,1,1)
                            line_color_normal:(1,1,1,1)
                        MDLabel:
                            font_style: 'H4'
                            theme_text_color: 'Custom'
                            text: "密碼"
                            text_color: (1,1,1,1)
                            
                        MDTextField:
                            canvas.before:
                                Color:
                                    rgba: 1, 1, 1, 1
                            id:login_password
                            color_mode: 'custom'
                            helper_text_mode: "on_focus"
                            helper_text: ""
                            line_color_focus:(1,1,1,1)
                            line_color_normal:(1,1,1,1)
                            password: True
                                
                        BoxLayout:
                            size_hint: None, None
                            size: self.minimum_size
                            spacing: dp(10)
                            pos_hint: {'center_x': .5}
                            MDRoundFlatButton:                                
                                id:sigbtn
                                text: "註冊"
                                pos_hint: {'center_x': .5}
                                color_mode: 'custom'
                                theme_text_color :'Custom'
                                background_color: (1,1,1,1)
                                text_color:(1,1,1,1)
                                on_release:app.on_call_Signuppopup()
                            MDFillRoundFlatButton:
                                text: "登入"
                                pos_hint: {'center_x': .5}
                                theme_text_color :'Custom'
                                background_color: (1,1,1,1)
                                text_color:(0,0,0,1)
                                md_bg_color: (1,1,1,1)
                                on_release:
                                    app.Login(login_account.text,login_password.text)

                            Widget:
                                size_hint_y: None
                                height: dp(10) 

                    BoxLayout:
                        size_hint_y: None
                        height: dp(50)
                        
"""
class ImageButton(ButtonBehavior, AsyncImage):
    pass

class KitchenSink(App, Screens):
    
    current_selection = ListProperty([])
    filtertype = StringProperty()
    theme_cls = ThemeManager()
    theme_cls.primary_palette = 'Orange'
    title = "無所不在英文單字測驗系統"
    checkbox_is_active = ObjectProperty(False)
    def __init__(self, **kwargs):
        
        super(KitchenSink, self).__init__(**kwargs)
        self.selectqustion=[]

        self.filerword=""

        self.filtertype="選擇篩選單字" #篩選單字
        #下拉選單內容
        self.questionadm = [
                     {'viewclass': 'MDRaisedButton',
                        'text': '選擇題',
                        'on_release': lambda : self.on_call_choicequestionpopup()},
                        {'viewclass': 'MDRaisedButton',
                        'text': '克漏字',
                        'on_release': lambda :self.on_call_leakwordpopup()},               
        ]
        self.questiontype = [
            {'viewclass': 'MDRaisedButton',
            'text': '字彙測驗',
            'on_release': lambda : self.loadquestion('["tb_choice","字彙測驗"]')},
            {'viewclass': 'MDRaisedButton',
            'text': '對話測驗',
            'on_release': lambda :self.loadquestion('["tb_choice","對話測驗"]')},
            {'viewclass': 'MDRaisedButton',
            'text': '對話測驗',
            'on_release': lambda :self.loadquestion('["tb_leakword_article","克漏字"]')},
        ]
        
       
    def Signup(self,root,userid,username,userpassword,userrepassword,usermail,userphone): #註冊作業
        form=[userid,username,userpassword,userrepassword,usermail,userphone]
        num=0
        for i in form:            
            if i.text == "":
                i.helper_text="不能為空"
                i.line_color_normal=(1,0,0,1)
                i.helper_text_mode = "persistent"
                return
            elif i.max_text_length<len(i.text):
                i.helper_text="字數過長"
                i.line_color_normal=(1,0,0,1)
                i.helper_text_mode = "persistent"
                return
            else:
                i.helper_text=""
                i.line_color_normal=(1,1,1,1)
                i.helper_text_mode = "persistent"
            num+=1
        

        if (userpassword.text != userrepassword.text ):
            userpassword.helper_text="兩次的密碼不一致"
            userpassword.line_color_normal=(1,0,0,1)
            userpassword.helper_text_mode = "persistent"
            return
        else:    
            userrepassword.helper_text=""
            userrepassword.line_color_normal=(1,1,1,1)
            userrepassword.helper_text_mode = "persistent"
        
        my_data = {'id': userid.text, 'name': username.text, 'password': userpassword.text,'email':usermail.text,"phone":userphone.text}
        r = requests.post('http://120.96.63.79/oit_English/creatcount', data = my_data)
        data = r.json()
        if (data['message'] == "success"):
            toast("帳號註冊成功")
            root.dismiss()
        else:
            toast("帳號已重複")
     
    def Login(self,ac,pw):   #登入作業      
        my_data = {'account': str(ac), 'password': str(pw)}
        r = requests.post('http://120.96.63.79/oit_English/Login', data = my_data)
        print(r.json()['Status'])
        if r.json()['Status'] =="Success":
            toast(str(r.json()['name'] )+"您好")
            Screens.show_MainPage(self)
            self.userdata=r.json()
        else:
            toast("帳號或密碼錯誤")
        
    
    def set_title_toolbar(self, title): 
        self.main_widget.ids.toolbar.title = title
    
   
    def on_call_Signuppopup(self):
        poti = SignupPopup('註冊',background = 'atlas://data/images/defaulttheme/vkeyboard_key_down')
        poti.open()
    def on_call_choicequestionpopup(self,EditMode=False,id=0,type=0):
        if EditMode:
            poti = Choicequestionpopup(title='新增選擇題',mainobject=self,isedit=True,choice_id=str(id),choice_type=type,background = '[0,0,0,0]')
            poti.open()
        else:
            poti = Choicequestionpopup(title='新增選擇題',mainobject=self,isedit=False,background = '[0,0,0,0]')
            poti.open()

    def on_call_leakwordpopup(self,EditMode=False,id=0,type=0): #克漏字彈出式式視窗
        if EditMode:
            poti = Leakwordpopup(title='新增克漏字題目',mainobject=self,isedit=True,choice_id=str(id),choice_type=type,background = '[0,0,0,0]')
            poti.open()
        else:
            poti = Leakwordpopup(title='新增克漏字題目',mainobject=self,isedit=False,background = '[0,0,0,0]')
            poti.open()
        

    def callpopup(self,filerword,rv_key): #呼叫題目彈出是視窗
        print(filerword)
        x = ast.literal_eval(filerword)
        if x[0] == "tb_choice":
            self.on_call_choicequestionpopup(EditMode=True,id=str(rv_key),type=filerword)
        if x[0] == "tb_leakword_article":
            self.on_call_leakwordpopup(EditMode=True,id=str(rv_key),type=filerword)

    def loadquestion(self,type="None",search=False): #載入資料
        from kivymd.uix.card import MDCardPost
        x = ast.literal_eval(type)
        self.filerword = type
        self.filtertype = x[1] #文字顯示
        my_data = {'questiontype': type}
        r = requests.post('http://120.96.63.79/oit_English/getquestionlist', data = my_data)
        data = r.json()
        

        self.main_widget.ids.scr_mngr.get_screen(
                'QustionAdmin').ids.rv.data =  [
                        dict(                            
                            rv_key=i+1,
                            question='{}'.format(data['questioncontent'][i]),
                            level='{}'.format( data['questionlevel'][i])
                        )
                        for i in range(len(data['question_id']))
                    ]
        
    def select_row(self, rv_key, active):
        if active and rv_key not in self.current_selection:
            self.current_selection.append(rv_key)
        elif not active and rv_key in self.current_selection:
            self.current_selection.remove(rv_key)

    def deletequestion(self): #刪除題目
        x = ast.literal_eval(self.filerword) 
        my_data={'tb_name': x[0],'type': str(x[1]),'id':str(self.current_selection)}
        
        
        r = requests.post('http://120.96.63.79/oit_English/delchoicequestion', data = my_data)
        data = r.json()
        self.current_selection=[]
        self.loadquestion(self.filerword)
           
    def build(self):
        self.main_widget = Builder.load_string(main_widget_kv)
        return self.main_widget

    

class Choicequestionpopup(Popup): #新增選擇題視窗
    title = StringProperty()
    ctype = StringProperty()
    choice_id = StringProperty()
    choice_type =StringProperty()
    isedit  = BooleanProperty()
    mainobject =  ObjectProperty()
    def __init__(self, mainobject,isedit,title, **kwargs):
        super(Choicequestionpopup, self).__init__(**kwargs)
        self.title = title  
        self.mainobject = mainobject
        self.newgpswordtype = [
                {'viewclass': 'MDRaisedButton',
                'text': '字彙測驗',
                'on_release': lambda : self.changetype("字彙測驗")},
                {'viewclass': 'MDRaisedButton',
                'text': '對話測驗',
                'on_release': lambda :self.changetype("對話測驗")}]
        if isedit: #編輯模式
            self.ids.apply_btn.text="編輯題目"
            self.ids.ctype.disabled=True
            x = ast.literal_eval(self.choice_type)
            my_data = {'tb_name':x[0],'type':x[1],'id':self.choice_id}
            r = requests.post('http://120.96.63.79/oit_English/getquestioninformation', data = my_data)
            data = r.json()
            self.ids.question.text=data['choice_question']
            self.ids.questionA.text= data['choice_ansA']
            self.ids.questionB.text= data['choice_ansB']
            self.ids.questionC.text= data['choice_ansC']
            self.ids.questionD.text= data['choice_ansD']
            self.ids.choice_analysis.text =  data['choice_analysis']
            self.ids.ctype.text  =data['choice_type']
            answer=[data['choice_ansA'],data['choice_ansB'],data['choice_ansC'],data['choice_ansD']]
                        
            if answer.index(data['choice_answer']) == 0:
                self.ids.ans_chkboxA.active=True
            if answer.index(data['choice_answer']) == 1:
                self.ids.ans_chkboxB.active=True
            if answer.index(data['choice_answer']) == 2:
                self.ids.ans_chkboxC.active=True
            if answer.index(data['choice_answer']) == 3:
                self.ids.ans_chkboxD.active=True

    def changetype(self,word):
        self.ids.ctype.text = word
    def apply_btn(self): #編輯/新增題目
        
        form =[self.ids.question,self.ids.questionA,self.ids.questionB,self.ids.questionC,self.ids.questionD]
        if self.ids.ctype.text == "請選擇":
            toast("部分項目未填")
            return
        
        for i in form:
            if i.text == "":
                i.helper_text="不能為空"
                i.line_color_normal=(1,0,0,1)
                i.helper_text_mode = "persistent"
                toast("部分項目未填")
                return           
            else:
                i.helper_text=""
                i.line_color_normal=(1,1,1,1)
                i.helper_text_mode = "persistent"                

        answerchkbox=[self.ids.ans_chkboxA.active,self.ids.ans_chkboxB.active,self.ids.ans_chkboxC.active,self.ids.ans_chkboxD.active]
        if True not in answerchkbox:
            self.ids.ans_chkboxA.color=(1,0,0,1)
            self.ids.ans_chkboxB.color=(1,0,0,1)
            self.ids.ans_chkboxC.color=(1,0,0,1)
            self.ids.ans_chkboxD.color=(1,0,0,1)
            toast("部分項目未填")
            return
        if (answerchkbox.index(True)) == 0:
            answer = self.ids.questionA.text
        if (answerchkbox.index(True)) == 1:
            answer = self.ids.questionB.text
        if (answerchkbox.index(True)) == 2:
            answer = self.ids.questionC.text
        if (answerchkbox.index(True)) == 3:
            answer = self.ids.questionD.text
        self.ids.ans_chkboxA.color=[0.0, 0.0, 0.0, 0.54]
        self.ids.ans_chkboxB.color=[0.0, 0.0, 0.0, 0.54]
        self.ids.ans_chkboxC.color=[0.0, 0.0, 0.0, 0.54]
        self.ids.ans_chkboxD.color=[0.0, 0.0, 0.0, 0.54]
        
        if self.ids.apply_btn.text =="新增題目":                            
            my_data = {'question': self.ids.question.text,"type":self.ids.ctype.text, 'choice_ansA': self.ids.questionA.text, 'choice_ansB': self.ids.questionB.text, 'choice_ansC': self.ids.questionC.text, 'choice_ansD': self.ids.questionD.text,'choice_answer':answer,'choice_analysis':self.ids.choice_analysis.text}
            r = requests.post('http://120.96.63.79/oit_English/insertchoicequestion', data = my_data)
            data = r.json()
            self.dismiss()
            toast("新增題目完成")
            self.mainobject.loadquestion('["tb_choice","字彙測驗"]')     
        if self.ids.apply_btn.text =="編輯題目":                
            x = ast.literal_eval(self.choice_type)
            my_data = {'tb_name':x[0],'type':x[1],'id':self.choice_id,'question': self.ids.question.text ,'choice_ansA': self.ids.questionA.text, 'choice_ansB': self.ids.questionB.text, 'choice_ansC': self.ids.questionC.text, 'choice_ansD': self.ids.questionD.text,'choice_answer':answer,'choice_analysis':self.ids.choice_analysis.text}
            r = requests.post('http://120.96.63.79/oit_English/updatequestioninformation', data = my_data)
            data = r.json()
            self.dismiss()
            toast("編輯題目完成")
            self.mainobject.loadquestion(str(self.choice_type))  
        
class Leakwordpopup(Popup): #新增克漏字視窗
    title = StringProperty()
    color = StringProperty()
    answer = ObjectProperty()
    isedit  = BooleanProperty()
    choice_id = StringProperty()
    choice_type =StringProperty()
    mainobject =  ObjectProperty()
    questioncontent = ListProperty()
    def __init__(self, mainobject,isedit,title, **kwargs):
        super(Leakwordpopup, self).__init__(**kwargs)
        self.mainobject = mainobject
        self.set_description(title)
        self.isedit = isedit
        if self.isedit:
            choice_id=self.choice_id
            choice_type = self.choice_type
            self.ids.apply_qut.text = "確認編輯題目"
            my_data = {'type': '["tb_leakword_article","克漏字"]','id':str(self.choice_id)}
            r = requests.post('http://120.96.63.79/oit_English/getleakoptioncontent', data = my_data)
            data=r.json()
            for i in range(data['count']):
                self.add()
            self.ids.question_content.text = data['question']

            for i in range(len(self.questioncontent)):
                option  = [str(data['content'][i][0]),str(data['content'][i][1]),str(data['content'][i][2]),str(data['content'][i][3])]                
                if option.index(data['answer'][i]) ==0:
                    self.questioncontent[i][12].active = True 
                if option.index(data['answer'][i]) ==1:
                    self.questioncontent[i][14].active = True 
                if option.index(data['answer'][i]) ==2:
                    self.questioncontent[i][16].active = True 
                if option.index(data['answer'][i]) ==3:
                    self.questioncontent[i][18].active = True 
                
                self.questioncontent[i][5].text = str(data['content'][i][0])
                self.questioncontent[i][7].text = str(data['content'][i][1])
                self.questioncontent[i][9].text = str(data['content'][i][2])
                self.questioncontent[i][11].text = str(data['content'][i][2])


    def set_description(self, title):
        self.title = title
    def add(self):            
        self.temp = []
        new_btn = self.ids.apply_btn

        self.ids.layout.remove_widget(new_btn)

        del_btn = MDRaisedButton(text="刪除", size_hint_x=None, pos_hint={
            'center_x': 0.5, 'center_x': 0.5})
        del_btn.bind(on_press=lambda x: delquestion(self, x))

        number_btn = MDLabel(size_hint_x=None,
                             pos_hint={'center_x': 0.5, 'center_x': 0.5})
        self.ids.layout.add_widget(number_btn)
        self.ids.layout.add_widget(del_btn)

        la1 = GridLayout(rows=2, cols=4, spacing=dp(20), size_hint_y=None)
        laform1 = [MDLabel(text="選項A", size_hint_x=None), MDTextField(size_hint_x=0.7), MDLabel(text="選項B", size_hint_x=None), MDTextField(
            size_hint_x=0.7), MDLabel(text="選項C", size_hint_x=None), MDTextField(size_hint_x=0.7), MDLabel(text="選項D", size_hint_x=None), MDTextField(size_hint_x=0.7)]

        lb1 = GridLayout(rows=1, cols=8, spacing=dp(20), size_hint_y=None)
        lbform1 = [MDCheckbox(size_hint=(None, None)), MDLabel(text="選項A"), MDCheckbox(size_hint=(None, None)), MDLabel(
            text="選項B"), MDCheckbox(size_hint=(None, None)), MDLabel(text="選項C"), MDCheckbox(size_hint=(None, None)), MDLabel(text="選項D")]

        self.temp.append(del_btn)
        self.temp.append(number_btn)
        self.temp.append(la1)
        self.temp.append(lb1)
        for i in laform1:
            self.temp.append(i)
        for i in lbform1:
            self.temp.append(i)

        for i in self.temp[4:12]:
            la1.add_widget(i)
        for i in self.temp[12:]:
            lb1.add_widget(i)

        self.ids.layout.add_widget(la1)
        self.ids.layout.add_widget(lb1)
        self.questioncontent.append(self.temp)
        self.ids.layout.add_widget(new_btn)  # 在Layout最後面擺上新增題目按鈕

        for i in range(len(self.questioncontent)):
            self.questioncontent[i][0].id = str(i+1)
            self.questioncontent[i][1].text = "第"+str(i+1)+"題 "
            self.questioncontent[i][0].id = str(i+1)
            self.questioncontent[i][1].text = "第"+str(i+1)+"題 "
            self.questioncontent[i][12].group = str(i+1)
            self.questioncontent[i][14].group = str(i+1)
            self.questioncontent[i][16].group = str(i+1)
            self.questioncontent[i][18].group = str(i+1)

        def delquestion(self, form):
            for i in self.questioncontent[int(form.id)-1]:
                self.ids.layout.remove_widget(i)
            del self.questioncontent[int(form.id)-1]
            for i in range(len(self.questioncontent)):
                self.questioncontent[i][0].id = str(i+1)
                self.questioncontent[i][1].text = "第"+str(i+1)+"題 "
                self.questioncontent[i][12].group = str(i+1)
                self.questioncontent[i][14].group = str(i+1)
                self.questioncontent[i][16].group = str(i+1)
                self.questioncontent[i][18].group = str(i+1)

    def save(self):    
        iserror = False
        if self.ids.question_content.text == "":
            self.ids.question_content.line_color_normal = (1, 0, 0, 1)
            iserror = True
        else:
            self.ids.question_content.line_color_normal = (0, 0, 0, 0.12)
            iserror = False
        for i in range(len(self.questioncontent)):
            if self.questioncontent[i][5].text == "":
                self.questioncontent[i][5].line_color_normal = (1, 0, 0, 1)
                iserror = True
            else:
                self.questioncontent[i][5].line_color_normal = (0, 0, 0, 0.12)
                iserror = False

            if self.questioncontent[i][7].text == "":
                self.questioncontent[i][7].line_color_normal = (1, 0, 0, 1)
                iserror = True
            else:
                self.questioncontent[i][7].line_color_normal = (0, 0, 0, 0.12)
                iserror = False

            if self.questioncontent[i][9].text == "":
                self.questioncontent[i][9].line_color_normal = (1, 0, 0, 1)
                iserror = True
            else:
                self.questioncontent[i][9].line_color_normal = (0, 0, 0, 0.12)
                iserror = False

            if self.questioncontent[i][11].text == "":
                self.questioncontent[i][11].line_color_normal = (1, 0, 0, 1)
                iserror = True
            else:
                self.questioncontent[i][11].line_color_normal = (0, 0, 0, 0.12)
                iserror = False

            if (self.questioncontent[i][12].active == False and self.questioncontent[i][14].active == False and self.questioncontent[i][16].active == False and self.questioncontent[i][18].active == False):
                self.questioncontent[i][12].color = (1, 0, 0, 1)
                self.questioncontent[i][14].color = (1, 0, 0, 1)
                self.questioncontent[i][16].color = (1, 0, 0, 1)
                self.questioncontent[i][18].color = (1, 0, 0, 1)
                iserror = True
            else:
                if (self.questioncontent[i][12].active == False):
                    self.questioncontent[i][12].color = [0.0, 0.0, 0.0, 0.54]
                if (self.questioncontent[i][14].active == False):
                    self.questioncontent[i][14].color = [0.0, 0.0, 0.0, 0.54]
                if (self.questioncontent[i][16].active == False):
                    self.questioncontent[i][16].color = [0.0, 0.0, 0.0, 0.54]
                if (self.questioncontent[i][18].active == False):
                    self.questioncontent[i][18].color = [0.0, 0.0, 0.0, 0.54]
                iserror = False

        if iserror:
            toast("檢查所有項目已填")
            return
        if len(self.questioncontent) == 0:
            toast("只要要有一題目")
            return
        
        if self.isedit:
            my_data={'type': str(self.choice_type),'id':str(self.choice_id),'article':str(self.ids.question_content.text)}
            r = requests.post('http://120.96.63.79/oit_English/updateleakarticle', data = my_data) #修改題目
            my_data={'type': '["tb_leakword_article","克漏字"]','id':str(self.choice_id)}
            r = requests.post('http://120.96.63.79/oit_English/deleteleakoption', data = my_data)
            
            for i in range(len(self.questioncontent)):
                if self.questioncontent[i][12].active:
                    ans = self.questioncontent[i][5].text  # 答案
                if self.questioncontent[i][14].active:
                    ans = self.questioncontent[i][7].text
                if self.questioncontent[i][16].active:
                    ans = self.questioncontent[i][9].text
                if self.questioncontent[i][18].active:
                    ans = self.questioncontent[i][11].text  
                my_data={'type': 'test',  'article_index':str(self.choice_id),'leakword_option_ansA':self.questioncontent[i][5].text, 'leakword_option_ansB':self.questioncontent[i][7].text, 'leakword_option_ansC':self.questioncontent[i][9].text, 'leakword_option_ansD':self.questioncontent[i][11].text,'leakword_option_answer':ans}
                r = requests.post('http://120.96.63.79/oit_English/insertleakoption', data = my_data) #修改題目
            
            self.dismiss()
            toast("編輯題目完成")
            self.mainobject.loadquestion(str(self.choice_type))  
        else: #新增題目
            my_data={'type': '克漏字','leakword_article_article':str(self.ids.question_content.text),'leakword_article_analysis':str(self.ids.question_analysis) }
            r = requests.post('http://120.96.63.79/oit_English/insertleakarticle', data = my_data)
            data=r.json()

            r = requests.get('http://120.96.63.79/oit_English/getleakarticleindex')
            data=r.json()
            aritcleindex = data['articleindex']

            for i in range(len(self.questioncontent)):
                if self.questioncontent[i][12].active:
                    ans = self.questioncontent[i][5].text  # 答案
                if self.questioncontent[i][14].active:
                    ans = self.questioncontent[i][7].text
                if self.questioncontent[i][16].active:
                    ans = self.questioncontent[i][9].text
                if self.questioncontent[i][18].active:
                    ans = self.questioncontent[i][11].text  
                my_data={'type': 'test',  'article_index':str(aritcleindex),'leakword_option_ansA':self.questioncontent[i][5].text, 'leakword_option_ansB':self.questioncontent[i][7].text, 'leakword_option_ansC':self.questioncontent[i][9].text, 'leakword_option_ansD':self.questioncontent[i][11].text,'leakword_option_answer':ans}
                r = requests.post('http://120.96.63.79/oit_English/insertleakoption', data = my_data)
                data=r.json()
            self.dismiss()
            toast("編輯題目完成")
            print(self.choice_type)
            
            #self.mainobject.loadquestion(str(self.choice_type))  
        
class SignupPopup(Popup): #註冊彈出視窗
    title = StringProperty()
    color = StringProperty()
    answer = ObjectProperty()
    textinputtext = StringProperty()
    def __init__(self, title, **kwargs):
        super(SignupPopup, self).__init__(**kwargs)
        self.set_description(title)
        self.textinputtext = 'palim'

    def set_description(self, title):
        self.title = title 
    


if __name__ == '__main__':
    KitchenSink().run()


