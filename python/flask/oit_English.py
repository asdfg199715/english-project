# -*- coding: utf-8 -*-
# https://ithelp.ithome.com.tw/articles/10202886

from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort

# 我把api的處理都放在database/database.py 用來建立連線
from database.database import app as application

# 我把api的處理都放在database/api.py裡面
from database.api import logingetuserdata
from database.api import creatuser
from database.api import getvacublarytest
from database.api import insertword
from database.api import geterrword
from database.api import getindex
from database.api import inserthistroy
from database.api import getworddata
from database.api import getwordhistroycontent

from database.api import getgpssword
from database.api import insertwordgps
from database.api import updatewordgps
from database.api import delwordgps

from database.api import getwordinformation
# app = Flask(__name__)#database.database裡面有定義app了，再寫會錯誤

from database.api import insertquesstionchoice

from database.api import getquestion
from database.api import delquestion

from database.api import getquestinformation

from database.api import updatequestionchoice

from database.api import getarticleleakindex
from database.api import insertleakwordarticle
from database.api import insertleakwordoption
from database.api import getoptioncontentleak #取得選項內容

from database.api import updatearticleleak #更新文章
from database.api import deleteoptionleak #刪除選項

from database.api import creatmatplotlib
# 這就是app名稱
app = application
api = Api(app)

# 一定要設定
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# 用來接傳進來的變數用的
parser = reqparse.RequestParser()
# Post或Get的變數名稱，還有他的說明(help)

parser.add_argument('id', help='user id')
parser.add_argument('name', help='user name')
parser.add_argument('password', help='user password')
parser.add_argument('email', help='user email')
parser.add_argument('phone', help='user phone')


parser.add_argument('count', help='Question Count')
parser.add_argument('type', help='Question type')
parser.add_argument('range', help='Question range')

parser.add_argument('account', help='User account')
parser.add_argument('password', help='User password')

parser.add_argument('userid', help='inserterrorworrd data')
parser.add_argument('range', help='inserterrorworrd range')
parser.add_argument('word', help='inserterrorworrd word')


parser.add_argument('userid', help='getlastindex word')


parser.add_argument('index', help='inserthistroy word')
parser.add_argument('range', help='inserthistroy word')
parser.add_argument('type', help='inserthistroy type')
parser.add_argument('date', help='inserthistroy date')
parser.add_argument('score', help='inserthistroy score')
parser.add_argument('cscore', help='inserthistroy cscore')

parser.add_argument('trange', help='')


parser.add_argument("word", help="word")
parser.add_argument("ctype", help="ctype")
parser.add_argument("etype", help="etype")
parser.add_argument("chinese", help="chinese")
parser.add_argument("pos", help="pos")
parser.add_argument("example", help="example")
parser.add_argument("cexample", help="cexample")


parser.add_argument("num", help="updatenumber")
parser.add_argument("errortag", help="errortag")

parser.add_argument("question", help="choicequestions")
parser.add_argument("choice_ansA", help="choicequestions")
parser.add_argument("choice_ansB", help="choicequestions")
parser.add_argument("choice_ansC", help="choicequestions")
parser.add_argument("choice_ansD", help="choicequestions")
parser.add_argument("choice_answer",help="choicequestions")
parser.add_argument("type",help="choicequestions")
parser.add_argument("choice_analysis",help="choice_analysis")

parser.add_argument("questiontype",help="questiontype")
parser.add_argument("tb_name",help="tb_name")

parser.add_argument("leakword_article_article",help="leakword_article")
parser.add_argument("leakword_option_ansA",help="leakword_option")
parser.add_argument("leakword_option_ansB",help="leakword_option")
parser.add_argument("leakword_option_ansC",help="leakword_option")
parser.add_argument("leakword_option_ansD",help="leakword_option")
parser.add_argument("leakword_option_answer",help="leakword_option")
parser.add_argument("article_index", help = "article_index")
parser.add_argument("leakword_article_analysis",help='leakword_article_analysis')
parser.add_argument("article",help="article")

parser.add_argument("user",help="user")

# API
'''
class signin(Resource):
    def get(self):
        return {"returnd": "Not Found"}
        # retrun 404

    def post(self):
        args = parser.parse_args()  # 接值

        account = args['account']  # 取得名為account的傳入值

        password = args['password']

        if(account != None and password != None):
            try:
                # 呼叫自己寫的函數來處理，Class login放在database/api裡面
                r = login(account, password)

                # r會接到傳回來的東西
                if(not(r)):
                    # 傳回一包json，包含自定義的變數和內容
                    return {"Status": "Failed", "Return": "Member Not Found."}

                elif(r == "Login failed"):
                    return {"Status": "Failed", "Return": "Not accept to login."}

                else:
                    return {"Status": "Success", "Name": r}

            except Exception as e:
                return {"Status": "Failed", "Return": str(e)}
'''


class inserterrorword(Resource):
    def post(self):
        args = parser.parse_args()
        userid = args['userid']
        index = args['index']
        range = args['range']
        word = args['word']
        errortag = args['errortag']
        r = insertword(userid, index, range, word, errortag)
        return {"Status": r}


class Login(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            account = args['account']
            password = args['password']
            r = logingetuserdata(account, password)
            if(not(r)):
                return {"Status": "LoginFailed"}
            else:
                return {"Status": "Success", "id": r[0], "name": r[1], "mail": r[2], "phone": r[3]}
        except Exception as e:
            return {"Status": "Failed", "Return": str(e)}


class creatcount(Resource):
    def post(self):
        args = parser.parse_args()
        id = args['id']
        name = args['name']
        password = args['password']
        email = args['email']
        phone = args['phone']
        r = creatuser(id, name, password, email, phone)
        return {"message": r}


class getvactest(Resource):
    def post(self):
        args = parser.parse_args()
        count = args['count']
        qtype = args['type']
        range = args['range']
        r = getvacublarytest(count, qtype, range)
        return {"questionid": r[0], "question": r[1], "option": r[2], "answer": r[3]}


class getoftenword(Resource):
    def post(self):
        args = parser.parse_args()
        range = args['range']
        userid = args['userid']
        r = geterrword(range, userid)
        return {"Status": "success", "content": r}


class getlastwordtestindex(Resource):
    def post(self):
        args = parser.parse_args()
        userid = args['userid']
        r = getindex(userid)
        if r == None:
            return {"Status": "success", "content": 1}
        else:
            return {"Status": "success", "content": int(r)+1}


class createhistroy(Resource):
    def post(self):
        args = parser.parse_args()
        userid = args['userid']
        index = args['index']
        range = args['range']
        type = args['type']
        date = args['date']
        score = args['score']
        cscore = args['cscore']
        r = inserthistroy(userid, index, range, type, date, score, cscore)
        return r


class getwordtesthistroy(Resource):
    def post(self):
        args = parser.parse_args()
        userid = args['userid']
        r = getworddata(userid)
        return r


class getwordtesthistroycontent(Resource):
    def post(self):
        args = parser.parse_args()
        userid = args['userid']
        trange = args['range']
        index = args['index']

        r = getwordhistroycontent(userid, index, trange)
        return {"word": r[0], "gpswordetype": r[1], "gpswordindex": r[2], 'errortage': r[3]}


class gpswordadmingetword(Resource):  # 單字庫取得單字
    def post(self):
        args = parser.parse_args()
        type = args['type']
        word = args['word']
        r = getgpssword(type, word)
        return {"gps_class_number": r[0], 'gps_class_etype': r[1], 'gps_class_ctype': r[2], 'gps_class_word': r[3], 'gps_class_chinese': r[4], 'gps_class_pos': r[5], 'gps_class_example': r[6], 'gps_class_cexample': r[7]}


class gpswordadmininsertword(Resource):  # GPS新增單字
    def post(self):
        args = parser.parse_args()
        word = args['word']
        ctype = args['ctype']
        etype = args['etype']
        chinese = args['chinese']
        pos = args['pos']
        example = args['example']
        cexample = args['cexample']
        r = insertwordgps(word, ctype, etype, chinese, pos, example, cexample)
        return {"message": r}


class gpswordadminupdateword(Resource):  # GPS更新單字
    def post(self):
        args = parser.parse_args()
        num = args['num']
        word = args['word']
        ctype = args['ctype']
        etype = args['etype']
        chinese = args['chinese']
        pos = args['pos']
        example = args['example']
        cexample = args['cexample']
        r = updatewordgps(num, word, ctype, etype,
                          chinese, pos, example, cexample)
        return {"message": r}


class gpswordadmindelword(Resource):  # 刪除單字資訊
    def post(self):
        args = parser.parse_args()
        num = args['num']
        etype = args['etype']
        r = delwordgps(num, etype)
        return {"state": r}


class getgpswordinformation(Resource):  # 取得單一單字資訊
    def post(self):
        args = parser.parse_args()
        num = args['num']
        etype = args['etype']
        r = getwordinformation(num, etype)
        if r == "null":
            return{"gps_class_number": "null"}
        else:
            return {"gps_class_number": r[0], "gps_class_etype": r[1], "gps_class_ctype": r[2], "gps_class_word": r[3], "gps_class_chinese": r[4], "gps_class_pos": r[5], "gps_class_example": r[6], "gps_class_cexample": r[7]}


class getquestionlist(Resource):
    def post(self):
        args = parser.parse_args()
        questiontype = args['questiontype']
        r = getquestion(questiontype)
        return{"question_id":r[0],"questioncontent":r[1],"questionlevel":r[2]}


class insertchoicequestion(Resource):
    def post(self):
        args = parser.parse_args()
        type = args['type']
        question = args['question']
        choice_ansA = args['choice_ansA']
        choice_ansB = args['choice_ansB']
        choice_ansC = args['choice_ansC']
        choice_ansD = args['choice_ansD']
        choice_answer = args['choice_answer']
        choice_analysis = args['choice_analysis']
        r = insertquesstionchoice(type,question, choice_ansA,choice_ansB,choice_ansC,choice_ansD,choice_answer,choice_analysis)
        if r == "null":
            return{"gps_class_number": "null"}
        else:
            return{"count": r}

class delchoicequestion(Resource):
    def post(self):
        args = parser.parse_args()
        tb_name=args['tb_name']
        type = args['type']
        id = args['id']
        r = delquestion(tb_name,type,id)
        if r == "null":
            return{"gps_class_number": "null"}
        else:
            return{"count": r}

class getquestioninformation(Resource):
    def post(self):
        args = parser.parse_args()
        tb_name=args['tb_name']
        type = args['type']
        id = args['id']
        r = getquestinformation(tb_name,type,id)
        if r == "null":
            return{"choice_question": "null"}
        else:
            return{"choice_question": r[0],'choice_ansA':r[1],'choice_ansB':r[2],'choice_ansC':r[3],'choice_ansD':r[4],"choice_answer":r[5],'choice_analysis':r[6],'choice_type':r[7]}

class updatequestioninformation(Resource): 
    def post(self):
        args = parser.parse_args()
        tb_name=args['tb_name']
        type = args['type']
        id = args['id']
        question = args['question']
        choice_ansA = args['choice_ansA']
        choice_ansB = args['choice_ansB']
        choice_ansC = args['choice_ansC']
        choice_ansD = args['choice_ansD']
        choice_answer = args['choice_answer']
        choice_analysis = args['choice_analysis']
        r = updatequestionchoice(tb_name,type,id,question, choice_ansA,choice_ansB,choice_ansC,choice_ansD,choice_answer,choice_analysis)
        if r == "null":
            return{"gps_class_number": "null"}
        else:
            return{"count": r}


class insertleakarticle(Resource): #新增克漏字文章
    def post(self):
        args = parser.parse_args()
        type = args['type']
        leakword_article_article = args['leakword_article_article']
        leakword_article_analysis=args['leakword_article_analysis']
        r = insertleakwordarticle(type,leakword_article_article,leakword_article_analysis)
        if r == "null":
            return{"gps_class_number": "null"}
        else:
            return{"count": r}

class getleakarticleindex(Resource): #新增克漏字文章
    def get(self):
        r = getarticleleakindex()
        if r == "0":
            return{"articleindex": "1"}
        else:
            return{"articleindex": r}

class insertleakoption(Resource): #新增克漏字選項
    def post(self):
        args = parser.parse_args()
        type = args['type']
        leakword_option_ansA = args['leakword_option_ansA']
        leakword_option_ansB = args['leakword_option_ansB']
        leakword_option_ansC = args['leakword_option_ansC']
        leakword_option_ansD = args['leakword_option_ansD']
        leakword_option_answer = args['leakword_option_answer']
        article_index= args['article_index']
        r = insertleakwordoption(type,article_index,leakword_option_ansA,leakword_option_ansB,leakword_option_ansC,leakword_option_ansD,leakword_option_answer)
        if r == "null":
            return{"gps_class_number": "null"}
        else:
            return{"count": r}

class getleakoptioncount(Resource): 
    def post(self):
        args = parser.parse_args()
        type = args['type']
        id = args['id']
        r = getoptioncountleak(type,id)
        if r == "null":
            return{"count": "null"}
        else:
            return{"count": r}

class getleakoptioncontent(Resource):
    def post(self):
        args = parser.parse_args()
        type = args['type']
        id = args['id']
        r = getoptioncontentleak(type,id)
        if r == "null":
            return{"count": r[0],'content':r[1],'question':r[2],'answer':r[3]}
        else:
            return{"count": r[0],'content':r[1],'question':r[2],'answer':r[3]}


class updateleakarticle(Resource):
    def post(self):
        args = parser.parse_args()
        type = args['type']
        id = args['id']
        article = args['article']
        r = updatearticleleak(type,id,article)
        if r == "null":
            return{"count": "null"}
        else:
            return{"count": r}

class deleteleakoption(Resource):
    def post(self):
        args = parser.parse_args()
        type = args['type']
        id = args['id']
        r = deleteoptionleak(type,id)
        if r == "null":
            return{"count": "null"}
        else:
            return{"count": r}

class getmatplotlib(Resource):
    def post(self):
        args = parser.parse_args()
        user = args['user']
        r=creatmatplotlib(user)
        if r!= "null":
            return{"word": r[0],'range':r[1]}

# api.add_resource(signin, '/oit_English/signin')  # API的網址，連到Class名稱(這裡是signin)
api.add_resource(Login, '/oit_English/Login')  # 登入
api.add_resource(creatcount, '/oit_English/creatcount')  # 註冊帳號
api.add_resource(inserterrorword, '/oit_English/inserterrorword')  # 測驗後插入錯誤單字
api.add_resource(getvactest, '/oit_English/getvactest')  # 取得單字
api.add_resource(getoftenword, '/oit_English/getoftenword')  # 取得錯誤單字


api.add_resource(getlastwordtestindex,
                 '/oit_English/getlastindex')  # 取得最後一次作答次數

api.add_resource(createhistroy,
                 '/oit_English/createhistroy')  # 新增作答紀錄

api.add_resource(getwordtesthistroy,
                 '/oit_English/getwordtesthistroy')  # 取得作答紀錄列表

api.add_resource(getwordtesthistroycontent,
                 '/oit_English/getwordtesthistroycontent')  # 取得作答紀錄列表


# 1080826
api.add_resource(gpswordadmingetword,
                 '/oit_English/gpswordadmingetword')  # 取得GPS單字量列表

api.add_resource(gpswordadmininsertword,
                 '/oit_English/gpswordadmininsertword')  # 插入GPS單字

api.add_resource(gpswordadminupdateword,
                 '/oit_English/gpswordadminupdateword')  # 更新GPS單字

api.add_resource(gpswordadmindelword,
                 '/oit_English/gpswordadmindelword')  # 刪除GPS單字

api.add_resource(getgpswordinformation,
                 '/oit_English/getgpswordinformation')  # 取得單一單字資訊



api.add_resource(getquestionlist,
                 '/oit_English/getquestionlist') #取得題目列表
                 
api.add_resource(insertchoicequestion, #新增選擇題
                 '/oit_English/insertchoicequestion')

api.add_resource(delchoicequestion, #刪除選擇題
                '/oit_English/delchoicequestion')

api.add_resource(getquestioninformation, #取得題目資訊 用來編輯題目
                '/oit_English/getquestioninformation')


api.add_resource(updatequestioninformation, #取得題目資訊 用來編輯題目
                '/oit_English/updatequestioninformation')

api.add_resource(insertleakarticle, #新增克漏字文章
                 '/oit_English/insertleakarticle')
api.add_resource(getleakarticleindex, #新增克漏字文章
                 '/oit_English/getleakarticleindex')
api.add_resource(insertleakoption, #新增克漏字選項
                 '/oit_English/insertleakoption')

api.add_resource(getleakoptioncontent, #取得選項內容
                '/oit_English/getleakoptioncontent')
api.add_resource(updateleakarticle,
                 '/oit_English/updateleakarticle') #更新克漏字文章
api.add_resource(deleteleakoption,
                '/oit_English/deleteleakoption') #刪除文章選項ID

api.add_resource(getmatplotlib,
                '/oit_English/getmatplotlib') #產生關聯圖
if __name__ == '__main__':
    app.run(debug=True)
