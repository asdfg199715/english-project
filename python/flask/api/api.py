# -*- coding: utf-8 -*-
# https://myapollo.com.tw/2016/09/28/python-sqlalchemy-orm-1/
import datetime
import sys
import json
from sqlalchemy import create_engine, and_, exc, Integer, ForeignKey, String, Column
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import case
from sqlalchemy import func
from sqlalchemy import desc
import ast
import random
# 從database/model引入所有class，包含與DB的映對(Mapping)關係
from database.model import *

# 從database/database把db傳過來
from database.database import db

db.create_all()  # db建立連線

# 用來登入的API的處理(一個Class)，這邊login會傳入兩個變數(帳號，密碼)

'''
def login(ac, pw):
    try:
        # table.query.filet(條件) <=相當於Select * Where 條件
        # 這邊Member_account"不是"資料庫裡的名稱，是在Database/Model中定義的
        q = tb_member.query.filter(and_(tb_member.Member_account == str(
            ac), tb_member.Member_password == str(pw))).first()

        return q.Member_name

    except Exception as e:
        print("error occur: %s" % (e))
'''


def creatuser(id, name, password, email, phone):
    try:
        admin = tb_user(id, name, password, email, phone)
        db.session.add(admin)
        db.session.commit()
        return "success"
    except Exception as e:
        return e


def logingetuserdata(ac, pw):
    try:
        q = tb_user.query.filter(and_(tb_user.user_id == str(
            ac), tb_user.user_password == str(pw))).first()
        return q.user_id, q.user_name, q.user_email, q.user_phone
    except Exception as e:
        print("error occur: %s" % (e))


def insertword(id, index, range, engword):
    import ast
    wordlist = ast.literal_eval(engword)
    try:
        for i in wordlist:
            q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == int(
                i), tb_Vocabulary.Vocabulary_range == str(range))).first()
            word = tb_error_word(id, index, range, q.Vocabulary_Word)
            db.session.add(word)
            db.session.commit()
    except Exception as e:
        print("error occur: %s" % (e))


def geterrword(rg, user):  # 取得常錯單字
    error = []
    errornum = []
    errorinex = {}
    try:
        for r in db.session.query(tb_error_word.error_word).group_by(tb_error_word.error_word).having(func.count(tb_error_word.error_word) > 1).filter(tb_error_word.error_id == str(user)).filter(tb_error_word.error_question_range == str(rg)).order_by(func.count(tb_error_word.error_word).desc()):
            error.append(r.error_word)
        for i in error:
            errornum.append(db.session.query(
                tb_error_word.error_word).filter(tb_error_word.error_id == "tet").filter(tb_error_word.error_question_range == str(rg)).filter(tb_error_word.error_word == str(i)).count())

        if len(error) > 15:
            del errornum[15:]
            del errornum[15:]
        errorinex["word"] = error
        errorinex["count"] = errornum

        return errorinex
    except Exception as e:
        print("error occur: %s" % (e))


def getvacublarytest(co, ty, rg):  # co 題目數量 ty 類型 rg範圍(全民英檢中級...)
    if (ty == "Wordtest" or ty == "Lisitingtest"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}  # 選項
            answer = {}  # 解答
            while True:
                rd = random.randint(1, 1087)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break
            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                question.append(q.Vocabulary_Word)

            for i in range(len(questionid)):
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()

                answer[str(i)] = str(q.Vocabulary_Chinese)
            for j in range(int(co)):
                tempid = []
                while True:
                    rd = random.randint(1, 1087)
                    if rd not in tempid:
                        tempid.append(rd)
                    if len(tempid) == 3:
                        break
                tempid.append(questionid[j])
                temp = []
                for i in tempid:
                    q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                        i), tb_Vocabulary.Vocabulary_range == str(rg))).first()

                    temp.append(q.Vocabulary_Chinese)
                random.shuffle(temp)
                option[str(j)] = temp

            return(questionid, question, option, answer)
            # return q.Voca_Word

        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "SingleWordtest"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}
            answer = {}  # 解答

            while True:
                rd = random.randint(1, 1087)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break

            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                question.append(q.Vocabulary_Chinese)
            for i in range(len(questionid)):
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                answer[str(i)] = str(q.Vocabulary_Word)

            return(questionid, question, option, answer)

        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "Rearrangement"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}
            answer = {}  # 解答

            while True:
                rd = random.randint(1, 1087)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break

            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                temp = q.Vocabulary_Simple.split(" ")
                random.shuffle(temp)
                new = " ".join(temp)
                question.append(new)

            for i in range(len(questionid)):
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                answer[str(i)] = str(q.Vocabulary_Simple)

            return(questionid, question, option, answer)
        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "Sentencetest"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}
            answer = {}  # 解答
            while True:
                rd = random.randint(1, 1087)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break
            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                question.append(q.Vocabulary_CSimple)

            for i in range(len(question)):
                temp = []
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                answer[str(i)] = q.Vocabulary_Simple
                temp.append(q.Vocabulary_Simple)
                temp.append(q.Vocabulary_Word)

                option[str(i)] = temp
            return(questionid, question, option, answer)
        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "GpsWordtest"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}
            answer = {}  # 解答
            return(questionid, ty, option, rg)
        except Exception as e:
            print("error occur: %s" % (e))


def getindex(id):
    try:
        q = tb_wordtesthistroy.query.filter(tb_wordtesthistroy.wordtest_userid == str(
            id)).order_by(tb_wordtesthistroy.wordtest_id.desc()).first()
        return q.wordtest_id
    except Exception as e:
        print("error occur: %s" % (e))


def inserthistroy(userid, index, range, type, date, score, cscore):  # 插入歷史紀錄
    try:
        data = tb_wordtesthistroy(
            userid, index, range, type, date, score, cscore)
        db.session.add(data)
        db.session.commit()
        return "success"
    except Exception as e:
        print("error occur: %s" % (e))


def getworddata(userid):
    data = []
    try:
        for r in db.session.query(tb_wordtesthistroy).filter(tb_wordtesthistroy.wordtest_userid == str(userid)).order_by(tb_wordtesthistroy.wordtest_id.desc()):
            temp = []
            temp.append(r.wordtest_id)
            temp.append(r.wordtest_range)
            temp.append(r.wordtest_type)
            temp.append(r.wordtest_score)
            temp.append(r.wordtest_date)
            data.append(temp)
        return data

    except Exception as e:
        print("error occur: %s" % (e))


def getwordhistroycontent(userid, index, trange):
    data = []
    try:
        for r in db.session.query(tb_error_word).filter(tb_error_word.error_id == str(userid)).filter(tb_error_word.error_anwerindex == str(index)).filter(tb_error_word.error_question_range == str(trange)):
            data.append(r.error_word)
        return data
    except Exception as e:
        print("error occur: %s" % (e))
