# -*- coding: utf-8 -*-
from database.database import db

# 自己設定的名稱，可以跟Database的Table不同


class tb_user(db.Model):
    __tablename__ = 'tb_user'

    user_id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.VARCHAR(5))
    user_password = db.Column(db.VARCHAR(20))
    user_email = db.Column(db.VARCHAR(50))
    user_phone = db.Column(db.VARCHAR(10))

    def __init__(self, user_id, user_name, user_password, user_email, user_phone):
        self.user_id = user_id
        self.user_name = user_name
        self.user_password = user_password
        self.user_email = user_email
        self.user_phone = user_phone

    def get_id(self):
        return self.user_id


class tb_Vocabulary(db.Model):
    __tablename__ = 'tb_Vocabulary'

    Vocabulary_ID = db.Column(db.Integer, primary_key=True)
    Vocabulary_range = db.Column(db.Text, primary_key=True)
    Vocabulary_Word = db.Column(db.Text)
    Vocabulary_Chinese = db.Column(db.Text)
    Vocabulary_Part = db.Column(db.Text)
    Vocabulary_Simple = db.Column(db.Text)
    Vocabulary_CSimple = db.Column(db.Text)

    def __init__(self, Vocabulary_ID, Vocabulary_range, Vocabulary_Word, Vocabulary_Chinese, Vocabulary_Part, Vocabulary_Simple, Vocabulary_CSimple):
        self.Vocabulary_ID = Vocabulary_ID
        self.Vocabulary_range = Vocabulary_range
        self.Vocabulary_Word = Vocabulary_Word
        self.Vocabulary_Chinese = Vocabulary_Chinese
        self.Vocabulary_Part = Vocabulary_Part
        self.Vocabulary_Simple = Vocabulary_Simple
        self.Vocabulary_CSimple = Vocabulary_CSimple


class tb_testhistroy(db.Model):
    __tablename__ = 'tb_testhistroy'

    user_id = db.Column(db.Integer, primary_key=True)
    user_anwerindex = db.Column(db.Integer, primary_key=True)
    histroy_question_range = db.Column(db.Text, primary_key=True)
    histroy_word = db.Column(db.Text, primary_key=True)
    histroy_error_tag = db.Column(db.Boolean)

    def __init__(self, user_id, user_anwerindex, histroy_question_range, histroy_word, histroy_error_tag	):
        self.user_id = user_id
        self.user_anwerindex = user_anwerindex
        self.histroy_question_range = histroy_question_range
        self.histroy_word = histroy_word
        self.histroy_error_tag = histroy_error_tag


class tb_wordtesthistroy(db.Model):
    __tablename__ = 'tb_wordtesthistroy'

    wordtest_userid = db.Column(db.Integer, primary_key=True)
    wordtest_id = db.Column(db.Integer, primary_key=True)
    wordtest_range = db.Column(db.Text)
    wordtest_type = db.Column(db.Text)
    wordtest_date = db.Column(db.Text)
    wordtest_score = db.Column(db.Text)
    wordtest_cscore = db.Column(db.Text)

    def __init__(self, wordtest_userid, wordtest_id, wordtest_range, wordtest_type, wordtest_date, wordtest_score, wordtest_cscore):
        self.wordtest_userid = wordtest_userid
        self.wordtest_id = wordtest_id
        self.wordtest_range = wordtest_range
        self.wordtest_type = wordtest_type
        self.wordtest_date = wordtest_date
        self.wordtest_score = wordtest_score
        self.wordtest_cscore = wordtest_cscore


class tb_gps_class(db.Model):
    __tablename__ = 'tb_gps_class'

    gps_class_number = db.Column(db.Integer, primary_key=True)
    gps_class_etype = db.Column(db.Text, primary_key=True)
    gps_class_ctype = db.Column(db.Text)
    gps_class_word = db.Column(db.Text)
    gps_class_chinese = db.Column(db.Text)
    gps_class_pos = db.Column(db.Text)
    gps_class_example = db.Column(db.Text)
    gps_class_cexample = db.Column(db.Text)

    def __init__(self, gps_class_number, gps_class_etype, gps_class_ctype, gps_class_word, gps_class_chinese, gps_class_pos, gps_class_example, gps_class_cexample):
        self.gps_class_number = gps_class_number
        self.gps_class_etype = gps_class_etype
        self.gps_class_ctype = gps_class_ctype
        self.gps_class_word = gps_class_word
        self.gps_class_chinese = gps_class_chinese
        self.gps_class_pos = gps_class_pos
        self.gps_class_example = gps_class_example
        self.gps_class_cexample = gps_class_cexample


class tb_choice(db.Model):
    __tablename__ = 'tb_choice'

    choice_id = db.Column(db.Integer, primary_key=True)
    choice_subject = db.Column(db.Text, primary_key=True)
    choice_type = db.Column(db.Text)
    choice_level = db.Column(db.Integer)
    choice_anscount = db.Column(db.Integer)
    choice_anscorrect = db.Column(db.Integer)
    choice_anserror = db.Column(db.Integer)
    choice_question = db.Column(db.Integer)
    choice_ansA = db.Column(db.Text)
    choice_ansB = db.Column(db.Text)
    choice_ansC = db.Column(db.Text)
    choice_ansD = db.Column(db.Text)
    choice_answer = db.Column(db.Text)
    choice_author = db.Column(db.Text)
    choice_public = db.Column(db.Integer)
    choice_analysis = db.Column(db.Text)

    def __init__(self, choice_id, choice_subject, choice_type, choice_level, choice_anscount, choice_anscorrect, choice_anserror, choice_question, choice_ansA, choice_ansB, choice_ansC, choice_ansD, choice_answer, choice_author, choice_public, choice_analysis):
        self.choice_id = choice_id
        self.choice_subject = choice_subject
        self.choice_type = choice_type
        self.choice_level = choice_level
        self.choice_anscount = choice_anscount
        self.choice_anscorrect = choice_anscorrect
        self.choice_anserror = choice_anserror
        self.choice_question = choice_question
        self.choice_ansA = choice_ansA
        self.choice_ansB = choice_ansB
        self.choice_ansC = choice_ansC
        self.choice_ansD = choice_ansD
        self.choice_answer = choice_answer
        self.choice_author = choice_author
        self.choice_public = choice_public
        self.choice_analysis = choice_analysis


class tb_leakword_article(db.Model):
    __tablename__ = 'tb_leakword_article'

    leakword_article_id = db.Column(db.Integer, primary_key=True)
    leakword_article_level = db.Column(db.Integer)
    leakword_article_subject = db.Column(db.Text)
    leakword_article_type = db.Column(db.Text)
    leakword_article_anscount = db.Column(db.Integer)
    leakword_article_anscorrect = db.Column(db.Integer)
    leakword_article_anserror = db.Column(db.Integer)
    leakword_article_article = db.Column(db.Text)
    leakword_article_author = db.Column(db.Text)
    leakword_article_public = db.Column(db.Integer)
    leakword_article_analysis = db.Column(db.Text)

    def __init__(self, leakword_article_id, leakword_article_level, leakword_article_subject, leakword_article_type, leakword_article_anscount, leakword_article_anscorrect, leakword_article_anserror, leakword_article_article, leakword_article_author, leakword_article_public, leakword_article_analysis):
        self.leakword_article_id = leakword_article_id
        self.leakword_article_level = leakword_article_level
        self.leakword_article_subject = leakword_article_subject
        self.leakword_article_type = leakword_article_type
        self.leakword_article_anscount = leakword_article_anscount
        self.leakword_article_anscorrect = leakword_article_anscorrect
        self.leakword_article_anserror = leakword_article_anserror
        self.leakword_article_article = leakword_article_article
        self.leakword_article_author = leakword_article_author
        self.leakword_article_public = leakword_article_public
        self.leakword_article_analysis = leakword_article_analysis


class tb_leakword_option(db.Model):
    __tablename__ = 'tb_leakword_option'

    leakword_option_quid = db.Column(db.Integer, primary_key=True)
    leakword_option_id = db.Column(db.Integer, primary_key=True)
    leakword_option_subject = db.Column(db.Text)
    leakword_option_type = db.Column(db.Text)
    leakword_option_level = db.Column(db.Integer)
    leakword_option_anscount = db.Column(db.Integer)
    leakword_option_anscorrect = db.Column(db.Integer)
    leakword_option_anserror = db.Column(db.Integer)
    leakword_option_ansA = db.Column(db.Text)
    leakword_option_ansB = db.Column(db.Text)
    leakword_option_ansC = db.Column(db.Text)
    leakword_option_ansD = db.Column(db.Text)
    leakword_option_answer = db.Column(db.Text)

    def __init__(self,  leakword_option_quid, leakword_option_id, leakword_option_subject, leakword_option_type, leakword_option_level, leakword_option_anscount, leakword_option_anscorrect, leakword_option_anserror, leakword_option_ansA, leakword_option_ansB, leakword_option_ansC, leakword_option_ansD, leakword_option_answer):
        self.leakword_option_quid = leakword_option_quid
        self.leakword_option_id = leakword_option_id
        self.leakword_option_subject = leakword_option_subject
        self.leakword_option_type = leakword_option_type
        self.leakword_option_level = leakword_option_level
        self.leakword_option_anscount = leakword_option_anscount
        self.leakword_option_anscorrect = leakword_option_anscorrect
        self.leakword_option_anserror = leakword_option_anserror
        self.leakword_option_ansA = leakword_option_ansA
        self.leakword_option_ansB = leakword_option_ansB
        self.leakword_option_ansC = leakword_option_ansC
        self.leakword_option_ansD = leakword_option_ansD
        self.leakword_option_answer = leakword_option_answer


class tb_read_article(db.Model):
    __tablename__ = 'tb_read_article'

    read_article_id = db.Column(db.Integer, primary_key=True)
    read_article_level = db.Column(db.Integer)
    read_article_sudject = db.Column(db.Text)
    read_article_type = db.Column(db.Text)
    read_article_anscount = db.Column(db.Integer)
    read_article_anscorrect = db.Column(db.Integer)
    read_article_anserror = db.Column(db.Integer)
    read_article_article = db.Column(db.Text)
    read_article_question = db.Column(db.Text)
    read_article_author = db.Column(db.Text)
    read_article_public = db.Column(db.Integer)
    read_article_analysis = db.Column(db.Text)

    def __init__(self, read_article_id, read_article_level, read_article_sudject, read_article_type, read_article_anscount, read_article_anscorrect, read_article_anserror, read_article_article, read_article_question, read_article_author, read_article_public, read_article_analysis):
        self.read_article_id = read_article_id
        self.read_article_level = read_article_level
        self.read_article_sudject = read_article_sudject
        self.read_article_type = read_article_type
        self.read_article_anscount = read_article_anscount
        self.read_article_anscorrect = read_article_anscorrect
        self.read_article_anserror = read_article_anserror
        self.read_article_article = read_article_article
        self.read_article_question = read_article_question
        self.read_article_author = read_article_author
        self.read_article_public = read_article_public
        self.read_article_analysis = read_article_analysis


class tb_read_question(db.Model):
    __tablename__ = 'tb_read_question'

    read_question_id = db.Column(db.Integer, primary_key=True)
    read_question_quid = db.Column(db.Integer, primary_key=True)
    read_question_sudject = db.Column(db.Text)
    read_question_type = db.Column(db.Text)
    read_question_level = db.Column(db.Integer)
    read_question_anscount = db.Column(db.Integer)
    read_question_anscorrect = db.Column(db.Integer)
    read_question_anserror = db.Column(db.Integer)
    read_question_ansA = db.Column(db.Text)
    read_question_ansB = db.Column(db.Text)
    read_question_ansC = db.Column(db.Text)
    read_question_ansD = db.Column(db.Text)
    read_question_answer = db.Column(db.Text)

    def __init__(self, read_question_id, read_question_quid, read_question_sudject, read_question_type, read_question_level, read_question_anscount, read_question_anscorrect, read_question_anserror, read_question_ansA, read_question_ansB, read_question_ansC, read_question_ansD, read_question_answer):
        self.read_question_id = read_question_id
        self.read_question_quid = read_question_quid
        self.read_question_sudject = read_question_sudject
        self.read_question_type = read_question_type
        self.read_question_level = read_question_level
        self.read_question_anscount = read_question_anscount
        self.read_question_anscorrect = read_question_anscorrect
        self.read_question_anserror = read_question_anserror
        self.read_question_ansA = read_question_ansA
        self.read_question_ansB = read_question_ansB
        self.read_question_ansC = read_question_ansC
        self.read_question_ansD = read_question_ansD
        self.read_question_answer = read_question_answer


class tb_reorganization_question(db.Model):
    __tablename__ = 'tb_reorganization_question'

    reorganization_question_id = db.Column(db.Integer, primary_key=True)
    reorganization_question_level = db.Column(db.Integer)
    reorganization_question_sudject = db.Column(db.Text)
    reorganization_question_type = db.Column(db.Text)
    reorganization_question_anscount = db.Column(db.Integer)
    reorganization_question_anscorrect = db.Column(db.Integer)
    reorganization_question_anserror = db.Column(db.Integer)
    reorganization_question_article = db.Column(db.Integer)
    reorganization_question_question = db.Column(db.Integer)
    reorganization_question_author = db.Column(db.Text)
    reorganization_question_public = db.Column(db.Integer)
    reorganization_question_analysis = db.Column(db.Text)

    def __init__(self, reorganization_question_id, reorganization_question_level, reorganization_question_sudject, reorganization_question_type, reorganization_question_anscount, reorganization_question_anscorrect, reorganization_question_anserror, reorganization_question_article, reorganization_question_question, reorganization_question_author, reorganization_question_public, reorganization_question_analysis):
        self.reorganization_question_id = reorganization_question_id
        self.reorganization_question_level = reorganization_question_level
        self.reorganization_question_sudject = reorganization_question_sudject
        self.reorganization_question_type = reorganization_question_type
        self.reorganization_question_anscount = reorganization_question_anscount
        self.reorganization_question_anscorrect = reorganization_question_anscorrect
        self.reorganization_question_anserror = reorganization_question_anserror
        self.reorganization_question_article = reorganization_question_article
        self.reorganization_question_question = reorganization_question_question
        self.reorganization_question_author = reorganization_question_author
        self.reorganization_question_public = reorganization_question_public
        self.reorganization_question_analysis = reorganization_question_analysis


class tb_reorganization_option(db.Model):
    __tablename__ = 'tb_reorganization_option'

    reorganization_option_id = db.Column(db.Integer, primary_key=True)
    reorganization_option_quid = db.Column(db.Integer, primary_key=True)
    reorganization_option_word = db.Column(db.Text)

    def __init__(self, reorganization_option_id, reorganization_option_quid, reorganization_option_word):
        self.reorganization_option_id = reorganization_option_id
        self.reorganization_option_quid = reorganization_option_quid
        self.reorganization_option_word = reorganization_option_word
