# -*- coding: utf-8 -*-
# https://myapollo.com.tw/2016/09/28/python-sqlalchemy-orm-1/
import datetime
import sys
import json
from sqlalchemy import create_engine, and_, exc, Integer, ForeignKey, String, Column
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import case
from sqlalchemy import func
from sqlalchemy import desc
import ast
import random
# 從database/model引入所有class，包含與DB的映對(Mapping)關係
from database.model import *

# 從database/database把db傳過來
from database.database import db


import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import warnings
import requests
import random


db.create_all()  # db建立連線

# 用來登入的API的處理(一個Class)，這邊login會傳入兩個變數(帳號，密碼)

'''
def login(ac, pw):
    try:
        # table.query.filet(條件) <=相當於Select * Where 條件
        # 這邊Member_account"不是"資料庫裡的名稱，是在Database/Model中定義的
        q = tb_member.query.filter(and_(tb_member.Member_account == str(
            ac), tb_member.Member_password == str(pw))).first()

        return q.Member_name

    except Exception as e:
        print("error occur: %s" % (e))
'''


def creatuser(id, name, password, email, phone):
    try:
        admin = tb_user(id, name, password, email, phone)
        db.session.add(admin)
        db.session.commit()
        return "success"
    except Exception as e:
        return e


def logingetuserdata(ac, pw):
    try:
        q = tb_user.query.filter(and_(tb_user.user_id == str(
            ac), tb_user.user_password == str(pw))).first()
        return q.user_id, q.user_name, q.user_email, q.user_phone
    except Exception as e:
        print("error occur: %s" % (e))


def insertword(id, index, range, engword,errortag):
    try:
        if (range == "VocabularyGEPTLA") or (range =="VocabularyGEPTLB") or (range =="VocabularyGEPTLC") or (range =="VocabularyLA"):
            wordlist = ast.literal_eval(engword)
            errortag = ast.literal_eval(errortag)
            num=0
            for i in wordlist:
                for r in db.session.query(tb_Vocabulary).filter(tb_Vocabulary.Vocabulary_ID	==str(i)).filter(tb_Vocabulary.Vocabulary_range	==str(range)):
                    word = tb_testhistroy(id, index, range, r.Vocabulary_Word,errortag[num])
                    db.session.add(word)
                    db.session.commit()
                    num+=1
            return errortag
        else:  #GPS單字
            errortag = ast.literal_eval(errortag)
            wordlist = ast.literal_eval(engword) #dict
            num=0
            for i in wordlist.values():                
                for r in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i[0])).filter(tb_gps_class.gps_class_number==str(i[1])):
                    word = tb_testhistroy(id, index, i[0], r.gps_class_word,errortag[num])
                    db.session.add(word)
                    db.session.commit()
                    num+=1
            return "GPSWORD"
        
    except Exception as e:
        print("error occur: %s" % (e))


def geterrword(rg, user):  # 取得常錯單字
    error = []
    errornum = []
    errorinex = {}
    try:
        for r in db.session.query(tb_testhistroy.histroy_word).group_by(tb_testhistroy.histroy_word).having(func.count(tb_testhistroy.histroy_word) > 1).filter(tb_testhistroy.user_id	 == str(user)).filter(tb_testhistroy.histroy_question_range == str(rg)).order_by(func.count(tb_testhistroy.histroy_word).desc()):
            error.append(r.histroy_word)
        for i in error:
            errornum.append(db.session.query(
                tb_testhistroy.histroy_word).filter(tb_testhistroy.user_id	 == str(user)).filter(tb_testhistroy.histroy_question_range == str(rg)).filter(tb_testhistroy.histroy_word == str(i)).count())

        if len(error) > 15:
            del errornum[15:]
            del errornum[15:]
        errorinex["word"] = error
        errorinex["count"] = errornum

        return errorinex
    except Exception as e:
        print("error occur: %s" % (e))


def getvacublarytest(co, ty, rg):  # co 題目數量 ty 類型 rg範圍(全民英檢中級...)
    if (ty == "Wordtest" or ty == "Lisitingtest"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}  # 選項
            answer = {}  # 解答
            count = db.session.query(tb_Vocabulary).filter(tb_Vocabulary.Vocabulary_range==str(rg)).count()

            while True:
                rd = random.randint(1, count)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break
            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                question.append(q.Vocabulary_Word)

            for i in range(len(questionid)):
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()

                answer[str(i)] = str(q.Vocabulary_Chinese)
            for j in range(int(co)):
                tempid = []
                while True:
                    rd = random.randint(1, count)
                    if rd not in tempid:
                        tempid.append(rd)
                    if len(tempid) == 3:
                        break
                tempid.append(questionid[j])
                temp = []
                for i in tempid:
                    q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                        i), tb_Vocabulary.Vocabulary_range == str(rg))).first()

                    temp.append(q.Vocabulary_Chinese)
                random.shuffle(temp)
                option[str(j)] = temp

            return(questionid, question, option, answer)
            # return q.Voca_Word

        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "Showpicture"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}  # 選項
            answer = {}  # 解答
            count = db.session.query(tb_Vocabulary).filter(tb_Vocabulary.Vocabulary_range==str(rg)).count()
            while True:
                rd = random.randint(1, count)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break
            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                question.append(q.Vocabulary_Word)

            for i in range(len(questionid)):
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()

                answer[str(i)] = str(q.Vocabulary_Word	)
            for j in range(int(co)):
                tempid = []
                while True:
                    rd = random.randint(1, count)
                    if rd not in tempid:
                        tempid.append(rd)
                    if len(tempid) == 3:
                        break
                tempid.append(questionid[j])
                temp = []
                for i in tempid:
                    q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                        i), tb_Vocabulary.Vocabulary_range == str(rg))).first()

                    temp.append(q.Vocabulary_Word)
                random.shuffle(temp)
                option[str(j)] = temp

            return(questionid, question, option, answer)
            # return q.Voca_Word

        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "SpellWordtest"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}
            answer = {}  # 解答
            count = db.session.query(tb_Vocabulary).filter(tb_Vocabulary.Vocabulary_range==str(rg)).count()

            while True:
                rd = random.randint(1, count)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break

            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                question.append(q.Vocabulary_Chinese)
            for i in range(len(questionid)):
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                answer[str(i)] = str(q.Vocabulary_Word)

            return(questionid, question, option, answer)

        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "Rearrangement"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}
            answer = {}  # 解答
            count = db.session.query(tb_Vocabulary).filter(tb_Vocabulary.Vocabulary_range==str(rg)).count()

            while True:
                rd = random.randint(1, count)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break

            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                temp = q.Vocabulary_Simple.split(" ")
                random.shuffle(temp)
                new = " ".join(temp)
                question.append(new)

            for i in range(len(questionid)):
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                answer[str(i)] = str(q.Vocabulary_Simple)

            return(questionid, question, option, answer)
        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "Sentencetest"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}
            answer = {}  # 解答
            count = db.session.query(tb_Vocabulary).filter(tb_Vocabulary.Vocabulary_range==str(rg)).count()

            while True:
                rd = random.randint(1, count)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break
            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                question.append(q.Vocabulary_CSimple)

            for i in range(len(question)):
                temp = []
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                answer[str(i)] = q.Vocabulary_Simple
                temp.append(q.Vocabulary_Simple)
                temp.append(q.Vocabulary_Word)

                option[str(i)] = temp
            return(questionid, question, option, answer)
        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "GpsWordtest" or ty == "GpsLisitingtest"):
        try:
            etype = ast.literal_eval(rg)  # 類型陣列
            randometype = []  # 隨機陣列，從類型陣列中選出要考的單字
            question = []
            option = {}
            questionid = {}
            answer = {}

            while True:
                if len(randometype) == int(co):
                    break
                f = random.choice(etype)
                randometype.append(f)

            num = 0
            for i in randometype:
                temp = []
                c = random.randint(1, db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i)).count())                    
                for r in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i)).filter(tb_gps_class.gps_class_number== str(c)):
                    question.append(r.gps_class_word)
                    temp.append(r.gps_class_etype)
                    answer[num] = r.gps_class_chinese
                    
                temp.append(c)
                questionid[num] = temp
                num += 1
            num = 0
            for i in questionid.values():
                temp = []
                for r  in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i[0])).filter(tb_gps_class.gps_class_number==str(i[1])):
                    temp.append(r.gps_class_chinese)

                while True:
                    if len(temp) >= 4:
                        break                    
                    q = random.randint(1, db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i[0])).count())
                    for r in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i[0])).filter(tb_gps_class.gps_class_number==str(q)):
                        if r.gps_class_chinese not in temp:
                            temp.append(r.gps_class_chinese)     
                                   
                option[num] = temp
                num += 1
                
            return(questionid, question, option, answer)

        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "GpsSpellWordtest"):
        try:
            etype = ast.literal_eval(rg)  # 類型陣列
            randometype = []  # 隨機陣列，從類型陣列中選出要考的單字
            question = []
            option = {}
            questionid = {}
            answer = {}

            while True:
                if len(randometype) == int(co):
                    break
                f = random.choice(etype)
                randometype.append(f)

            num = 0
            for i in randometype:
                temp = []
                c = random.randint(1, tb_gps_class.query.filter(
                    tb_gps_class.gps_class_etype == str(i)).count())
                r = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == str(
                    c), tb_gps_class.gps_class_etype == str(i))).first()
                question.append(r.gps_class_chinese)
                temp.append(r.gps_class_etype)
                temp.append(c)
                questionid[num] = temp
                num += 1

            num = 0
            for i in questionid.values():
                r = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == int(
                    i[1]), tb_gps_class.gps_class_etype == str(i[0]))).first()
                answer[str(num)] = r.gps_class_word
                num += 1
            return(questionid, question, option, answer)

        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "GpsRearrangement"):
        try:
            etype = ast.literal_eval(rg)  # 類型陣列
            randometype = []  # 隨機陣列，從類型陣列中選出要考的單字
            question = []
            option = {}
            questionid = {}
            answer = {}

            while True:
                if len(randometype) == int(co):
                    break
                f = random.choice(etype)
                randometype.append(f)
            num = 0

            for i in randometype:
                temp = []
                c = random.randint(1, tb_gps_class.query.filter(
                    tb_gps_class.gps_class_etype == str(i)).count())
                r = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == str(
                    c), tb_gps_class.gps_class_etype == str(i))).first()
                question.append(r.gps_class_example)
                temp.append(r.gps_class_etype)
                temp.append(c)
                questionid[num] = temp
                num += 1

            num = 0
            for i in questionid.values():
                r = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == int(
                    i[1]), tb_gps_class.gps_class_etype == str(i[0]))).first()
                answer[str(num)] = r.gps_class_example
                num += 1
            return(questionid, question, option, answer)
        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "GpsSentencetest"):
        try:
            etype = ast.literal_eval(rg)  # 類型陣列
            randometype = []  # 隨機陣列，從類型陣列中選出要考的單字
            question = []
            option = {}
            questionid = {}
            answer = {}

            while True:
                if len(randometype) == int(co):
                    break
                f = random.choice(etype)
                randometype.append(f)
            num = 0

            for i in randometype:
                temp = []
                c = random.randint(1, tb_gps_class.query.filter(
                    tb_gps_class.gps_class_etype == str(i)).count())
                r = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == str(
                    c), tb_gps_class.gps_class_etype == str(i))).first()
                question.append(r.gps_class_cexample)
                temp.append(r.gps_class_etype)
                temp.append(c)
                temp = []
                temp.append(r.gps_class_example)
                temp.append(r.gps_class_word)
                answer[num] = r.gps_class_example
                option[num] = temp
                questionid[num] = temp
                num += 1
            return(questionid, question, option, answer)
        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "Connecttest"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}
            answer = {}  # 解答
            count = db.session.query(tb_Vocabulary).filter(tb_Vocabulary.Vocabulary_range==str(rg)).count()

            while True:
                rd = random.randint(1, count)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break            
            
            temp1=[]
            temp2=[]
            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                temp1.append(q.Vocabulary_Word)
                temp2.append(q.Vocabulary_Chinese)
            question.append(temp1)
            question.append(temp2)

            index=0
            for i in questionid:
                temp=[]
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                temp.append(q.Vocabulary_Word)
                temp.append(q.Vocabulary_Chinese)
                answer[index]=temp
                index+=1

            


            return(questionid, question, option, answer)
        except Exception as e:
            print("error occur: %s" % (e))

def getindex(id):
    try:
        q = tb_wordtesthistroy.query.filter(tb_wordtesthistroy.wordtest_userid == str(
            id)).order_by(tb_wordtesthistroy.wordtest_id.desc()).first()
        return q.wordtest_id
    except Exception as e:
        print("error occur: %s" % (e))


def inserthistroy(userid, index, range, type, date, score, cscore):  # 插入歷史紀錄
    try:
        if (range == "VocabularyGEPTLA") or (range =="VocabularyGEPTLB") or (range =="VocabularyGEPTLC") or (range =="VocabularyLA"):
            data = tb_wordtesthistroy(
                userid, index, range, type, date, score, cscore)
        else:
            data = tb_wordtesthistroy(
                userid, index, "GpsWordtest", type, date, score, cscore)
        db.session.add(data)
        db.session.commit()
        return "success"
    except Exception as e:
        print("error occur: %s" % (e))


def getworddata(userid):
    data = []
    try:
        for r in db.session.query(tb_wordtesthistroy).filter(tb_wordtesthistroy.wordtest_userid == str(userid)).order_by(tb_wordtesthistroy.wordtest_id.desc()):
            temp = []
            temp.append(r.wordtest_id)
            temp.append(r.wordtest_range)
            temp.append(r.wordtest_type)
            temp.append(r.wordtest_score)
            temp.append(r.wordtest_date)
            data.append(temp)
        return data

    except Exception as e:
        print("error occur: %s" % (e))


def getwordhistroycontent(userid, index, trange):
    word=[]
    gpswordetype=[]
    gpswordindex=[]
    errortag=[]
    try:
        if trange == "VocabularyGEPTLA" or trange == "VocabularyGEPTLB" or trange == "VocabularyGEPTLC"  or trange == "VocabularyLA":
            for r in db.session.query(tb_testhistroy).filter(tb_testhistroy.user_id== str(userid)).filter(tb_testhistroy.user_anwerindex== str(index)).filter(tb_testhistroy.histroy_question_range == str(trange)):
                word.append(r.histroy_word)
                errortag.append(r.histroy_error_tag)
        else:
            for r in db.session.query(tb_testhistroy).filter(tb_testhistroy.user_id	== str(userid)).filter(tb_testhistroy.user_anwerindex== str(index)):
                word.append(r.histroy_word) 
                errortag.append(r.histroy_error_tag)            
            for i in word:
                temp=[]
                r = db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_word == str(i)).first()
                gpswordetype.append(r.gps_class_number)
                gpswordindex.append(r.gps_class_etype)
        return (word,gpswordetype,gpswordindex,errortag)
    except Exception as e:
        print("error occur: %s" % (e))

def getgpssword(type,word=""): #單字列表
    try:
        if word == None:
            gps_class_number = []
            gps_class_etype = []
            gps_class_ctype = []
            gps_class_word = []
            gps_class_chinese=[]
            gps_class_pos=[]
            gps_class_example=[]
            gps_class_cexample=[]
            for r in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype	 == str(type)).order_by(tb_gps_class.gps_class_number):
                gps_class_number.append(str(r.gps_class_number))
                gps_class_etype.append(str(r.gps_class_etype))
                gps_class_ctype.append(str(r.gps_class_ctype))
                gps_class_word.append(str(r.gps_class_word))
                gps_class_chinese.append(str(r.gps_class_chinese))
                gps_class_pos.append(str(r.gps_class_pos))
                if (r.gps_class_example):
                    gps_class_example.append(str(r.gps_class_example.replace(r.gps_class_example[20:],"...")))
                else:
                    gps_class_example.append("")
                gps_class_cexample.append(str(r.gps_class_cexample))

            #return (gps_class_number, gps_class_etype, gps_class_ctype,gps_class_word,gps_class_chinese,gps_class_pos,gps_class_example,gps_class_cexample)
        else:
            gps_class_number = []
            gps_class_etype = []
            gps_class_ctype = []
            gps_class_word = []
            gps_class_chinese=[]
            gps_class_pos=[]
            gps_class_example=[]
            gps_class_cexample=[]
            for r in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_word	 == str(word)).order_by(tb_gps_class.gps_class_number):
                gps_class_number.append(str(r.gps_class_number))
                gps_class_etype.append(str(r.gps_class_etype))
                gps_class_ctype.append(str(r.gps_class_ctype))
                gps_class_word.append(str(r.gps_class_word))
                gps_class_chinese.append(str(r.gps_class_chinese))
                gps_class_pos.append(str(r.gps_class_pos))
                if (r.gps_class_example):
                    gps_class_example.append(str(r.gps_class_example.replace(r.gps_class_example[20:],"...")))
                else:
                    gps_class_example.append("")
                gps_class_cexample.append(str(r.gps_class_cexample))   
        return (gps_class_number, gps_class_etype, gps_class_ctype,gps_class_word,gps_class_chinese,gps_class_pos,gps_class_example,gps_class_cexample)
    except Exception as e:
        print("error occur: %s" % (e))

def insertwordgps(word,ctype,etype,chinese,pos,example,cexample):
    q = tb_gps_class.query.filter(tb_gps_class.gps_class_etype == str(
            etype)).count()
    gpsword = tb_gps_class(q+1,etype,ctype,word,chinese,pos,example,cexample)
    db.session.add(gpsword)
    db.session.commit()
    return "success"

def updatewordgps(num, word, ctype, etype, chinese, pos, example, cexample):
    affected_rows = db.session.query(tb_gps_class).filter_by(gps_class_number=num).filter_by(gps_class_etype=etype).update({'gps_class_word':word,'gps_class_chinese':chinese,'gps_class_pos':pos,'gps_class_example':example,'gps_class_cexample':cexample})
    db.session.commit()
    return affected_rows

def getwordinformation(num, etype):
    q = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == int(
             num), tb_gps_class.gps_class_etype == str(etype))).first()
    if not(q):
        return "null"
    else:
        data=[q.gps_class_number,q.gps_class_etype,q.gps_class_ctype,q.gps_class_word,q.gps_class_chinese,q.gps_class_pos,q.gps_class_example,q.gps_class_cexample]
        return data

def delwordgps(num,etype):
    affected_rows =db.session.query(tb_gps_class).filter_by(gps_class_number=num).filter_by(gps_class_etype=etype).delete()
    db.session.commit()

    word = []
    for r in db.session.query(tb_gps_class).filter_by(gps_class_etype=etype):
        word.append(r.gps_class_word)
    
    for i in range(len(word)):
        db.session.query(tb_gps_class).filter_by(gps_class_etype=str(etype)).filter_by(gps_class_word=str(word[i+1])).update({'gps_class_number':i+1})
        db.session.commit()
        
    #db.session.commit()
    return affected_rows


def getquestion(type): #取得題目列表
    x = ast.literal_eval(type)
    questionid=[]
    questioncontent=[]
    questionlevel=[]
    if x[0] == "tb_choice":
        for r in db.session.query(tb_choice).filter_by(choice_type=str(x[1])).order_by(tb_choice.choice_id):
            questionid.append(str(r.choice_id))
            questioncontent.append(str(r.choice_question))
            questionlevel.append(str(r.choice_level))
        return (questionid, questioncontent,questionlevel)
    if x[0] == "tb_leakword_article":
        for r in db.session.query(tb_leakword_article).filter_by(leakword_article_type=str(x[1])).order_by(tb_leakword_article.leakword_article_id):
            questionid.append(str(r.leakword_article_id))
            questioncontent.append(str(r.leakword_article_article))
            questionlevel.append(str(r.leakword_article_level))
        return (questionid, questioncontent,questionlevel) 
    else:
        return ("other","null","null")
    #return (questionid, questioncontent,questionlevel)

def insertquesstionchoice(type,question, choice_ansA,choice_ansB,choice_ansC,choice_ansD,choice_answer,choice_analysis):    
    count = db.session.query(tb_choice).filter_by(choice_type=str(type)).count()
    data  = tb_choice(count+1,"英文",type,0,0,0,0,question,choice_ansA,choice_ansB,choice_ansC,choice_ansD,choice_answer,"test",0,choice_analysis)
    db.session.add(data)
    db.session.commit()      
    return "success"    

def delquestion(tb_name,type,id):
    if tb_name == "tb_choice":
        x = ast.literal_eval(id)
        for i in x:
            affected_rows =db.session.query(tb_choice).filter_by(choice_id	=str(i)).filter_by(choice_type=str(type)).delete()
            db.session.commit()

        questionid=[]
        for r in db.session.query(tb_choice).filter_by(choice_type=str(type)).order_by((tb_choice.choice_id).asc()):
            questionid.append(r.choice_id)

        for i in range(len(questionid)):
            db.session.query(tb_choice).filter_by(choice_id=str(questionid[i])).filter_by(choice_type=str(type)).update({'choice_id':i+1})
            db.session.commit()
    if tb_name == "tb_leakword_article":
        x = ast.literal_eval(id)
        for i in x:
            affected_rows =db.session.query(tb_leakword_article).filter_by(leakword_article_id=str(i)).delete()
            db.session.commit()
            
        questionid=[]
        for r in db.session.query(tb_leakword_article).filter_by(leakword_article_type=str(type)).order_by((tb_leakword_article.leakword_article_id).asc()):
            questionid.append(r.leakword_article_id)

        for i in range(len(questionid)):
            db.session.query(tb_leakword_article).filter_by(leakword_article_id	=str(questionid[i])).filter_by(leakword_article_type=str(type)).update({'leakword_article_id':i+1})
            db.session.commit()
        

def getquestinformation(tb_name,type,id):
    if tb_name == "tb_choice":
        try:
            r = tb_choice.query.filter(and_(tb_choice.choice_id == int(
                id), tb_choice.choice_type == str(type))).first()            
            data=[r.choice_question,r.choice_ansA,r.choice_ansB,r.choice_ansC,r.choice_ansD,r.choice_answer,r.choice_analysis,r.choice_type]
            return data
        except Exception as e: #表示沒資料
            return "null"

def updatequestionchoice(tb_name,type,id,question, choice_ansA,choice_ansB,choice_ansC,choice_ansD,choice_answer,choice_analysis):
    if tb_name == "tb_choice":
        affected_rows = db.session.query(tb_choice).filter_by(choice_id	=id).filter_by(choice_type=type).update({'choice_question':question,'choice_ansA':choice_ansA,'choice_ansB':choice_ansB,'choice_ansC':choice_ansC,'choice_ansD':choice_ansD,'choice_answer':choice_answer,'choice_analysis':choice_analysis,'choice_answer':choice_answer})
        db.session.commit()
        return affected_rows

def insertleakwordarticle(type,leakword_article_article,leakword_article_analysis):     #新增題目
    count = db.session.query(tb_leakword_article).filter_by(leakword_article_type=str(type)).count()
    data  = tb_leakword_article(count+1,0,"英文",type,0,0,0,leakword_article_article,"test",1,leakword_article_analysis)
    db.session.add(data)
    db.session.commit()

def getarticleleakindex():
    count = db.session.query(tb_leakword_article).count()
    return count

def insertleakwordoption(type,articleid,leakword_option_ansA,leakword_option_ansB,leakword_option_ansC,leakword_option_ansD,leakword_option_answer):    
    count_id = db.session.query(tb_leakword_option).filter_by(leakword_option_quid=str(articleid)).count()
    data  = tb_leakword_option(articleid,count_id+1,"英文",type,0,0,0,0,
                               leakword_option_ansA,leakword_option_ansB,leakword_option_ansC,leakword_option_ansD,
                               leakword_option_answer)
    db.session.add(data)
    db.session.commit()



def getoptioncontentleak(type,id):
    optioncontent=[]
    answer=[]
    optioncount = db.session.query(tb_leakword_option).filter_by(leakword_option_quid=str(id)).count()
    for r in db.session.query(tb_leakword_option).filter_by(leakword_option_quid=str(id)).order_by((tb_leakword_option.leakword_option_id).asc()):
        answer.append(r.leakword_option_answer)
        temp=[]
        temp.append(r.leakword_option_ansA)
        temp.append(r.leakword_option_ansB)
        temp.append(r.leakword_option_ansC)
        temp.append(r.leakword_option_ansD)
        optioncontent.append(temp)
    for r in db.session.query(tb_leakword_article).filter_by(leakword_article_id=str(id)):
        question = r.leakword_article_article	
    return optioncount,optioncontent,question,answer

def updatearticleleak(type,id,article):
    x = ast.literal_eval(type)
    db.session.query(tb_leakword_article).filter_by(leakword_article_id=str(id)).filter_by(leakword_article_type=str(x[1])).update({'leakword_article_article':str(article)})
    db.session.commit()

def deleteoptionleak(type,id):
    x = ast.literal_eval(type)
    affected_rows = db.session.query(tb_leakword_option).filter_by(leakword_option_quid	=str(id)).delete()
    db.session.commit()
    return affected_rows

def creatmatplotlib(user):
    import os 
    sudoPassword = 'AA0976707623'
    command = 'sudo touch /var/www/html/123.txt'
    p = os.system('echo %s|sudo -S %s' % (sudoPassword, command))
    word=[]
    wordrange=[]

    for r in db.session.query(tb_testhistroy.histroy_word).group_by(tb_testhistroy.histroy_word).having(func.count(tb_testhistroy.histroy_word) > 1).filter(tb_testhistroy.user_id== str(user)).order_by(func.count(tb_testhistroy.histroy_word).desc()):
        word.append(r.histroy_word)
            
    for i in word[0:4]:
        row =  db.session.query(tb_testhistroy).filter_by(histroy_word=str(i)).filter(tb_testhistroy.user_id== str(user)).first()
        if row:
            wordrange.append(row.histroy_question_range)

    colorlist=['aqua','yellow']

    colors=[]
    edge_list=[]

    for i in list(dict.fromkeys(wordrange[0:4])): #先關聯使用者與常錯類型   
        if i =="VocabularyLA":
            i ="國中英文單字"
        elif   i=="VocabularyGEPTLA":
            i ="全民英檢初級"
        elif  i =="VocabularyGEPTLB":
            i ="全民英檢中級"
        elif  i =="VocabularyGEPTLC":
            i ="全民英檢高級"
        edge_list.append((str(user), str(i)))

    for i in range(len(word[0:4])):
        if (wordrange[i] =="VocabularyLA"):
            wordrange[i] ="國中英文單字"
        elif  wordrange[i] =="VocabularyGEPTLA":
            wordrange[i] = "全民英檢初級"
        elif  wordrange[i] =="VocabularyGEPTLB":
            wordrange[i] = "全民英檢中級"
        elif  wordrange[i]  =="VocabularyGEPTLC":
            wordrange[i] ="全民英檢高級"
        
        edge_list.append((wordrange[i],word[i]))
    
    
    for i in (word[0:4]):
        r = requests.get('https://relatedwords.org/api/related?term='+str(i))
        data1 =r.json()
        relationwordlsit=[]
        for j in data1:
            if " " not in j['word']:
                relationwordlsit.append(j['word'])
            if len(relationwordlsit) >=3:
                break
        for k in relationwordlsit:       
            edge_list.append((i,k))
    

    for i in range(len(edge_list)+1):
        colors.append("cyan")
    for i in range(len(list(dict.fromkeys(wordrange)))+1):
        colors[i] ="blue"
    for i in range(len(list(dict.fromkeys(wordrange)))+1,len(list(dict.fromkeys(wordrange)))+len(wordrange)+1):
        colors[i] ="red"

    colors[0]="yellow"

    #colors = ['grey', 'aqua', 'aqua', 'aqua', 'aqua', 'aqua', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow']
    #edge_list = [('tet', 'SPA'), ('tet', 'atm'), ('tet', 'florist'),('tet','bakery'),("tet","museum"),("SPA","bath"),("atm","teller"),("atm","atmosphere"),("florist","corsage"),("bakery","baked"),("museum","louvre"),("SPA","sauna")]
    g = nx.Graph(edge_list)
    pos = nx.spring_layout(g, k=0.3*1/np.sqrt(len(g.nodes())), iterations=20)
    plt.figure(3, figsize=(10, 10))
    nx.draw(g, pos=pos,with_labels=True, node_size=900, node_color = colors)
    nx.draw_networkx_labels(g, pos=pos)
    plt.savefig('/var/www/html/'+user+'.jpg')
    #plt.savefig('image.jpg')
    return (word[0:5],wordrange[0:5])