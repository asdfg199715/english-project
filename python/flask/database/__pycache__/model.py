# -*- coding: utf-8 -*-
from database.database import db

# 自己設定的名稱，可以跟Database的Table不同


class tb_user(db.Model):
    __tablename__ = 'tb_user'

    user_id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.VARCHAR(5))
    user_password = db.Column(db.VARCHAR(20))
    user_email = db.Column(db.VARCHAR(50))
    user_phone = db.Column(db.VARCHAR(10))

    def __init__(self, user_id, user_name, user_password, user_email, user_phone):
        self.user_id = user_id
        self.user_name = user_name
        self.user_password = user_password
        self.user_email = user_email
        self.user_phone = user_phone

    def get_id(self):
        return self.user_id


class tb_Vocabulary(db.Model):
    __tablename__ = 'tb_Vocabulary'

    Vocabulary_ID = db.Column(db.Integer, primary_key=True)
    Vocabulary_range = db.Column(db.Text, primary_key=True)
    Vocabulary_Word = db.Column(db.Text)
    Vocabulary_Chinese = db.Column(db.Text)
    Vocabulary_Part = db.Column(db.Text)
    Vocabulary_Simple = db.Column(db.Text)
    Vocabulary_CSimple = db.Column(db.Text)

    def __init__(self, Vocabulary_ID, Vocabulary_range, Vocabulary_Word, Vocabulary_Chinese, Vocabulary_Part, Vocabulary_Simple, Vocabulary_CSimple):
        self.Vocabulary_ID = Vocabulary_ID
        self.Vocabulary_range = Vocabulary_range
        self.Vocabulary_Word = Vocabulary_Word
        self.Vocabulary_Chinese = Vocabulary_Chinese
        self.Vocabulary_Part = Vocabulary_Part
        self.Vocabulary_Simple = Vocabulary_Simple
        self.Vocabulary_CSimple = Vocabulary_CSimple


class tb_error_word(db.Model):
    __tablename__ = 'tb_error_word'

    error_id = db.Column(db.Text, primary_key=True)
    error_anwerindex = db.Column(db.Text, primary_key=True)
    error_question_range = db.Column(db.Text, primary_key=True)
    error_word = db.Column(db.Text, primary_key=True)

    def __init__(self, error_id, error_anwerindex, error_question_range, error_word):
        self.error_id = error_id
        self.error_anwerindex = error_anwerindex
        self.error_question_range = error_question_range
        self.error_word = error_word


class tb_wordtesthistroy(db.Model):
    __tablename__ = 'tb_wordtesthistroy'

    wordtest_userid = db.Column(db.Text, primary_key=True)
    wordtest_id = db.Column(db.Text, primary_key=True)
    wordtest_range = db.Column(db.Text)
    wordtest_type = db.Column(db.Text)
    wordtest_date = db.Column(db.Text)
    wordtest_score = db.Column(db.Text)
    wordtest_cscore = db.Column(db.Text)

    def __init__(self, wordtest_userid, wordtest_id, wordtest_range, wordtest_type, wordtest_date, wordtest_score, wordtest_cscore):
        self.wordtest_userid = wordtest_userid
        self.wordtest_id = wordtest_id
        self.wordtest_range = wordtest_range
        self.wordtest_type = wordtest_type
        self.wordtest_date = wordtest_date
        self.wordtest_score = wordtest_score
        self.wordtest_cscore = wordtest_cscore


class tb_gps_class(db.Model):
    __tablename__ = 'tb_gps_class'

    gps_class_number = db.Column(db.Integer, primary_key=True)
    gps_class_etype = db.Column(db.Text, primary_key=True)
    gps_class_ctype = db.Column(db.Text)
    gps_class_word = db.Column(db.Text)
    gps_class_chinese = db.Column(db.Text)
    gps_class_pos = db.Column(db.Text)
    gps_class_example = db.Column(db.Text)
    gps_class_cexample = db.Column(db.Text)

    def __init__(self, gps_class_number, gps_class_etype, gps_class_ctype, gps_class_word, gps_class_chinese, gps_class_pos, gps_class_example, gps_class_cexample):
        self.gps_class_number = gps_class_number
        self.gps_class_etype = gps_class_etype
        self.gps_class_ctype = gps_class_ctype
        self.gps_class_word = gps_class_word
        self.gps_class_chinese = gps_class_chinese
        self.gps_class_pos = gps_class_pos
        self.gps_class_example = gps_class_example
        self.gps_class_cexample = gps_class_cexample
