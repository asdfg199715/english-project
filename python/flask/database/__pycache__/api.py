# -*- coding: utf-8 -*-
# https://myapollo.com.tw/2016/09/28/python-sqlalchemy-orm-1/
import datetime
import sys
import json
from sqlalchemy import create_engine, and_, exc, Integer, ForeignKey, String, Column
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import case
from sqlalchemy import func
from sqlalchemy import desc
import ast
import random
# 從database/model引入所有class，包含與DB的映對(Mapping)關係
from database.model import *

# 從database/database把db傳過來
from database.database import db

db.create_all()  # db建立連線

# 用來登入的API的處理(一個Class)，這邊login會傳入兩個變數(帳號，密碼)

'''
def login(ac, pw):
    try:
        # table.query.filet(條件) <=相當於Select * Where 條件
        # 這邊Member_account"不是"資料庫裡的名稱，是在Database/Model中定義的
        q = tb_member.query.filter(and_(tb_member.Member_account == str(
            ac), tb_member.Member_password == str(pw))).first()

        return q.Member_name

    except Exception as e:
        print("error occur: %s" % (e))
'''


def creatuser(id, name, password, email, phone):
    try:
        admin = tb_user(id, name, password, email, phone)
        db.session.add(admin)
        db.session.commit()
        return "success"
    except Exception as e:
        return e


def logingetuserdata(ac, pw):
    try:
        q = tb_user.query.filter(and_(tb_user.user_id == str(
            ac), tb_user.user_password == str(pw))).first()
        return q.user_id, q.user_name, q.user_email, q.user_phone
    except Exception as e:
        print("error occur: %s" % (e))


def insertword(id, index, range, engword):
    try:
        if range == ("VocabularyGEPTLA" or "VocabularyGEPTLB" or "VocabularyGEPTLC" or "VocabularyLA"):
            wordlist = ast.literal_eval(engword)
            for i in wordlist:
                for r in db.session.query(tb_Vocabulary).filter(tb_Vocabulary.Vocabulary_ID	==str(i)).filter(tb_Vocabulary.Vocabulary_range	==str(range)):
                    word = tb_error_word(id, index, range, r.Vocabulary_Word)
                    db.session.add(word)
                    db.session.commit()
            return "Success"
        else:  #GPS單字
            wordlist = ast.literal_eval(engword) #dict
            for i in wordlist.values():
                for r in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i[0])).filter(tb_gps_class.gps_class_number==str(i[1])):
                    word = tb_error_word(id, index, "GpsWordtest", r.gps_class_word)
                    db.session.add(word)
                    db.session.commit()
            return "Success"
        
    except Exception as e:
        print("error occur: %s" % (e))


def geterrword(rg, user):  # 取得常錯單字
    error = []
    errornum = []
    errorinex = {}
    try:
        for r in db.session.query(tb_error_word.error_word).group_by(tb_error_word.error_word).having(func.count(tb_error_word.error_word) > 1).filter(tb_error_word.error_id == str(user)).filter(tb_error_word.error_question_range == str(rg)).order_by(func.count(tb_error_word.error_word).desc()):
            error.append(r.error_word)
        for i in error:
            errornum.append(db.session.query(
                tb_error_word.error_word).filter(tb_error_word.error_id == "tet").filter(tb_error_word.error_question_range == str(rg)).filter(tb_error_word.error_word == str(i)).count())

        if len(error) > 15:
            del errornum[15:]
            del errornum[15:]
        errorinex["word"] = error
        errorinex["count"] = errornum

        return errorinex
    except Exception as e:
        print("error occur: %s" % (e))


def getvacublarytest(co, ty, rg):  # co 題目數量 ty 類型 rg範圍(全民英檢中級...)
    if (ty == "Wordtest" or ty == "Lisitingtest"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}  # 選項
            answer = {}  # 解答
            while True:
                rd = random.randint(1, 1087)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break
            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                question.append(q.Vocabulary_Word)

            for i in range(len(questionid)):
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()

                answer[str(i)] = str(q.Vocabulary_Chinese)
            for j in range(int(co)):
                tempid = []
                while True:
                    rd = random.randint(1, 1087)
                    if rd not in tempid:
                        tempid.append(rd)
                    if len(tempid) == 3:
                        break
                tempid.append(questionid[j])
                temp = []
                for i in tempid:
                    q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                        i), tb_Vocabulary.Vocabulary_range == str(rg))).first()

                    temp.append(q.Vocabulary_Chinese)
                random.shuffle(temp)
                option[str(j)] = temp

            return(questionid, question, option, answer)
            # return q.Voca_Word

        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "SpellWordtest"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}
            answer = {}  # 解答

            while True:
                rd = random.randint(1, 1087)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break

            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                question.append(q.Vocabulary_Chinese)
            for i in range(len(questionid)):
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                answer[str(i)] = str(q.Vocabulary_Word)

            return(questionid, question, option, answer)

        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "Rearrangement"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}
            answer = {}  # 解答

            while True:
                rd = random.randint(1, 1087)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break

            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                temp = q.Vocabulary_Simple.split(" ")
                random.shuffle(temp)
                new = " ".join(temp)
                question.append(new)

            for i in range(len(questionid)):
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                answer[str(i)] = str(q.Vocabulary_Simple)

            return(questionid, question, option, answer)
        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "Sentencetest"):
        try:
            questionid = []  # 題目ID
            question = []  # 題目
            option = {}
            answer = {}  # 解答
            while True:
                rd = random.randint(1, 1087)
                if rd not in questionid:
                    questionid.append(rd)
                if (len(questionid) == int(co)):
                    break
            for i in questionid:
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    i), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                question.append(q.Vocabulary_CSimple)

            for i in range(len(question)):
                temp = []
                q = tb_Vocabulary.query.filter(and_(tb_Vocabulary.Vocabulary_ID == str(
                    questionid[i]), tb_Vocabulary.Vocabulary_range == str(rg))).first()
                answer[str(i)] = q.Vocabulary_Simple
                temp.append(q.Vocabulary_Simple)
                temp.append(q.Vocabulary_Word)

                option[str(i)] = temp
            return(questionid, question, option, answer)
        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "GpsWordtest" or ty == "GpsLisitingtest"):
        try:
            etype = ast.literal_eval(rg)  # 類型陣列
            randometype = []  # 隨機陣列，從類型陣列中選出要考的單字
            question = []
            option = {}
            questionid = {}
            answer = {}

            while True:
                if len(randometype) == int(co):
                    break
                f = random.choice(etype)
                randometype.append(f)

            num = 0
            for i in randometype:
                temp = []
                c = random.randint(1, db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i)).count())
                    
                for r in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i)).filter(tb_gps_class.gps_class_number== str(c)):
                    question.append(r.gps_class_word)
                    temp.append(r.gps_class_etype)
                
                temp.append(c)
                questionid[num] = temp
                num += 1

            
            num = 0
            for i in questionid.values():
                temp = []
                for r  in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i[0])).filter(tb_gps_class.gps_class_number==str(i[1])):
                    temp.append(r.gps_class_chinese)

                while True:
                    if len(temp) >= 4:
                        break                    
                    q = random.randint(1, db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i[0])).count())
                    for r in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i[0])).filter(tb_gps_class.gps_class_number==str(q)):
                        temp.append(r.gps_class_chinese)                    
                    temp.append(r.gps_class_chinese)
                option[num] = temp
                
                for r in  db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype==str(i[0])).filter(tb_gps_class.gps_class_number==str(q)):
                    answer[num] = r.gps_class_chinese
                
                num += 1
                
            return(questionid, question, option, answer)

        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "GpsSpellWordtest"):
        try:
            etype = ast.literal_eval(rg)  # 類型陣列
            randometype = []  # 隨機陣列，從類型陣列中選出要考的單字
            question = []
            option = {}
            questionid = {}
            answer = {}

            while True:
                if len(randometype) == int(co):
                    break
                f = random.choice(etype)
                randometype.append(f)

            num = 0
            for i in randometype:
                temp = []
                c = random.randint(1, tb_gps_class.query.filter(
                    tb_gps_class.gps_class_etype == str(i)).count())
                r = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == str(
                    c), tb_gps_class.gps_class_etype == str(i))).first()
                question.append(r.gps_class_chinese)
                temp.append(r.gps_class_etype)
                temp.append(c)
                questionid[num] = temp
                num += 1

            num = 0
            for i in questionid.values():
                r = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == int(
                    i[1]), tb_gps_class.gps_class_etype == str(i[0]))).first()
                answer[str(num)] = r.gps_class_word
                num += 1
            return(questionid, question, option, answer)

        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "GpsRearrangement"):
        try:
            etype = ast.literal_eval(rg)  # 類型陣列
            randometype = []  # 隨機陣列，從類型陣列中選出要考的單字
            question = []
            option = {}
            questionid = {}
            answer = {}

            while True:
                if len(randometype) == int(co):
                    break
                f = random.choice(etype)
                randometype.append(f)
            num = 0

            for i in randometype:
                temp = []
                c = random.randint(1, tb_gps_class.query.filter(
                    tb_gps_class.gps_class_etype == str(i)).count())
                r = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == str(
                    c), tb_gps_class.gps_class_etype == str(i))).first()
                question.append(r.gps_class_example)
                temp.append(r.gps_class_etype)
                temp.append(c)
                questionid[num] = temp
                num += 1

            num = 0
            for i in questionid.values():
                r = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == int(
                    i[1]), tb_gps_class.gps_class_etype == str(i[0]))).first()
                answer[str(num)] = r.gps_class_example
                num += 1
            return(questionid, question, option, answer)
        except Exception as e:
            print("error occur: %s" % (e))
    if (ty == "GpsSentencetest"):
        try:
            etype = ast.literal_eval(rg)  # 類型陣列
            randometype = []  # 隨機陣列，從類型陣列中選出要考的單字
            question = []
            option = {}
            questionid = {}
            answer = {}

            while True:
                if len(randometype) == int(co):
                    break
                f = random.choice(etype)
                randometype.append(f)
            num = 0

            for i in randometype:
                temp = []
                c = random.randint(1, tb_gps_class.query.filter(
                    tb_gps_class.gps_class_etype == str(i)).count())
                r = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == str(
                    c), tb_gps_class.gps_class_etype == str(i))).first()
                question.append(r.gps_class_cexample)
                temp.append(r.gps_class_etype)
                temp.append(c)
                temp = []
                temp.append(r.gps_class_example)
                temp.append(r.gps_class_word)
                answer[num] = r.gps_class_example
                option[num] = temp
                questionid[num] = temp
                num += 1
            return(questionid, question, option, answer)
        except Exception as e:
            print("error occur: %s" % (e))


def getindex(id):
    try:
        q = tb_wordtesthistroy.query.filter(tb_wordtesthistroy.wordtest_userid == str(
            id)).order_by(tb_wordtesthistroy.wordtest_id.desc()).first()
        return q.wordtest_id
    except Exception as e:
        print("error occur: %s" % (e))


def inserthistroy(userid, index, range, type, date, score, cscore):  # 插入歷史紀錄
    try:
        if range == ("VocabularyGEPTLA" or "VocabularyGEPTLB" or "VocabularyGEPTLC" or "VocabularyLA"):
            data = tb_wordtesthistroy(
                userid, index, range, type, date, score, cscore)
        else:
            data = tb_wordtesthistroy(
                userid, index, "GpsWordtest", type, date, score, cscore)
        db.session.add(data)
        db.session.commit()
        return "success"
    except Exception as e:
        print("error occur: %s" % (e))


def getworddata(userid):
    data = []
    try:
        for r in db.session.query(tb_wordtesthistroy).filter(tb_wordtesthistroy.wordtest_userid == str(userid)).order_by(tb_wordtesthistroy.wordtest_id.desc()):
            temp = []
            temp.append(r.wordtest_id)
            temp.append(r.wordtest_range)
            temp.append(r.wordtest_type)
            temp.append(r.wordtest_score)
            temp.append(r.wordtest_date)
            data.append(temp)
        return data

    except Exception as e:
        print("error occur: %s" % (e))


def getwordhistroycontent(userid, index, trange):
    word=[]
    gpswordetype=[]
    gpswordindex=[]
    try:
        if trange == "GpsWordtest":
            for r in db.session.query(tb_error_word).filter(tb_error_word.error_id== str(userid)).filter(tb_error_word.error_anwerindex	 == str(index)).filter(tb_error_word.error_question_range == str(trange)):
                word.append(r.error_word)            
            
            for i in word:
                temp=[]
                r = db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_word == str(i)).first()
                gpswordetype.append(r.gps_class_number)
                gpswordindex.append(r.gps_class_etype)
            
        else:
            for r in db.session.query(tb_error_word).filter(tb_error_word.error_id == str(userid)).filter(tb_error_word.error_anwerindex == str(index)).filter(tb_error_word.error_question_range == str(trange)):
                word.append(r.error_word)
        return (word,gpswordetype,gpswordindex)
    except Exception as e:
        print("error occur: %s" % (e))


def getgpssword(type,word=""): #單字列表
    try:
        if word == None:
            gps_class_number = []
            gps_class_etype = []
            gps_class_ctype = []
            gps_class_word = []
            gps_class_chinese=[]
            gps_class_pos=[]
            gps_class_example=[]
            gps_class_cexample=[]
            for r in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_etype	 == str(type)).order_by(tb_gps_class.gps_class_number):
                gps_class_number.append(str(r.gps_class_number))
                gps_class_etype.append(str(r.gps_class_etype))
                gps_class_ctype.append(str(r.gps_class_ctype))
                gps_class_word.append(str(r.gps_class_word))
                gps_class_chinese.append(str(r.gps_class_chinese))
                gps_class_pos.append(str(r.gps_class_pos))
                if (r.gps_class_example):
                    gps_class_example.append(str(r.gps_class_example.replace(r.gps_class_example[20:],"...")))
                else:
                    gps_class_example.append("")
                gps_class_cexample.append(str(r.gps_class_cexample))

            #return (gps_class_number, gps_class_etype, gps_class_ctype,gps_class_word,gps_class_chinese,gps_class_pos,gps_class_example,gps_class_cexample)
        else:
            gps_class_number = []
            gps_class_etype = []
            gps_class_ctype = []
            gps_class_word = []
            gps_class_chinese=[]
            gps_class_pos=[]
            gps_class_example=[]
            gps_class_cexample=[]
            for r in db.session.query(tb_gps_class).filter(tb_gps_class.gps_class_word	 == str(word)).order_by(tb_gps_class.gps_class_number):
                gps_class_number.append(str(r.gps_class_number))
                gps_class_etype.append(str(r.gps_class_etype))
                gps_class_ctype.append(str(r.gps_class_ctype))
                gps_class_word.append(str(r.gps_class_word))
                gps_class_chinese.append(str(r.gps_class_chinese))
                gps_class_pos.append(str(r.gps_class_pos))
                if (r.gps_class_example):
                    gps_class_example.append(str(r.gps_class_example.replace(r.gps_class_example[20:],"...")))
                else:
                    gps_class_example.append("")
                gps_class_cexample.append(str(r.gps_class_cexample))   
        return (gps_class_number, gps_class_etype, gps_class_ctype,gps_class_word,gps_class_chinese,gps_class_pos,gps_class_example,gps_class_cexample)
    except Exception as e:
        print("error occur: %s" % (e))

def insertwordgps(word,ctype,etype,chinese,pos,example,cexample):
    q = tb_gps_class.query.filter(tb_gps_class.gps_class_etype == str(
            etype)).count()
    gpsword = tb_gps_class(q+1,etype,ctype,word,chinese,pos,example,cexample)
    db.session.add(gpsword)
    db.session.commit()
    return "success"


def updatewordgps(num, word, ctype, etype, chinese, pos, example, cexample):
    affected_rows = db.session.query(tb_gps_class).filter_by(gps_class_number=num).filter_by(gps_class_etype=etype).update({'gps_class_word':word,'gps_class_chinese':chinese,'gps_class_pos':pos,'gps_class_example':example,'gps_class_cexample':cexample})
    db.session.commit()
    return affected_rows

def getwordinformation(num, etype):
    q = tb_gps_class.query.filter(and_(tb_gps_class.gps_class_number == int(
             num), tb_gps_class.gps_class_etype == str(etype))).first()
    if not(q):
        return "null"
    else:
        data=[q.gps_class_number,q.gps_class_etype,q.gps_class_ctype,q.gps_class_word,q.gps_class_chinese,q.gps_class_pos,q.gps_class_example,q.gps_class_cexample]
        return data

def delwordgps(num,etype):


    affected_rows =db.session.query(tb_gps_class).filter_by(gps_class_number=num).filter_by(gps_class_etype=etype).delete()
    db.session.commit()

    word = []
    for r in db.session.query(tb_gps_class).filter_by(gps_class_etype=etype):
        word.append(r.gps_class_word)
    
    for i in range(len(word)):
        db.session.query(tb_gps_class).filter_by(gps_class_etype=str(etype)).filter_by(gps_class_word=str(word[i+1])).update({'gps_class_number':i+1})
        db.session.commit()
        
    #db.session.commit()
    return affected_rows